<?php
/* =============================== *
 * ****     CONFIG. GERAL     **** *
 * =============================== */
$_CONF['lang'] = 'pt';//idioma padrao
$_CONF['lang_avaliable'] = array('pt','en');
$_CONF['charset'] = 'UTF-8';

/* =============================== *
 * *** CONFIG. BANCO DE DADOS **** *
 * =============================== */
$i=0;
$_CONF['sgbd'][$i] = 'mysql';//myql, postgreS
$_CONF['host'][$i] = 'localhost'; //caminho para o banco de dados
$_CONF['user'][$i] = 'root'; //usuario do banco de dados
$_CONF['pwd'][$i] = 'rootpass'; //senha do usuario
$_CONF['db'][$i] = 'xxies189_petshop'; //nome do banco de dados
$_CONF['db_charset'][$i] = 'utf8';
$_CONF['db_decode_charset'][$i] = 'latin1';
/* ============================== *
 * ***** CONF. DO SISTEMA ******* *
 * ============================== */
 $_CONF['sys_name'] = 'petshop';
 $_CONF['sys_title'] = 'Capricho no Bixo - PetShop';
 $_CONF['sys_title_upper'] = 'PETSHOP CAPRICHO NO BIXO';
 $_CONF['sys_cnpj'] = '';						   
 $_CONF['sys_ie'] = '';
 $_CONF['sys_agencia'] = '';
 $_CONF['sys_conta_corrente'] = '';
 $_CONF['sys_endereco'] = 'Rua Joaquim de Oliveira, 284 - Centro - Monte Alegre do Sul / SP';			  
 $_CONF['sys_tel'] =  'Tel: (19) 3899-2127 / (19) 99955-6469';
 
/* ===================================== *
 * ***** CONFIGS BLOCKS E TEMPLATES **** *
 * ===================================== */
define('DOMAIN','http://www.caprichonobixo.com.br');
define('DOCUMENT_ROOT', './');
define('DIR_LOCALE', './locale');
define('DIR_PAGES','./pages/');
define('DIR_ASYNC', DIR_PAGES.'async/');
define('DIR_THEMES', './themes/');
define('DIR_TEMPLATES', 'templates/');
define('DIR_TEMPLATE_PAGES', DIR_PAGES.DIR_TEMPLATES.'/');
define('DIR_PAGES_CSS', DIR_PAGES.'css/');
define('DIR_PAGES_JS', DIR_PAGES.'js/');

/* ============================== *
 * ***** ERROR REPORTING  ******* *
 * ============================== */
//session_cache_limiter('private');
session_cache_expire(240);
//@define('VERBOSE_AT', false);

/* Valores para ERROR REPORTING
1  	 E_ERROR
2 	 E_WARNING
4 	 E_PARSE
8 	 E_NOTICE
16 	 E_CORE_ERROR
32 	 E_CORE_WARNING
64 	 E_COMPILE_ERROR
128  E_COMPILE_WARNING
256  E_USER_ERROR
512  E_USER_WARNING
1024 E_USER_NOTICE
6143 E_ALL
2048 E_STRICT  (php 5.0)
4096 E_RECOVERABLE_ERROR php (5.2.0)
*/
@define('SHOW_SQL_ERROR', true);
error_reporting(E_ERROR);
ini_set('display_errors', 1);
ini_set('default_charset', $_CONF['charset']);
?>
