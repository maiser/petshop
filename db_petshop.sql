-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 13, 2016 at 06:52 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";;

--
-- Database: `xxies189_petshop`
--
CREATE DATABASE IF NOT EXISTS `xxies189_petshop` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `xxies189_petshop`;

-- --------------------------------------------------------

--
-- Table structure for table `AGENDA`
--

DROP TABLE IF EXISTS `AGENDA`;
CREATE TABLE IF NOT EXISTS `AGENDA` (
  `id` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `fk_pet` int(11) NOT NULL,
  `operacao` tinyint(1) DEFAULT '0' COMMENT '0: agendado, 1:concluído, 2: desmarcado'
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AGENDA`
--

INSERT INTO `AGENDA` (`id`, `data`, `hora`, `fk_pet`, `operacao`) VALUES
(1, '2015-01-14', '09:30:00', 6, 1),
(2, '2015-01-28', '08:25:00', 6, 1),
(3, '2015-02-12', '09:30:00', 7, 1),
(4, '2015-01-29', '09:30:00', 8, 1),
(5, '2015-02-05', '09:30:00', 8, 1),
(6, '2015-02-12', '09:30:00', 8, 1),
(7, '2015-02-12', '10:00:00', 9, 1),
(8, '2015-02-12', '13:30:00', 10, 1),
(9, '2015-02-12', '14:00:00', 11, 1),
(10, '2015-02-12', '15:30:00', 12, 1),
(11, '2015-02-06', '13:30:00', 13, 1),
(12, '2015-02-06', '13:30:00', 14, 1),
(13, '2015-02-04', '13:30:00', 15, 1),
(14, '2015-02-13', '13:30:00', 13, 1),
(15, '2015-02-13', '13:30:00', 14, 1),
(16, '0000-00-00', '00:00:00', 15, 2),
(17, '2015-02-12', '11:00:00', 3, 1),
(18, '2015-02-12', '16:00:00', 16, 1),
(19, '2015-02-13', '15:30:00', 17, 1),
(20, '2015-02-13', '15:35:00', 18, 1),
(21, '2015-01-30', '15:30:00', 17, 1),
(22, '2015-01-30', '15:35:00', 18, 1),
(23, '2015-02-06', '15:30:00', 17, 1),
(24, '2015-02-06', '15:30:00', 18, 1),
(25, '2015-02-12', '10:20:00', 19, 1),
(26, '2015-02-13', '09:30:00', 20, 1),
(27, '2015-02-13', '10:00:00', 21, 1),
(28, '2015-02-16', '10:00:00', 22, 1),
(29, '2015-02-14', '08:30:00', 23, 1),
(30, '2015-02-16', '08:30:00', 24, 1),
(31, '2015-02-17', '09:30:00', 25, 1),
(32, '2015-02-17', '09:00:00', 26, 1),
(33, '2015-02-17', '10:00:00', 27, 1),
(34, '2015-02-19', '10:00:00', 6, 1),
(35, '2015-02-19', '10:30:00', 7, 1),
(36, '2015-02-19', '09:30:00', 19, 1),
(37, '2015-02-19', '08:30:00', 8, 1),
(38, '2015-02-19', '14:00:00', 28, 1),
(39, '2015-02-20', '13:30:00', 13, 2),
(40, '2015-02-20', '14:00:00', 14, 2),
(41, '0000-00-00', '00:00:00', 15, 1),
(42, '2015-02-20', '10:00:00', 20, 1),
(43, '2015-02-20', '10:30:00', 21, 1),
(44, '2015-02-20', '10:00:00', 29, 1),
(45, '2015-03-03', '09:30:00', 30, 2),
(46, '2015-02-04', '15:00:00', 31, 1),
(47, '2014-12-24', '15:00:00', 32, 1),
(48, '2014-12-31', '15:00:00', 32, 1),
(49, '2014-12-17', '17:00:00', 33, 1),
(50, '2015-02-20', '15:30:00', 17, 1),
(51, '2015-02-20', '15:30:00', 18, 1),
(52, '2015-02-18', '16:30:00', 34, 1),
(53, '2015-02-21', '09:30:00', 35, 1),
(54, '2015-02-24', '09:30:00', 25, 1),
(55, '2015-02-24', '10:00:00', 27, 1),
(56, '2015-02-24', '11:00:00', 36, 1),
(57, '2015-02-20', '13:00:00', 13, 1),
(58, '2015-02-20', '13:00:00', 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `DONO`
--

DROP TABLE IF EXISTS `DONO`;
CREATE TABLE IF NOT EXISTS `DONO` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `cep` int(8) unsigned zerofill DEFAULT NULL,
  `cidade` varchar(200) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `endereco` varchar(300) DEFAULT NULL,
  `bairro` varchar(150) DEFAULT NULL,
  `numero` int(6) DEFAULT NULL,
  `complemento` varchar(300) DEFAULT NULL,
  `tel_fixo` varchar(15) DEFAULT NULL,
  `tel_cel` varchar(15) NOT NULL,
  `email` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DONO`
--

INSERT INTO `DONO` (`id`, `nome`, `cep`, `cidade`, `uf`, `country`, `endereco`, `bairro`, `numero`, `complemento`, `tel_fixo`, `tel_cel`, `email`) VALUES
(1, 'Yolanda', 00000000, '', '', '', '', '', 0, '', '(19) 3899-1464', '', ''),
(2, 'Fernando Amigão', 00000000, '', '', '', '', '', 0, '', '(19) 3899-2575', '', ''),
(3, 'Murilo', 00000000, '', '', '', '', '', 0, '', '', '(19) 9981-15968', ''),
(4, 'Juliana Truzzi', 00000000, '', '', '', '', '', 0, '', '(19) 3899-1275', '', ''),
(5, 'Marcos Kaloy', 00000000, '', '', '', '', '', 0, '', '(19) 3899-2204', '', ''),
(6, 'Thalita', 00000000, '', '', '', '', '', 0, '', '(19) 0000-0000', '', ''),
(7, 'Marceli / Diogo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(8, 'Maria / Menino Jesus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(9, 'Zé Vicente', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 9972-8918', '', NULL),
(10, 'Ana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 9961-7709', '', NULL),
(11, 'Deyse Aguiar', 00000000, '', '', '', '', '', 0, '', '(19) 0000-0000', '', ''),
(12, 'Nacy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 3899-1229', '', NULL),
(13, 'Fernando ramazine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 3899-1639', '', NULL),
(14, 'Thailine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(15, 'Vanessa Combustop', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(16, 'Irmã da Sandra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(17, 'Amanda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(18, 'Aninha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(19, 'Maria José', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '(19) 9984-10475', NULL),
(20, 'Renata', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '(19) 9967-58887', NULL),
(21, 'Nadia Tedeschi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(22, 'Zé Enéias', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '(19) 9971-22636', NULL),
(23, 'Bia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(24, 'Menino Jesus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(25, 'Paola Tucci', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '(19) 9754-60918', NULL),
(26, 'Ana (Departamento de Esportes)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(27, 'Vizinha da Belinha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL),
(28, 'Carla Valente', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '(19) 9963-87266', NULL),
(29, 'Fernanda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 3899-2568', '', NULL),
(30, 'Rogerio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(19) 0000-0000', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `FATURA`
--

DROP TABLE IF EXISTS `FATURA`;
CREATE TABLE IF NOT EXISTS `FATURA` (
  `id` int(11) NOT NULL,
  `total` decimal(5,2) NOT NULL,
  `desconto` decimal(5,2) NOT NULL,
  `fk_dono` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `FATURA`
--

INSERT INTO `FATURA` (`id`, `total`, `desconto`, `fk_dono`) VALUES
(1, '25.00', '0.00', 14),
(2, '20.00', '0.00', 3),
(3, '25.00', '0.00', 8),
(4, '90.00', '0.00', 5),
(5, '45.00', '0.00', 12),
(6, '35.00', '0.00', 18),
(7, '25.00', '0.00', 16),
(8, '25.00', '0.00', 20),
(9, '20.00', '0.00', 21),
(10, '25.00', '0.00', 14),
(11, '25.00', '0.00', 28),
(12, '25.00', '0.00', 19),
(13, '20.00', '0.00', 10),
(14, '45.00', '0.00', 22),
(15, '25.00', '0.00', 29),
(16, '25.00', '0.00', 19),
(17, '60.00', '0.00', 25),
(18, '25.00', '0.00', 26),
(19, '25.00', '0.00', 26),
(20, '20.00', '0.00', 17),
(21, '45.00', '0.00', 12),
(22, '45.00', '0.00', 13),
(23, '45.00', '0.00', 13),
(24, '45.00', '0.00', 13),
(25, '45.00', '0.00', 13),
(26, '45.00', '0.00', 13),
(27, '45.00', '0.00', 13),
(28, '45.00', '0.00', 13),
(29, '20.00', '10.00', 30);

-- --------------------------------------------------------

--
-- Table structure for table `PAGAMENTO`
--

DROP TABLE IF EXISTS `PAGAMENTO`;
CREATE TABLE IF NOT EXISTS `PAGAMENTO` (
  `id` int(11) NOT NULL,
  `id_agenda` int(11) NOT NULL,
  `id_dono` int(11) NOT NULL,
  `fk_fatura` int(11) DEFAULT NULL,
  `total` decimal(5,2) NOT NULL,
  `desconto` decimal(5,2) NOT NULL,
  `pago` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: pago',
  `data_pg` datetime DEFAULT NULL,
  `data_include` datetime NOT NULL COMMENT 'Data que foi incluído para pagamento'
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PAGAMENTO`
--

INSERT INTO `PAGAMENTO` (`id`, `id_agenda`, `id_dono`, `fk_fatura`, `total`, `desconto`, `pago`, `data_pg`, `data_include`) VALUES
(1, 1, 5, 4, '45.00', '0.00', 1, '2015-02-12 17:56:03', '2015-02-12 08:27:36'),
(2, 2, 5, 4, '45.00', '0.00', 1, '2015-02-12 17:56:03', '2015-02-12 08:27:41'),
(3, 4, 7, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 08:33:16'),
(4, 5, 7, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 08:33:26'),
(5, 13, 11, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:23:57'),
(6, 11, 11, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:24:01'),
(7, 12, 11, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:24:05'),
(8, 6, 7, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:24:12'),
(9, 7, 8, 3, '25.00', '0.00', 1, '2015-02-12 17:55:15', '2015-02-12 10:24:16'),
(10, 21, 13, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:30:26'),
(11, 22, 13, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:30:29'),
(12, 24, 13, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:30:36'),
(13, 23, 13, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 10:30:39'),
(14, 25, 14, 1, '25.00', '0.00', 1, '2015-02-12 11:23:37', '2015-02-12 11:23:28'),
(15, 17, 3, 2, '20.00', '0.00', 1, '2015-02-12 13:16:54', '2015-02-12 13:16:47'),
(16, 3, 6, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 16:01:11'),
(17, 8, 9, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 16:01:17'),
(18, 9, 9, NULL, '0.00', '0.00', 0, NULL, '2015-02-12 16:01:20'),
(19, 10, 10, 13, '20.00', '0.00', 1, '2015-02-19 16:10:32', '2015-02-12 16:01:26'),
(20, 18, 12, 21, '45.00', '0.00', 1, '2015-03-05 09:27:07', '2015-02-12 17:54:45'),
(21, 26, 15, 5, '20.00', '0.00', 1, '2015-02-13 15:24:31', '2015-02-13 15:24:03'),
(22, 27, 15, 5, '25.00', '0.00', 1, '2015-02-13 15:24:31', '2015-02-13 15:24:08'),
(23, 14, 11, NULL, '0.00', '0.00', 0, NULL, '2015-02-13 15:27:05'),
(24, 15, 11, NULL, '0.00', '0.00', 0, NULL, '2015-02-13 15:27:09'),
(25, 19, 13, NULL, '0.00', '0.00', 0, NULL, '2015-02-13 15:27:17'),
(26, 20, 13, NULL, '0.00', '0.00', 0, NULL, '2015-02-13 15:27:21'),
(27, 29, 17, 20, '20.00', '0.00', 1, '2015-03-05 09:26:57', '2015-02-14 09:15:07'),
(28, 30, 18, 6, '35.00', '0.00', 1, '2015-02-18 09:05:20', '2015-02-18 09:05:09'),
(29, 28, 16, 7, '25.00', '0.00', 1, '2015-02-18 09:05:43', '2015-02-18 09:05:36'),
(30, 32, 20, 8, '25.00', '0.00', 1, '2015-02-18 09:06:07', '2015-02-18 09:05:58'),
(31, 33, 21, 9, '20.00', '0.00', 1, '2015-02-18 09:06:29', '2015-02-18 09:06:20'),
(32, 47, 26, 19, '25.00', '0.00', 1, '2015-03-05 09:26:48', '2015-02-18 09:16:36'),
(33, 48, 26, 18, '25.00', '0.00', 1, '2015-03-05 09:26:43', '2015-02-18 09:16:45'),
(34, 46, 25, 17, '60.00', '0.00', 1, '2015-03-05 09:26:37', '2015-02-18 09:16:52'),
(35, 49, 27, NULL, '0.00', '0.00', 0, NULL, '2015-02-18 09:19:21'),
(36, 31, 19, 12, '25.00', '0.00', 1, '2015-02-19 16:10:04', '2015-02-19 16:08:54'),
(37, 52, 28, 11, '25.00', '0.00', 1, '2015-02-19 16:10:00', '2015-02-19 16:09:02'),
(38, 37, 7, NULL, '0.00', '0.00', 0, NULL, '2015-02-19 16:09:08'),
(39, 36, 14, 10, '25.00', '0.00', 1, '2015-02-19 16:09:51', '2015-02-19 16:09:14'),
(40, 35, 6, NULL, '0.00', '0.00', 0, NULL, '2015-02-19 16:09:21'),
(41, 38, 22, 14, '45.00', '0.00', 1, '2015-02-24 08:57:12', '2015-02-24 08:57:02'),
(42, 42, 15, NULL, '0.00', '0.00', 0, NULL, '2015-02-24 09:01:47'),
(43, 44, 23, NULL, '0.00', '0.00', 0, NULL, '2015-02-24 09:01:53'),
(44, 57, 11, NULL, '0.00', '0.00', 0, NULL, '2015-02-24 09:02:33'),
(45, 58, 11, NULL, '0.00', '0.00', 0, NULL, '2015-02-24 09:02:33'),
(46, 43, 15, NULL, '0.00', '0.00', 0, NULL, '2015-02-24 09:02:33'),
(47, 51, 13, NULL, '0.00', '0.00', 0, NULL, '2015-02-24 09:02:50'),
(48, 50, 13, 28, '45.00', '0.00', 1, '2015-03-05 09:28:17', '2015-02-24 09:02:54'),
(49, 53, 29, 15, '25.00', '0.00', 1, '2015-02-24 09:03:14', '2015-02-24 09:03:03'),
(50, 34, 5, NULL, '0.00', '0.00', 0, NULL, '2015-03-05 09:25:19'),
(51, 54, 19, 16, '25.00', '0.00', 1, '2015-03-05 09:26:15', '2015-03-05 09:25:27'),
(52, 55, 21, NULL, '0.00', '0.00', 0, NULL, '2015-03-05 09:25:36'),
(53, 56, 30, 29, '20.00', '10.00', 1, '2016-06-07 14:53:43', '2015-03-05 09:25:40');

-- --------------------------------------------------------

--
-- Table structure for table `PET`
--

DROP TABLE IF EXISTS `PET`;
CREATE TABLE IF NOT EXISTS `PET` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `genero` varchar(1) NOT NULL COMMENT 'M: Macho; F:Fêmea',
  `fk_raca` int(11) NOT NULL,
  `fk_dono` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PET`
--

INSERT INTO `PET` (`id`, `nome`, `genero`, `fk_raca`, `fk_dono`) VALUES
(1, 'Farofa', 'M', 39, 1),
(2, 'Biscoito', 'M', 39, 2),
(3, 'Kimera', 'F', 37, 3),
(4, 'Dumdum', 'F', 81, 4),
(5, 'Raika', 'F', 25, 4),
(6, 'Jaz', 'M', 25, 5),
(7, 'Yuri', 'M', 49, 6),
(8, 'Max', 'M', 49, 7),
(9, 'Jony', 'M', 5, 8),
(10, 'Chiva', 'F', 49, 9),
(11, 'Tesalia', 'F', 49, 9),
(12, 'Silk', 'F', 49, 10),
(13, 'Fofinho', 'M', 39, 11),
(14, 'Belinha', 'F', 39, 11),
(15, 'Hulk', 'M', 54, 11),
(16, 'King', 'M', 27, 12),
(17, 'Kevyn', 'M', 81, 13),
(18, 'Jady', 'F', 81, 13),
(19, 'Moly', 'F', 54, 14),
(20, 'Tica', 'F', 81, 15),
(21, 'Frida', 'F', 81, 15),
(22, 'Duda', 'F', 28, 16),
(23, 'Mel', 'F', 81, 17),
(24, 'Buba', 'M', 17, 18),
(25, 'Menino', 'M', 13, 19),
(26, 'Marmota', 'M', 50, 20),
(27, 'Nicky', 'M', 49, 21),
(28, 'Mai', 'F', 17, 22),
(29, 'mel', 'F', 25, 23),
(30, 'Jonhy', 'M', 5, 24),
(31, 'Tufão', 'M', 25, 25),
(32, 'Nico', 'M', 49, 26),
(33, 'Mel', 'F', 81, 27),
(34, 'Lucky', 'M', 49, 28),
(35, 'Luna', 'F', 81, 29),
(36, 'Belinha', 'F', 5, 30);

-- --------------------------------------------------------

--
-- Table structure for table `RACAS`
--

DROP TABLE IF EXISTS `RACAS`;
CREATE TABLE IF NOT EXISTS `RACAS` (
  `id` int(11) NOT NULL,
  `raca` varchar(50) NOT NULL,
  `tipo_animal` varchar(1) NOT NULL COMMENT '(C)achorro; (G)ato'
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RACAS`
--

INSERT INTO `RACAS` (`id`, `raca`, `tipo_animal`) VALUES
(1, 'Afghan Hound\n', 'C'),
(2, 'Airedale Terrier\n', 'C'),
(3, 'Akita Americano\n', 'C'),
(4, 'Akita Inu\n', 'C'),
(5, 'Basset\n', 'C'),
(6, 'Basset Hound\n', 'C'),
(7, 'Beagle\n', 'C'),
(8, 'Bichon Frisé\n', 'C'),
(9, 'Boxer\n', 'C'),
(10, 'Bulldog\n', 'C'),
(11, 'Bullmastiff\n', 'C'),
(12, 'Bull Terrier\n', 'C'),
(13, 'Chihuahua\n', 'C'),
(14, 'Chow Chow\n', 'C'),
(15, 'Cocker Americano\n', 'C'),
(16, 'Cocker Inglês\n', 'C'),
(17, 'Collie\n', 'C'),
(18, 'Dálmata\n', 'C'),
(19, 'Dobermann\n', 'C'),
(20, 'Dogue Alemão\n', 'C'),
(21, 'Fila Brasileiro\n', 'C'),
(22, 'Foxhound Inglês\n', 'C'),
(23, 'Fox Terrier Pelo Duro\n', 'C'),
(24, 'Fox Terrier Pelo Liso\n', 'C'),
(25, 'Golden Retriever\n', 'C'),
(26, 'Husky Siberiano\n', 'C'),
(27, 'Labrador\n', 'C'),
(28, 'Lhasa Apso\n', 'C'),
(29, 'Lulu da Pomerânia\n', 'C'),
(30, 'Maltês\n', 'C'),
(31, 'Mastiff\n', 'C'),
(32, 'Mastino Napoletano\n', 'C'),
(33, 'Pastor Alemão\n', 'C'),
(34, 'Pequinês\n', 'C'),
(35, 'Perdigueiro\n', 'C'),
(36, 'Pinscher\n', 'C'),
(37, 'Pitbull\n', 'C'),
(38, 'Pointer Inglês\n', 'C'),
(39, 'Poodle\n', 'C'),
(40, 'Rottweiler\n', 'C'),
(41, 'São Bernardo\n', 'C'),
(42, 'Schnauzer Gigante\n', 'C'),
(43, 'Schnauzer Miniatura\n', 'C'),
(44, 'Schnauzer Standard\n', 'C'),
(45, 'Setter Inglês\n', 'C'),
(46, 'Setter Irlandês\n', 'C'),
(47, 'Shar Pei\n', 'C'),
(48, 'Sheepdog\n', 'C'),
(49, 'Shih Tzu\n', 'C'),
(50, 'Spitz Alemão\n', 'C'),
(51, 'Staff Bull Terrier\n', 'C'),
(52, 'Teckel\n', 'C'),
(53, 'Terrier Brasileiro\n', 'C'),
(54, 'Yorkshire Terrier', 'C'),
(55, 'Abissínio\n', 'G'),
(56, 'American Shorthair\n', 'G'),
(57, 'Angorá\n', 'G'),
(58, 'Azul Russo\n', 'G'),
(59, 'Bengal\n', 'G'),
(60, 'Brazilian Shorthair\n', 'G'),
(61, 'British Shorthair\n', 'G'),
(62, 'Burmese\n', 'G'),
(63, 'Chartreux\n', 'G'),
(64, 'Cornish Rex\n', 'G'),
(65, 'Devon Rex\n', 'G'),
(66, 'Egyptian Mau\n', 'G'),
(67, 'European Shorthair\n', 'G'),
(68, 'Exótico\n', 'G'),
(69, 'Himalaio\n', 'G'),
(70, 'Maine Coon\n', 'G'),
(71, 'Munchkin\n', 'G'),
(72, 'Norwegian Forest\n', 'G'),
(73, 'Oriental\n', 'G'),
(74, 'Persa\n', 'G'),
(75, 'Ragdoll\n', 'G'),
(76, 'Sagrado da Birmânia\n', 'G'),
(77, 'Savannah\n', 'G'),
(78, 'Scottish Fold\n', 'G'),
(79, 'Siamês\n', 'G'),
(80, 'Sphynx', 'G'),
(81, 'Indefinida', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `REL_AGENDA_SERV`
--

DROP TABLE IF EXISTS `REL_AGENDA_SERV`;
CREATE TABLE IF NOT EXISTS `REL_AGENDA_SERV` (
  `id_agenda` int(11) NOT NULL,
  `id_servico` int(11) NOT NULL,
  `id_pet` int(11) NOT NULL,
  `valor_servico` decimal(5,2) NOT NULL,
  `realizado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `REL_AGENDA_SERV`
--

INSERT INTO `REL_AGENDA_SERV` (`id_agenda`, `id_servico`, `id_pet`, `valor_servico`, `realizado`) VALUES
(1, 4, 6, '45.00', 1),
(2, 4, 6, '45.00', 1),
(3, 1, 7, '20.00', 1),
(4, 1, 8, '20.00', 1),
(5, 1, 8, '20.00', 1),
(6, 1, 8, '20.00', 1),
(7, 2, 9, '25.00', 1),
(8, 1, 10, '20.00', 1),
(9, 1, 11, '20.00', 1),
(10, 1, 12, '20.00', 1),
(11, 2, 13, '25.00', 1),
(12, 2, 14, '25.00', 1),
(13, 6, 15, '40.00', 1),
(14, 2, 13, '25.00', 1),
(15, 2, 14, '25.00', 1),
(17, 1, 3, '20.00', 1),
(18, 4, 16, '45.00', 1),
(19, 4, 17, '45.00', 1),
(20, 4, 18, '45.00', 1),
(21, 4, 17, '45.00', 1),
(22, 4, 18, '45.00', 1),
(23, 4, 17, '45.00', 1),
(24, 4, 18, '45.00', 1),
(25, 2, 19, '25.00', 1),
(26, 1, 20, '20.00', 1),
(27, 2, 21, '25.00', 1),
(28, 2, 22, '25.00', 1),
(29, 1, 23, '20.00', 1),
(30, 3, 24, '35.00', 1),
(31, 2, 25, '25.00', 1),
(32, 2, 26, '25.00', 1),
(33, 1, 27, '20.00', 1),
(34, 4, 6, '45.00', 1),
(35, 1, 7, '20.00', 1),
(36, 2, 19, '25.00', 1),
(37, 1, 8, '20.00', 1),
(38, 4, 28, '45.00', 1),
(39, 2, 13, '25.00', 0),
(40, 2, 14, '25.00', 0),
(42, 1, 20, '20.00', 1),
(43, 2, 21, '25.00', 1),
(44, 3, 29, '35.00', 1),
(45, 2, 30, '25.00', 0),
(46, 8, 31, '60.00', 1),
(47, 2, 32, '25.00', 1),
(48, 2, 32, '25.00', 1),
(49, 9, 33, '60.00', 1),
(50, 4, 17, '45.00', 1),
(51, 4, 18, '45.00', 1),
(52, 2, 34, '25.00', 1),
(53, 2, 35, '25.00', 1),
(54, 2, 25, '25.00', 1),
(55, 1, 27, '20.00', 1),
(56, 1, 36, '20.00', 1),
(57, 2, 13, '25.00', 1),
(58, 2, 14, '25.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `REL_DONO_PET`
--

DROP TABLE IF EXISTS `REL_DONO_PET`;
CREATE TABLE IF NOT EXISTS `REL_DONO_PET` (
  `id_pet` int(11) NOT NULL,
  `id_dono` int(11) NOT NULL,
  `data` date NOT NULL,
  `removido` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Removido'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `REL_DONO_PET`
--

INSERT INTO `REL_DONO_PET` (`id_pet`, `id_dono`, `data`, `removido`) VALUES
(1, 1, '2015-02-11', 0),
(2, 2, '2015-02-11', 0),
(3, 3, '2015-02-11', 0),
(4, 4, '2015-02-11', 0),
(5, 4, '2015-02-11', 0),
(6, 5, '2015-02-12', 0),
(7, 6, '2015-02-12', 0),
(8, 7, '2015-02-12', 0),
(9, 8, '2015-02-12', 0),
(10, 9, '2015-02-12', 0),
(11, 9, '2015-02-12', 0),
(12, 10, '2015-02-12', 0),
(13, 11, '2015-02-12', 0),
(14, 11, '2015-02-12', 0),
(15, 11, '2015-02-12', 0),
(16, 12, '2015-02-12', 0),
(17, 13, '2015-02-12', 0),
(18, 13, '2015-02-12', 0),
(19, 14, '2015-02-12', 0),
(20, 15, '2015-02-12', 0),
(21, 15, '2015-02-12', 0),
(22, 16, '2015-02-12', 0),
(23, 17, '2015-02-13', 0),
(24, 18, '2015-02-13', 0),
(25, 19, '2015-02-13', 0),
(26, 20, '2015-02-13', 0),
(27, 21, '2015-02-13', 0),
(28, 22, '2015-02-13', 0),
(29, 23, '2015-02-13', 0),
(30, 24, '2015-02-13', 0),
(31, 25, '2015-02-18', 0),
(32, 26, '2015-02-18', 0),
(33, 27, '2015-02-18', 0),
(34, 28, '2015-02-18', 0),
(35, 29, '2015-02-19', 0),
(36, 30, '2015-02-24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `SERVICOS`
--

DROP TABLE IF EXISTS `SERVICOS`;
CREATE TABLE IF NOT EXISTS `SERVICOS` (
  `id` int(11) NOT NULL,
  `servico` varchar(50) NOT NULL,
  `preco` decimal(4,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `SERVICOS`
--

INSERT INTO `SERVICOS` (`id`, `servico`, `preco`) VALUES
(1, 'Banho Pequeno', '20.00'),
(2, 'Banho Médio', '25.00'),
(3, 'Banho Grande', '35.00'),
(4, 'Banho Gigante', '45.00'),
(5, 'Tosa Completa Pequeno', '35.00'),
(6, 'Tosa Completa Médio', '40.00'),
(7, 'Tosa Completa Grande', '45.00'),
(8, 'Tosa Completa Gigante', '60.00'),
(9, 'Banho especial', '60.00'),
(10, 'Banho Gigante Raças Especiais', '50.00');

-- --------------------------------------------------------

--
-- Table structure for table `USERS`
--

DROP TABLE IF EXISTS `USERS`;
CREATE TABLE IF NOT EXISTS `USERS` (
  `UID` varchar(15) NOT NULL,
  `NOME` varchar(30) NOT NULL,
  `USER_PERM` varchar(4) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `USERS`
--

INSERT INTO `USERS` (`UID`, `NOME`, `USER_PERM`, `PASSWORD`) VALUES
('maiser', 'Maiser', '6777', 'mja@123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AGENDA`
--
ALTER TABLE `AGENDA`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_pet` (`fk_pet`);

--
-- Indexes for table `DONO`
--
ALTER TABLE `DONO`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `FATURA`
--
ALTER TABLE `FATURA`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PAGAMENTO`
--
ALTER TABLE `PAGAMENTO`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PET`
--
ALTER TABLE `PET`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_raca` (`fk_raca`), ADD KEY `fk_dono` (`fk_dono`);

--
-- Indexes for table `RACAS`
--
ALTER TABLE `RACAS`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `REL_AGENDA_SERV`
--
ALTER TABLE `REL_AGENDA_SERV`
  ADD KEY `id_agenda` (`id_agenda`), ADD KEY `id_servico` (`id_servico`), ADD KEY `id_pet` (`id_pet`);

--
-- Indexes for table `REL_DONO_PET`
--
ALTER TABLE `REL_DONO_PET`
  ADD UNIQUE KEY `id_pet` (`id_pet`), ADD KEY `id_dono` (`id_dono`);

--
-- Indexes for table `SERVICOS`
--
ALTER TABLE `SERVICOS`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `USERS`
--
ALTER TABLE `USERS`
  ADD PRIMARY KEY (`UID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AGENDA`
--
ALTER TABLE `AGENDA`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `DONO`
--
ALTER TABLE `DONO`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `FATURA`
--
ALTER TABLE `FATURA`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `PAGAMENTO`
--
ALTER TABLE `PAGAMENTO`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `PET`
--
ALTER TABLE `PET`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `RACAS`
--
ALTER TABLE `RACAS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `SERVICOS`
--
ALTER TABLE `SERVICOS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;

