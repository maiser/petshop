<?php

require_once('FormValidator.class.php');

/**
 * Classe para manipula��o dos campos dos formul�rios (input de formul�rios) e afins
 *
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 0.2
 */
class FormManipulator {

    //vari�veis que foram validadas (Array)
    private $validated_vars;

    /** Classe construtura iniciando o atributo como sendo um array* */
    public function __construct() {
        $this->validated_vars = array();
    }

    /**
     * Seta as vari�veis classe do formul�rio $this com
     * os valores $_GET ou $_POST 
     * 
     * @param $sanitize_tags Bool, se true converte tags e aspas em caracteres especiais
     * @param $remove_tags Bool, se true remove tags HTML
     * @param $encode_utf8 Bool, se true codifica com utf8
     */
    public function setFormClassVars($sanitize_tags = false, $remove_tags = false, $encode_utf8 = false) {
        $child = &$this; //objeto impl�cito que herdou esta classe
        $request = null;

        //se o m�todo de envio do formul�rio for POST ent�o $mehod recebe $_POST
        //caso contr�rio recebe $_GET
        if (strcmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }
        //obtem as vari�veis da classe que modela o formul�rio
        $class_vars = get_class_vars(get_class($child));


        //if(is_array($class_vars)){
        foreach ($class_vars as $var_name => $var_value) {

            //$request = $_CONTROL->getREQUESTVar($var_name);
            isset($method[$var_name]) ? $request = $method[$var_name] : $request = null;

            if ((!is_null($var_value) && !is_null($request))) {

                $value = Protection::addSlashes($request);

                if ($sanitize_tags == true) {
                    $value = Protection::sanitizeAllTags($value);
                }
                if ($remove_tags == true) {
                    $value = Protection::removeTags($value);
                }
                if ($encode_utf8 == true) {
                    $child->$var_name = Protection::encodeUTF8($value);
                } else {
                    //$child->$var_name = Protection::decodeUTF8($value);
                    $child->$var_name = $value;
                }
                continue;
            }
            if (is_null($var_value))
                $child->$var_name = null;
        }
        //}
    }

    /**
     * Seta as vari�veis classe do formul�rio $this com
     * os valores $_GET ou $_POST 
     * @param $SS A vari�vel que � uma inst�ncia de sess�o
     */
    public function setSessionFormVars(&$SS) {
        $child = &$this; //objeto impl�cito que herdou esta classe
        $request = null;

        //se o m�todo de envio do formul�rio for POST ent�o $mehod recebe $_POST
        //caso contr�rio recebe $_GET
        if (strcmp($_SERVER['REQUEST_METHOD'], 'POST') == 0)
            $method = $_POST;
        else
            $method = $_GET;

        //obtem as vari�veis da classe que modela o formul�rio
        $class_vars = get_class_vars(get_class($child));

        //if(is_array($class_vars)){
        foreach ($class_vars as $var_name => $var_value) {

            //$request = $_CONTROL->getREQUESTVar($var_name);
            isset($method[$var_name]) ? $request = $method[$var_name] : $request = null;

            if ((!is_null($var_value) && !is_null($request))) {
                $SS->set_var($var_name, $var_value);
                continue;
            }
            if (is_null($var_value))
                $SS->set_var($varname, null);
        }



        //$class_vars = get_class_vars(get_class($this));
        //foreach ($class_vars as $var_name => $var_value) {
        //echo ' NOME: '. $var_name . ' VALOR: ' . $this->$var_name.'<br>';
        //}
    }

    /**
     * Retorna a variavel de nome $var_name relativa � classe instanciada
     * @param $var_name O nome da vari�vel
     */
    public function getVar($var_name) {
        //return $this->validated_vars[$var_name];
        return $this->$var_name;
        //return $this->$var_name;
    }

    /**
     * Debug para o formul�rio modelado
     */
    public function form_dump() {
        //var_dump(get_class($this));
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $var_name => $var_value) {
            if (is_array($this->$var_name)) {
                echo $var_name .':'. print_r($this->$var_name)."<br/>";
            } else {
                echo $var_name . ':' . $this->$var_name . ' </br>';
            }
        }
    }

}

?>
