<?php

/**
 * Classe para tradu�ao de front end usando Resource Bundle
 * @author Maiser J. Alves (maiseralves@gmail.com)
 */
class Translator {

    //Resource Bundle Object
    private $rb;

    /**
     * Construtor da classe
     * @param $lang 'locale' forma bb_BB
     * @param $folderlocale Caminho para os arquivos de tradu�ao
     *
     */
    /* function Translator($lang="root", $folderlocale="./locale") {
      $this -> rb = new ResourceBundle($lang, $folderlocale);
      } */
    function Translator($lang = "pt_BR", $folderlocale = "./locale") {
        /*
          //Setando a linguagem para $lang
          putenv('LC_ALL='.$lang);
          setlocale(LC_ALL, $lang);

          //Especificando a localiza��o das tabelas de tradu��es
          bindtextdomain("myPHPApp", $folderlocale);

          // Escolhendo dominio
          textdomain("myPHPApp");

          // Tradu��es ser�o procuradas em: $folderlocale/$lang/LC_MESSAGES/myPHPApp.mo
         */
    }

    function getMes($n) {

        switch ($n) {
            case 1: $mes = 'Janeiro';
                break;
            case 2: $mes = 'Fevereiro';
                break;
            case 3: $mes = 'Mar&ccedil;o';
                break;
            case 4: $mes = 'Abril';
                break;
            case 5: $mes = 'Maio';
                break;
            case 6: $mes = 'Junho';
                break;
            case 7: $mes = 'Julho';
                break;
            case 8: $mes = 'Agosto';
                break;
            case 9: $mes = 'Setembro';
                break;
            case 10: $mes = 'Outubro';
                break;
            case 11: $mes = 'Novembro';
                break;
            case 12: $mes = 'Dezembro';
                break;
        }

        return($mes);
    }

    function getDia($n) {

        switch ($n) {
            case 0: $dia = 'Domingo';
                break;
            case 1: $dia = 'Segunda';
                break;
            case 2: $dia = 'Ter&ccedil;a';
                break;
            case 3: $dia = 'Quarta';
                break;
            case 4: $dia = 'Quinta';
                break;
            case 5: $dia = 'Sexta';
                break;
            case 6: $dia = 'S&aacute;bado';
                break;
        }

        return($dia);
    }

    /**
     * Traduz uma entrada dada por $key
     * @param $key:string Chave a ser traduzida
     */
    /* public function get($key) {
      $t = $this -> rb -> get($key);
      if($t){
      echo $t;
      }else{
      echo $key;
      }
      } */
    public function get($key) {
        //echo gettext("$key");
        echo $key;
    }

    /**
     * Traduz uma entrada dada por $key
     * @param $key:string Chave a ser traduzida
     * @return O parametro traduzido
     */
    /* public function getr($key) {
      $t = $this -> rb -> get($key);
      if($t){
      return $t;
      }else{
      return $key;
      }

      } */
    public function getr($key) {
        //return gettext("$key");
        return $key;
    }

    /**
     *
     * ICU - International Components for Unicode (ICU) libraries
     * http://www.icu-project.org/
     *
     * ICU4J - ICU for JAVA
     * ICU4C - ICU for C/C++
     *
     * The Internationalization Extension - Intl
     * API is Procedural and Object Oriented
     *
     * Intl common functions:
     *  intl_get_error_code() — returns the ICU error code from the last operation
     *  intl_get_error_message() — returns a textual description of the last error
     *  intl_is_failure(int $err) — checks if a given error code is a failure code
     *  intl_error_name(int $err) — returns the ICU internal name for a given error code
     *
     * Estrutura de uma Resource
     * root:table {
     *       usage:string { "Usage: genrb [Options] files" }
     *       version:int { 122 }
     *       errorcodes:array {
     *           :string { "Invalid argument" }
     *           :string { "File not found" }
     *        }
     *   }
     *
     * arquivo root � o default caso nao haja tradu��o
     * genrb -d folderlocale root.txt pt_BR.txt en_US.txt
     */
    private function t($rb) {

        foreach ($rb as $k => $v) {
            if (is_object($v)) {
                //var_dump($k);
                print_r($v);
                var_dump($k);
                t($v);
            } else {
                //if($v == $translate) return $v;
                //echo "$k => $v<br/>";
                var_dump($k . " " . $v);
            }
        }
    }

}

?>