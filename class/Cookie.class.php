<?php

/**
 * Classe para gerenciamento de cookies em PHP
 *
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 0.9.1
 */
class Cookie {

    /**
     * Validade do cookie em segundos a partir da data atual (0 para sess�o)
     */
    private $validade = 0;

    /**
     * Dom�nio onde existe o cookie
     */
    private $domain = "";

    /** 
     * Bool, se True o Cookie s� existe no http e n�o pode ser acessado via javaScript
     * Prote��o contra XSS normalmente.
     */
    private $http_only = true;

    /** 
     * Caminho no servidor para o qual o cookie estar� dispon�vel.
     * Se for definido o valor "/", ele estar� dispon�vel para todo dom�nio
     * especificado no par�metro dom�nio. O valor padr�o � o diret�rio corrente
     * a partir do qual o cookie foi definido. 
     */
    private $path = "";

    /** 
     * Bool, indica se o cookie deve ser enviado somente com a exist�ncia de https
     * Se True, o cookie ser� transmitido se a conex�o for segura (HTTPS)
     */
    private $secure_http = false;

    /**
     * Habilita exibi��o de mensagens para debug de vari�veis
     */
    private $verbose = false;

    /**
     * 
     * @param type $validade
     * @param $domain Dom�nio para o qual o cookie estar� dispon�vel
     */
    function __construct($validade = 0, $domain = "") {

        if ($validade != 0) {
            $this->validade = time() + $validade;
        }

        if ($domain === true) {
            $this->domain = filter_input(INPUT_SERVER, 'HTTP_HOST');
        } else {
            $this->domain = $domain;
        }
    }

    /**
     * Registra um cookie na m�quina cliente
     * @param $varname Nome do cookie
     * @param $valor Valor do cookie
     * @param $validade Tempo em segundos a partir da data de cria��o.
     *        Validade negativa implica cookie j� expirado. 
     *        (-1) para mesma validade setada na inst�ncia da classe. ($this->validade);
     *        (0) para validade da sess�o
     */
    public function setVar($varname, $valor, $validade = -1) {
        if ($validade == -1) {
            $validade = $this->validade; //validade do cookie igual a da classe
        } else if ($validade < -1 || $validade > 0) {
            $validade = $validade + time();
        } else {
            $validade = 0;
        }
        return setcookie($varname, $valor, $validade, $this->path, $this->domain, $this->secure_http, $this->http_only);
    }

    /**
     * Obt�m a vari�vel de cookie (j� setada) de nome $varname
     * @param $varname Nome da vari�vel cookie
     * @return O valor da vari�vel ou falso caso a vari�vel n�o exista!
     */
    public function getVar($varname) {
        if ($this->varIsset($varname)) {
            return filter_input(INPUT_COOKIE, $varname);
        } else {
            $this->msgDebug(__FUNCTION__ . ': ' . "\$_COOKIE[" . $varname . "], n&atilde;o existe!");
            return false;
        }
    }

    /**
     * Exclui um cookie da m�quina cliente caso ele exista
     * @param $varname Nome do cookie
     * @return Verdadeiro em caso de sucesso e falso cc.
     */
    public function unsetVar($varname) {
        if ($this->varIsset($varname)) {
            $this->setVar($varname, null);
            return true;
        } else {
            $this->msgDebug(__FUNCTION__ . ': ' . "\$_COOKIE[" . $varname . "], n&atilde;o existe!");
            return false;
        }
    }

    /**
     * Seta o cookie para o tempo m�ximo permitido (Nunca expira)
     * @param $varname Nome do cookie
     * @return Verdadeiro em caso de sucesso e falso cc.
     */
    public function noExpireVar($varname) {
        if ($this->varIsset($varname)) {
            $validade = time();
            if ($this->setVar($varname, $this->getVar($varname), $validade) == false) {
                $this->msgDebug(__FUNCTION__ . ': ' . "\$_COOKIE[" . $varname . "], n&atilde;o pode ser expirado!");
                return false;
            }
        }
    }

    /**
     * Exclui um cookie da m�quina cliente caso ele exista
     * @param $varname Nome do cookie
     * @return Verdadeiro em caso de sucesso e falso cc.
     */
    public function expireVar($varname, $validade = -2) {
        if ($this->varIsset($varname)) {
            if ($this->setVar($varname, $this->getVar($varname), $validade) == false) {
                $this->msgDebug(__FUNCTION__ . ': ' . "\$_COOKIE[" . $varname . "], n&atilde;o pode ser expirado!");
                return false;
            }
        }
    }

    /**
     * 
     * @param type $secs Tempo padr�o da classe em segundos para os cookies criados com setVar
     */
    public function setCookieTime($secs = 0) {
        $this->validade = $secs + time();
    }

    /**
     * 
     * @return type O tempo de validade do cookie da inst�ncia
     */
    public function getCookieTime() {
        return $this->validade;
    }

    /**
     * 
     * @param type $bool Acesso ao cookie somente via Http?
     */
    public function setCookieHttpOnly($bool = true) {
        $this->http_only = $bool;
    }

    /**
     * 
     * @param type $bool Transmite o cookie somente em HTTPS?
     */
    public function setCookieHttps($bool = true) {
        $this->secure_http = $bool;
    }

    /**
     * 
     * @param type $path O caminho
     */
    public function setCookiePath($path = "/") {
        $this->path = $path;
    }

    /**
     * 
     * @param type $domain O dom�nio
     */
    public function setCookieDomain($domain) {
        $this->domain = $domain;
    }

    /**
     * Verifica se a vari�vel  $_COOKIE[$var] existe e possui valor
     * @param $varname O nome da vari�vel cookie
     * @return True caso $varname esteja setada. False caso contr�rio
     */
    public function varIsset($varname) {
        if (filter_input(INPUT_COOKIE, $varname, FILTER_DEFAULT, FILTER_NULL_ON_FAILURE)) {
            return true;
        } else {
            $this->msgDebug(__FUNCTION__ . ': ' . "\$_COOKIE[" . $varname . "], n&atilde;o est&aacute; setada!");
            return false;
        }
    }

    /**
     * Destr�i o cookie criado
     */
    public function destroy() {
        if (!empty($_COOKIE)) {
            foreach ($_COOKIE as $varname => $value) {
                $this->unsetVar($varname);
                $this->expireVar($varname);
            }
            return true;
        }
        return false;
    }

    /**
     * Destr�i todos os cookies criados exceto os listados no array par�metro
     * @param $list Array com o nome das vari�veis cookie para destruir
     * @param $destroy_list Bool Se false destr�i todos os cookies que n�o est�o em $list
     */
    public function destroyVars($list = array(), $destroy_list = true) {
        $count = count($list);
        $c = 0;
        if ($count > 0) {
            if (!empty($_COOKIE)) {

                foreach ($_COOKIE as $varname => $value) {
                    foreach ($list as $destroy) {
                        if ($destroy_list == true) {
                            if ($destroy == $varname) {

                                $this->unsetVar($varname);
                                $this->expireVar($varname);
                            }
                        } else {
                            if ($destroy != $varname) {
                                $c++;
                            }
                        }
                    }

                    if ($c == $count) {
                        $this->unsetVar($varname);
                        $this->expireVar($varname);
                    }
                    $c = 0;
                }
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Habilita ou desabilita verbose para debug de vari�veis
     * @param $bool Valor booleano true habilita verbose
     */
    public function enableVerbose($bool = true) {
        $this->verbose = $bool;
    }

    /**
     * Exibe mensagem caso enable_verbose esteja em true;
     * @param $mensagem A mensagem de debug;
     */
    private function msgDebug($mensagem) {
        if ($this->verbose == true) {
            echo $mensagem . "<br/>\r\n";
        }
    }

    public function dump() {
        print_r($_COOKIE);
        echo '<br/>';
    }

    public function dumpVars() {
        print_r(get_object_vars($this));
        echo '<br/>';
    }

}

?>
