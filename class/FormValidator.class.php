<?php
/** 
 * Classe para validação de dados (input de formulários) e afins
 * 
 * Referências para os métodos de validação: 
 * 	 ==> http://hungred.com/useful-information/php-form-validation-snippets/
 * 
 * @author Maiser José Alves Oliva (maiseralves@gmail.com)
 * @version 0.2
 */
class FormValidator{
	
	private $vars;
	
	//numero de campos que foram validados
	private $total_validated;
	
	//numero de campos totais
	private $total_fields;
	
	//array contendo valor 0 ou 1 se o campo foi validado
	public $result_validated;
	
	//mensagem de erros
	private $msg_error;
	
	/** Função construtora da classe */
	function __construct(){
		$this->total_validated = 0;
		$this->total_fields = 0;
		//$this->makeSafeVar($_POST);
		//$this->makeSafeVar($_GET);
		//$this->makeSafeVar($_REQUEST);
	}
	
	
	/**
	 * Adiciona uma variável para validação
	 * 
	 * @param $value = O valor a ser válidado
	 * @param $type = O tipo de valor que se deseja validar
	 * Possíveis tipos:
	 * 		'numeric'
	 * 		'string'
	 * 		'email'
	 * 		'data'
	 * 		'alfanum'
	 * 		'ip'
	 * 		'cnpj'
	 * 		'cpf'
	 * 		'creditcard'
	 * @param $name Nome da variável a ser validada
	 * @param $msg_error Mensagem caso $value não seja válido
	 */
	public function addVar($value, $type, $name, $msg_error=""){
		$this->result_validated[$name] = array($this->validateVar($value, $type), $msg_error);
	}
	
	/**
	 * Valida $value de acordo com $type
	 * @param $value O valor que ser quer validar
	 * @param $type O tipo a ser validado
	 */
	private function validateVar($value, $type){
		switch($type){
			case 'numeric':{
				return $this->numeric($value);
			}
			case 'string':{
				return $this->string($value);
			}
			case 'email':{
				return $this->email($value);
			}
			case 'data':{
				return $this->data($value);
			}
			case 'alfanum':{
				return $this->alfanum($value);
			}
			case 'ip':{
				return $this->ip($value);
			}
			case 'cnpj':{
				return $this->cnpj($value);
			}
			case 'cpf':{
				return $this->cpf($value);
			}
			case 'creditcard':{
				return $this->creditcard($value);
			}
			default: return 0;
		}
	}

	/**
	 * Nao esquecer de colocar esta funcao no retorno booleano das funcoes
	 */
	private function setValidated($value){
		//echo "VALOR: ".$value."<br />";
		if($value) $value = 1;
		else $value = 0;
		
		//$this->result_validated[] = $value;
		$this->total_fields++;
		$this->total_validated = $this->total_validated + $value;
		return $value;
	}
	
	/**
	 * Verifica se o formulario corrente é válido
	 * @param $opt Se verdadeiro exibe um dump da validação
	 */
	public function isValidForm(&$fields, $opt = false){
		if($opt == true) $this->dump_eval();
	
		$fields = $this->getErrorVars();//retorna por referencia os campos
		
		if($this->total_validated == $this->total_fields){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Verifica se o formulario corrente é válido
	 * @param $opt Se verdadeiro exibe um dump da validação
	 */
	public function isInvalidForm($opt = false){
		if($opt == true) $this->dump_eval();
		//retorna por referencia os campos
		
		//se o numero de campos validados confere com o totla de camops a serem validados
		if($this->total_validated == $this->total_fields){
			return false;//form valido
		}else{
			//retorna os campos com problemas
			$fields = $this->getErrorVars();
			return $fields;
		}
	}
	
	
	/**
	 * Obtem os campos e os erros
	 * @return $error['txt_nome_campo'] = ''
	 */
	public function getErrorVars(){
		foreach($this->result_validated as $key => $value){
			if($value[0] == 0){
				$error[$key] = $value[1];
			}//else{
				//$error[$key] = $value[0];
			//}
		}
		/* 
		unset($error);
		$error['numero'] = 'teste';
		$error['cpf'] = 'teste';
		$error['complemento'] = 'teste';*/
		if(isset($error)) return $error;
		else return false;
	}
	
	
	/**
	 * 
	 */
	public function checkVars(){
		var_dump($this->result_validated);
	}
	
	/**
	 * Remove a mascara de uma variável $masked_var
	 * @param $masked_var Um variável mascarada
	 * @param $chars Os caracteres a serem removidos da máscara
	 */
	public function removeMask(&$masked_var, $chars){
		if(is_array($chars)){
			foreach($chars  as $ch){
				$masked_var = str_replace($ch,'',$masked_var);
			}
		}else{
			$masked_var = str_replace($ch,'',$masked_var);
		}
		return $masked_var;
	}
	
	/**
	 * Valida em email
	 * @param $email String para o email
	 */
	public function email($email){
		return $this->setValidated(preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $email));
		//return filter_var($email, FILTER_VALIDATE_EMAIL); //php version >= 5.2
	}
	
	/**
	 * Valida data nos seguintes formatos
	 * MM-DD-YYYY or MM-DD-YY or FF.FF.FF
	 * @param $date String para data
	 */
	public function data($date){
		//This is a date format MM-DD-YYYY or MM-DD-YY validation which validate from year 0000-9999.
		// 05/12/2109 ou 05-12-0009 ou 05.12.9909 ou 05.12.99
		return $this->setValidated(preg_match('/^\d{1,2}\/\d{1,2}\/\d{4}$/', $date));
	    //return $this->setValidated(preg_match('/^((0?[1-9]|1[012])[-\/.](0?[1-9]|[12][0-9]|3[01])[-\/.][0-9]?[0-9]?[0-9]{2})*$/', $date));
	}
	
	/**
	 * Valida um endereço ip
	 * @param $ip Um endereçp ip
	 */
	public function ip($ip){
		 return $this->setValidated(preg_match('/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',$ip));
		//return filter_var($ip, FILTER_VALIDATE_IP); // php version > 5.0 
	}
	
	/**
	 * Valida somente números (float, integer, double)
	 * @param $value Um valor qualquer
	 */
	public function numeric($value){
		/*is_ double($value);
	    is_ float($value);
	    is_ int($value);
	    is_ integer($value);
		return $this->setValidated(is_numeric($value));
		*/
		return $this->setValidated(is_numeric($value));
	    //return $this->setValidated(preg_match('/[0-9]/'));
	}
	
	/**
	 * Valida somente strings
	 * @param $str Uma string
	 */
	public function string($str){
		return $this->setValidated(preg_match('/^[A-Za-z\s ]+$/', $str));
	}
	
	/**
	 * Valida somente string alfa numérica (letras e números)
	 * @$string Uma string
	 */
	public function alfanum($string){
		 return $this->setValidated(ctype_alnum ($string));
	}

	public function creditcard($cc){
		 //eg. 718486746312031
	    return $this->setValidated(preg_match('/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})$/', $cc));
	}
	
	//-----------------------------------------------------
	//Funcao: validaCNPJ($cnpj)
	//Sinopse: Verifica se o valor passado é um CNPJ válido
	// Retorno: Booleano
	// Autor: Gabriel Fróes - www.codigofonte.com.br
	//-----------------------------------------------------
	/**
	 * Valida o número do Cadastro Nacional de Pessoa Jurídica
	 * @param $cnpj = Um número de cnpj válido
	 */
	public function cnpj($cnpj) {
		$this->sanitizeNumber($cnpj);
		
	    if (strlen($cnpj) <> 18) return 0; 
	    $soma1 = ($cnpj[0] * 5) + 
	
	    ($cnpj[1] * 4) + 
	    ($cnpj[3] * 3) + 
	    ($cnpj[4] * 2) + 
	    ($cnpj[5] * 9) + 
	    ($cnpj[7] * 8) + 
	    ($cnpj[8] * 7) + 
	    ($cnpj[9] * 6) + 
	    ($cnpj[11] * 5) + 
	    ($cnpj[12] * 4) + 
	    ($cnpj[13] * 3) + 
	    ($cnpj[14] * 2); 
	    $resto = $soma1 % 11; 
	    $digito1 = $resto < 2 ? 0 : 11 - $resto; 
	    $soma2 = ($cnpj[0] * 6) + 
	
	    ($cnpj[1] * 5) + 
	    ($cnpj[3] * 4) + 
	    ($cnpj[4] * 3) + 
	    ($cnpj[5] * 2) + 
	    ($cnpj[7] * 9) + 
	    ($cnpj[8] * 8) + 
	    ($cnpj[9] * 7) + 
	    ($cnpj[11] * 6) + 
	    ($cnpj[12] * 5) + 
	    ($cnpj[13] * 4) + 
	    ($cnpj[14] * 3) + 
	    ($cnpj[16] * 2); 
	    $resto = $soma2 % 11; 
	    $digito2 = $resto < 2 ? 0 : 11 - $resto; 
		
		if(($cnpj[16] == $digito1) && ($cnpj[17] == $digito2))
		$return = true;
		
	    return  $this->setValidated($return); 
	} 
	
	/** Valida um CPF
	 * @param $CPF = um número de cpf
	 */
	public function cpf($cpf){
		//echo "AQUI";
	    if(preg_match("/^(\d{3}\.){2}\d{3}-\d{2}$/",$cpf) || preg_match("/\d{11}$/",$cpf))
	    {
			$this->sanitizeNumber($cpf);
			
	        if(substr_count($cpf,substr($cpf,0,1)) >= 11)
	        {
	            return $this->setValidated(false);
	        }
	        else
	        {
	            $cpf_temp = substr($cpf,0,9);
	            $soma1 = 0;
	            $soma2 = 0;
	            for($i = 1; $i<= 9; $i++)
	            {
	                $soma1 += intval(substr($cpf,$i-1,1)) * $i;
	            }
	            $dv1 = $soma1 % 11;
	            if($dv1 == 10) { $dv1 = 0; }
	            $cpf_temp = $cpf_temp.$dv1;
	            for($i = 0; $i<=9;$i++)
	            {
	                $soma2 += intval(substr($cpf_temp,$i,1)) * $i;
	            }
	            $dv2 = $soma2 % 11;
	            if($dv2 == 10) { $dv2 = 0; }
	            $cpf_final = $cpf_temp.$dv2;
	            if(strcmp($cpf,$cpf_final))
	            {
	                return  $this->setValidated(false);
	            }
	            else
	            {
	                return  $this->setValidated(true);
	            }
	        }      
	    }
	    else
	    {
	        return $this->setValidated(false);
	    }
	}
	


	/** Permite somente letras e espaços em uma string */
	public function sanitizeString($str){
		 //letters and space only
	    return $this->setValidated(preg_replace('/[^A-Za-z\s ]/', '', $str));
		//return filter_var($str, FILTER_SANITIZE_STRIPPED); // only 'String' is allowed eg. '<br>HELLO</br>' => 'HELLO' //php version >= 5.2
	}
	
	/** Permite somente letras e números em uma estring */
	public function sanitizeAlphaNum($string){
		return preg_replace('/[^a-zA-Z0-9]/', '', $string);
	}
	
	/** Permite somente números em uma string */
	public function sanitizeNumber(&$number){
		$number = preg_replace('/[^0-9]/', '', $number);
	}
	
	/** Valida uma url */
	public function sanitizeUrl($url){
		return filter_var($url, FILTER_SANITIZE_URL); //php version >= 5.2
	}
	
	/** Verficia se a string passada como parâmetro é uma senha forte */
	public function strongPassword($password){
		//must contain 8 characters, 1 uppercase, 1 lowercase and 1 number
	    return $this->setValidated(preg_match('/^(?=^.{8,}$)((?=.*[A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z]))^.*$/', $password));
	}
	
	/**
	 * Proteção contra XSS, JS e SQL injection por remoção de tags.
	 */
	public function makeSafeVar(&$str){
		$this->makeSafeVarByTags($str);
		$str = $this->makeSafeVarByQuotes($str);
	}
	
	
	/**
	 * Proteção contra XSS, JS e SQL injection por remoção de tags.
	 */
	private function makeSafeVarByTags(&$str){
		$callback = array($this, __CLASS__ .'::'. __FUNCTION__);
		return is_array($str) ? array_map($callback, $str) : str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES))));
	}
	
	/**
	 * Coloca barra no escape das aspas (duplas e simples)
	 */
	private function makeSafeVarByQuotes(&$str){
		$callback = array($this, __CLASS__ .'::'. __FUNCTION__);
		if (is_array($str)){
			return array_map($callback, $str);
		}else{
			return addslashes($str);
		}
	}
	
	/**
	 * Escapa variáveis da validação
	 */
	private function dump_eval(){
			if($this->total_fields != $this->total_validated){
				echo "ttf:". $this->total_fields. "</br>\n";
				echo "ttv:". $this->total_validated . "</br>\n";
				print_r($this->result_validated) ."</br>\n";
			}
	}
	
}
?>