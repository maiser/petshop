<?php

/**
 * Classe para gerenciamento de sess�es em PHP
 * Oferece suporte a criptografia de chaves e valores
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 0.9
 */
class Session {

    //nome e ID da sess�o (formato: 'nome = SID')
    private $SSID;
    //array que guarda informa��es sobre a sess�o criada
    /* [0] => data, [1] => ip, [2] => URI */
    private $info;
    private $verbose;

    /**
     * Contrutor da classe
     * @param $autostart Inicia a sess�o assim que o objeto � instanciado
     * @param $name Nome da sess�o
     */
    function Session($autostart = false, $name = "") {
        //nome da sess�o
        if ($name != "")
            $this->name($name);
        if ($autostart == true)
            $this->start();
    }

    /**
     * Registra as informa��es do cliente que est� usando a sess�o
     * as informa��es s�o atualizadas em toda requisi��o exceto
     */
    public function register_request() {
        $this->info['ip'] = $_SERVER['REMOTE_ADDR'];
        $this->info['url'] = $_SERVER['REQUEST_URI'];

        if ($this->var_isset("info")) {
            //$this->info['udata'] = date('dmYHis');
            //$this->info['cdata'] = $_SESSION["info"]["cdata"];
            //$this->update_var("info", $this->info);
        } else {
            //var_dump($this->info);
            $this->info['cdata'] = date('dmYHis');
            $this->set_var("info", $this->info);
        }
    }

    /**
     * Inicia ou restaura uma sess�o
     * Obs: se cookies estiverem sendo usados para armazenar a SID
     * ent�o esta fun��o deve ser chamada antes de qualquer sa�da produzida pelo browser
     * @return bool Verdadeiro em caso de sucesso na inicializa��o e falso caso contr�rio
     */
    public function start() {
        if (session_start()) {
            $this->SSID = session_id();
            return true;
        } else {
            $this->msg_debug("Problemas ao iniciar a sess�o!");
        }
        return false;
    }

    /**
     * Seta a ID da sess�o
     */
    public function set_id($id = NULL) {
        session_id($id);
        $this->SSID = $id;
        //echo session_id();
    }

    /**
     * Obt�m a ID da sess�o corrente
     * @return string ID da sess�o
     */
    public function get_id() {
        return $this->SSID;
    }

    /**
     * Envia a id da sess�o(*) para um cookie na m�quina cliente
     * � a maneira mais segura de recuperar os valores da sessao no servidor,
     * entretanto, se o cliente desabilitar os cookies haver� problemas.(**)
     * (*) Se no arquivo php.ini a entrada session.use_cookies = 1 ent�o esta
     * set_cookieID n�o precisa ser chamada pois a id da sess�o j� � enviada
     * para um cookie na m�quina cliente. O nome do cookie � session_name();
     * (**) Usa-se a propaga��o da ID da sess�o (SID) ou $this->SSID entre as
     * p�ginas para contornar o bloqueio de cookies;
     */
    public function set_cookieID() {
        //session_cache_expire()*60 calcula o valor em segundos do cookie
        //para que seja igual ao tempo de vida da sess�o
        setcookie(session_name(), $this->SSID, session_cache_expire() * 60);
    }

    /**
     * Obt�m a id da sess�o para um cookie na m�quina cliente
     * � a maneira mais segura de recuperar os valores da sessao no servidor,
     * entretanto, se o cliente desabilitar os cookies. Usa-se a propaga��o
     * da ID da sess�o (SID) ou $this->SSID entre as p�ginas
     */
    public function get_cookieID() {
        return $_COOKIE[session_name()];
    }

    /**
     * Seta um nome para a sess�o corrente
     */
    public function name($name) {
        if (!$this->exist())
            session_name($name);
    }

    /**
     * Registra uma vari�vel de sess�o de nome $varname e valor $valor
     * @param $varname Nome da vari�vel de sess�o
     * @param $value Valor da vari�vel criada
     * @return bool Verdadeiro se sucesso ao setar vari�vel e falso caso a vari�vel j� exista!
     */
    public function set_var($varname, $value, $replace=true) {
        if (!$this->var_isset($varname) || $replace) {
            $_SESSION[$varname] = $value;
            return true;
        } else {
            $this->msg_debug("\$_SESSION[" . $varname . "], j&aacute; existe!");
            return false;
        }
    }

    /**
     * Obtem uma vari�vel de sess�o (j� setada) de nome $varname e valor $valor
     * @param $varname Nome da vari�vel de sess�o
     * @return $_SESSION[$varname] O valor da vari�vel ou falso caso a vari�vel n�o exista!
     */
    public function get_var($varname) {
        if ($this->var_isset($varname)) {
            return $_SESSION[$varname];
        } else {
            $this->msg_debug("\$_SESSION[" . $varname . "], n&atilde;o existe!");
            return false;
        }
    }

    /**
     * Atualiza uma vari�vel de sess�o de nome $varname e valor $valor
     * @param $varname Nome da vari�vel de sess�o
     * @param $value Valor da vari�vel criada
     * @return Verdadeiro se sucesso ao atualizar vari�vel e falso caso a vari�vel n�o exista!
     */
    public function update_var($varname, $value) {
        if ($this->var_isset($varname)) {
            $_SESSION[$varname] = $value;
            return true;
        } else {
            $this->msg_debug("\$_SESSION[" . $varname . "], n&atilde;o existe!");
            return false;
        }
    }

    /**
     * Cria uma vari�vel de sess�o criptografada de nome $varname e valor $valor
     * @param $varname Nome da vari�vel de sess�o
     * @param $value Valor da vari�vel criada
     * @param $cryptvname Criptografa o nome da vari�vel usando crypt?
     * @param $cryptvalue Criptografar o valor da vari�vel usando crypt?
     * @param $md5 Criptografar nome ou valor tamb�m com md5? Somente se
     *             $cryptvname=true ou $cryptvalue=true;
     * @return Verdadeiro se sucesso ao setar vari�vel e falso caso a vari�vel j� exista!
     */
    public function set_secure_var($varname, $value, $cryptvname = false, $cryptvalue = false, $md5 = false) {
        if (!$this->var_isset($varname, $cryptvname, $md5)) {
            if ($cryptvname == true && $md5 == true)
                $varname = md5($varname);
            if ($cryptvname == true)
                $varname = crypt($varname);

            if ($cryptvalue == true && $md5 == true)
                $value = md5($value);
            if ($cryptvalue == true)
                $value = crypt($value);


            $_SESSION[$varname] = $value;
            return true;
        } else {
            $this->msg_debug("\$_SESSION[" . $varname . "], j&aacute; existe!");
            return false;
        }
    }

    /**
     * Obt�m uma vari�vel de sess�o criptografada (j� setada) de nome $varname e valor $valor
     * @param $varname Nome da vari�vel de sess�o
     * @param $value Valor da vari�vel criada
     * @param $cryptvname Criptografa o nome da vari�vel usando crypt?
     * @param $md5 Criptografar nome tamb�m com md5? Somente se $cryptvname=true
     * @return O valor da vari�vel encriptada ou falso caso a vari�vel n�o exista!
     */
    public function get_secure_var($varname, $cryptvname = false, $md5 = false) {

        $key = $this->var_isset($varname, $cryptvname, $md5);

        if ($key) {
            return $_SESSION[$key];
        } else {
            $this->msg_debug("\$_SESSION[" . $varname . "], n&atilde;o existe!");
            return false;
        }
    }

    /**
     * Atualiza uma vari�vel de sess�o de nome $varname e valor $valor
     * @param $varname Nome da vari�vel de sess�o
     * @param $value Valor da vari�vel criada
     * @param $cryptvname Criptografa o nome da vari�vel usando crypt?
     * @param $cryptvalue Criptografar o valor da vari�vel usando crypt?
     * @param $md5 Criptografar nome ou valor tamb�m com md5? Somente se
     *             $cryptvname=true ou $cryptvalue=true;
     * @return Verdadeiro se sucesso ao atualizar vari�vel e falso caso a vari�vel n�o exista!
     */
    public function update_secure_var($varname, $value, $cryptvname = false, $cryptvalue = false, $md5 = false) {
        if ($this->var_isset($varname)) {
            if ($cryptvname == true)
                $varname = crypt($varname);
            if ($cryptvname == true && $md5 == true)
                $varname = md5($varname);
            if ($cryptvalue == true)
                $value = crypt($value);
            if ($cryptvalue == true && $md5 == true)
                $value = md5($value);

            $_SESSION[$varname] = $value;
            return true;
        }else {
            $this->msg_debug("\$_SESSION[" . $varname . "], n&atilde;o existe!");
            return false;
        }
    }

    /**
     * Atualiza todas as (poss�veis) vari�veis da sess�o setadas atrav�s
     * de um array com chaves e valores. Somente as chaves
     * que corresponderem ao $array ser�o atualizadas.
     * @param $array Formato "key" => value
     * @return O n�mero de vari�veis atualizadas
     */
    public function update_vars($array) {
        $i = 0;
        foreach ($array as $key => $value) {
            if ($this->var_isset($key)) {
                $_SESSION[$key] = $value;
                $i++;
            }
        }
        return $i;
    }

    /**
     * Verifica se a vari�vel  $_SESSION[$var] existe e possui valor
     * @return True caso $varname esteja setada. Fale caso contr�rio
     */
    public function var_isset($varname, $crypt = false, $md5 = false) {

        if ($crypt == true || $md5 == true) {
            foreach ($_SESSION as $k => $v) {

                if ($crypt == true && $md5 == true) {
                    if (crypt(md5($varname), $k) == $k)
                        return $k;
                }else if ($crypt == true) {
                    if (crypt($varname, $k) == $k)
                        return $k;
                }else if ($md5 == true) {
                    if (isset($_SESSION[md5($varname)]))
                        return $k;
                }
            }
        }

        if (isset($_SESSION[$varname]))
            return true;
        else
            return false;
    }

    /**
     * Seta o tempo em minutos que uma sess�o expirar�
     * @param $time_min Tempo em minutos
     */
    public function expire($time_min = "180") {
        session_cache_expire($time_min);
    }

    /**
     * Verifica se a sess�o existe
     * @return True se sess�o existe. False cc.
     */
    public function exist() {
        if (!isset($_SESSION))
            return false;
        else
            return true;
    }

    /**
     * Destr�i a sess�o criada
     */
    public function destroy() {
        if ($this->exist()) {
            unset($_SESSION);
            session_destroy();
            return true;
        }
        return false;
    }

    /**
     * Obt�m informa��o sobre a vari�vel $this->info
     * que registra dados sobre o cliente e a cria��o do cookie.
     * @param $info_name Nome da chave do array info na qual deseja-se obter o valor
     * @return O conte�do da array de chave $info_name
     */
    public function get_request($info_name = "") {
        return $this->info[$info_name];
    }

    /**
     * Habilita ou desabilita verbose para debug de vari�veis
     * @param $bool Valor booleano true habilita verbose
     */
    public function enable_verbose($bool = true) {
        $this->verbose = $bool;
    }

    /**
     * Exibe mensagem caso enable_verbose esteja em true;
     * @param $mensagem A mensagem de debug;
     */
    private function msg_debug($mensagem) {
        if ($this->verbose == true)
            echo $mensagem . "<br/>\r\n";
    }

    /**
     * Debugger da classe
     */
    public function dump() {
        $this->msg_debug(get_class());
        $this->msg_debug("SessID:" . $this->SSID);
        echo "COOKIE SESSION ID: " . $this->get_cookieID() . "<br/>\r\n";
        echo "\r\n<dl>\r\n";

        foreach ($_SESSION as $k => $v) {

            echo "\t<dt>\$_SESSION[$k] = $v <dt/>\r\n";

            if (is_array($v)) {
                foreach ($v as $ka => $va) {
                    echo "\t\t<dd>\$_SESSION[$ka] = $va <dd/>\r\n";
                }
            }
        }
        echo "<dl/>\r\n";
    }

}

?>
