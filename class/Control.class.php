<?php

/*
 * Classe para controle das principais fun��es de um site
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 0.3
 */

class Control {

    //page content (body)
    public $site_page;
    //linguagem do site
    public $site_lang;
    //meta do site
    public $site_meta;
    //css referente a $site_page
    public $page_css;
    //js referente a $site_page
    public $page_js;
    //caminho relativo
    private $relative_path;
    //configuracoes
    private $config;

    /*
     * Funcao construtora
     */

    function __construct($relative_path) {
        require_once ($relative_path . 'include/Configure.php');
        $this->config = &$_CONF;      //configura��es do site
        $this->relative_path = $relative_path;  //caminho relativo para includes
        $this->site_page = 'home.php';    //pagina principal do site
        $this->site_lang = $this->config['lang']; //linguagem do site
        $this->page_css = null;      //css espec�fico de cada p�gina
        $this->page_js = null;      //javaScript espec�fico de cada p�gina
        $this->site_meta = 'meta.php';    //arquivo meta dados
        //prevenindo alguns tipos de ataques
        //$this->makeSafeVarByTags($_POST);
        //$this->makeSafeVar($_GET);
        //$this->makeSafeVar($_REQUEST);
    }

    /**
     * Retorna a variavel $_CONF do arquivo de configuracao
     * @return $_CONF do arquivo de configuracao do sistema
     */
    public function getConfigVar() {
        return $this->config;
    }

    /**
     * Retorna uma configura��o do arquivo de configura��es
     * Se a configuraca��o for um array ent�o $index � usado.
     * Tanto indexa��o por valores $index['valor'] como por numero $index[0]
     * @param $config = O nome da configuracao no arquivo de configuracao Configure.php
     * @param $index = O �ndice caso a configura��o seja um array
     * @return O valor da configura��o do sistema
     */
    public function getConfig($config, $index = 0) {
        if (is_array($this->config[$config]))
            return $this->config[$config][$index];
        else
            return $this->config[$config];
    }

    public function setConfig($key, $value, $index=""){
        if(is_int($index)){
            $this->config[$key][$index] = $value;
        }else{
            $this->config[$key] = $value;
        }
    }
    /**
     * Retorna o valor de uma vari�vel $_GET['$get_var'] se esta estiver setada
     * sen�o retorna $opt_false
     * @param $get_var O �ndice da vari�vel
     * @param $opt_false Um retorno opcional caso $get_var n�o seja encontrada
     * @return $_GET['$get_var'] ou $opt_false
     */
    public function getGETVar($get_var, $opt_false = null) {
        
        if (is_array(@$_GET[$get_var])) {
            $get = filter_input(INPUT_GET, $get_var, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        }else{
            $get = filter_input(INPUT_GET, $get_var);
        }
        
        if ($get != "") {
            //limintando o tamanho da vari�vel $_GET em no m�ximo 255 caracteres.
            return mb_strcut($get, 0, 255);
        } else {
            return $opt_false;
        }
    }

    /**
     * Retorna o valor de uma vari�vel $_POST['$get_var'] se esta estiver setada
     * sen�o retorna $opt_false. Obs: valores s�o utf8_encoded
     * @param $post_var O �ndice da vari�vel
     * @param $opt_false Um retorno opcional caso $get_var n�o seja encontrada
     * @return $_POST['$get_var'] ou $opt_false
     */
    public function getPOSTVar($post_var, $opt_false = null) {

        if (is_array(@$_POST[$post_var])) {
            $post = filter_input(INPUT_POST, $post_var, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        } else {
            $post = filter_input(INPUT_POST, $post_var);
        }

        if ($post != "") {
            return $post;
        } else {
            return $opt_false;
        }
    }

    /**
     * Retorna o valor de uma vari�vel $_GET['$get_var'] ou $_POST['$get_var'] se esta estiver setada
     * se houverem vari�veis de mesmo nome a primeira a ser retornada � $_POST['$get_var']
     * sen�o retorna $opt_false
     * @param $get_var O �ndice da vari�vel
     * @param $opt_false Um retorno opcional caso $get_var n�o seja encontrada
     * @return $_GET['$get_var'] ou $opt_false
     */
    public function getREQUESTVar($get_var, $opt_false = null) {
        //verificando a existencia da variavel link
        $request_var[] = $this->getGETVar($get_var, $opt_false);
        $request_var[] = $this->getPOSTVar($get_var, $opt_false);

        foreach ($request_var as $var => $value) {
            if ($value != "")
                return $request_var[$var];
        }

        return $opt_false;
    }

    /**
     * Seta um arquivo CSS de nome $cssname para a p�gina corrente
     * se $cssname = null ent�o $cssname possui o mesmo nome da p�gina corrente
     * @param $cssname Nome do arquivo CSS
     */
    public function setPageCss($cssname = null) {
        if (!is_null($cssname)) {
            $this->page_css = $cssname;
        } else {
            //arquivo css tem o mesmo nome da pagina php $site_page
            $this->page_css = DIR_PAGES_CSS . str_replace('.php', '.css', basename($this->site_page));
        }
    }

    /**
     * Obtem o HTML para o arquivo CSS da p�gina atual se o arquivo
     * existir na pasta padr�o DIR_PAGES_CSS
     */
    public function getPageCss($body_tag = false, $media = 'screen') {

        if ($this->page_css != null) {

            if (file_exists($this->page_css)) {

                if ($body_tag == false) {
                    echo '<link rel="stylesheet" type="text/css" href="' . $this->page_css . '" media="' . $media . '"/>' . "\r\n\t\t";
                } else {
                    echo '<style type="text/css" media="' . $media .'">@import url("' . $this->page_css . '");</style>' . "\r\n\t\t\t";
                }
            }
        }
    }

    /**
     * Seta um arquivo JavaScript de nome $jsname para a p�gina corrente
     * se $cssname = null ent�o $cssname possui o mesmo nome da p�gina corrente
     * @param $cssname Nome do arquivo JavaScript
     */
    public function setPageJs($jsname = null) {
        if (!is_null($jsname)) {
            $this->page_js = $jsname;
        } else {
            //arquivo js tem o mesmo nome da pagina php $site_page
            $this->page_js = DIR_PAGES_JS . str_replace('.php', '.js', basename($this->site_page));
        }
    }

    /**
     * Obtem o HTML para o arquivo JavaScript da p�gina atual se o arquivo
     * existir na pasta padr�o DIR_PAGES_JS
     */
    public function getPageJs() {
        if ($this->page_js != null) {
            if (file_exists($this->page_js))
                echo '<script type="text/javascript" src="' . $this->page_js . '"></script>' . "\r\n\t\t\t";
        }
    }

    /**
     * Importa um arquivo CSS no corpo da p�gina
     * @param $css_path Caminho do arquivo css a ser importado
     * @param $body_tag Se falso imprime o import com a tag <link> em vez da tag <style>
     */
    public function getCustomCss($css_path, $body_tag = true) {
        $this->setPageCss($css_path);
        $this->getPageCss($body_tag);
    }

    /**
     * Importa um arquivo CSS no corpo da p�gina
     * @param $css_path Caminho do arquivo css a ser importado
     */
    public function getCustomCssPrint($css_path) {
        $this->setPageCss($css_path);
        $this->getPageCss($body_tag = true, 'print');
    }

    /**
     * Importa um arquivo JavaScript no corpo da p�gina
     * @param $js_path Caminho do arquivo JavaScript a ser importado no corpo da p�gina
     */
    public function getCustomJs($js_path) {
        $this->setPageJs($js_path);
        $this->getPageJs();
    }

    /**
     * Seta o arquivo referente ao nome da p�gina
     * @param $pagename Nome da pagina que se deseja setar
     * Se a pagina $pagename n�o existir na lista de paginas
     * ent�o a pagina padrao sera $default
     */
    public function setSitePage($pagename) {
        //arquivo contendo a lista de p�ginas do site
        require_once ('./include/Pages.php');
        //tamanho maximo da string $pagename = 30
        mb_strcut($pagename, 0, 30);
        $this->makeSafeVar($pagename);
        $pagename = str_replace('.php', '', $pagename);
        $pagename = addslashes($pagename);
        $numpages = count($_PAGE);

        //verificando a existencia de pagina
        if (isset($_PAGE[$pagename]))
            $this->site_page = $_PAGE[$pagename];
        else
            $this->site_page = '404.php';
    }

    public function getSitePage($key) {
        require_once ('./include/Pages.php');
        if (isset($_PAGE[$key])) {
            return $_PAGE[$key];
        } else {
            return '404.php';
        }
    }

    public function getAsyncPage($key) {
        require_once ('./include/Pages.php');
        if (isset($_ASYNC[$key])) {
            return $_ASYNC[$key];
        } else {
            return '404.php';
        }
    }

    /**
     * Seta a linguagem do site em um Cookie
     */
    public function setSiteLang($lang) {

        $get_lang = $this->makeSafeVar($lang);

        if (is_array($this->config['lang_avaliable'])) {

            foreach ($this->config['lang_avaliable'] as $value) {
                if ($get_lang == $value) {
                    $this->site_lang = $value;
                    return true;
                    break;
                }
            }
        } else {
            if ($get_lang == $this->config['lang_avaliable']) {
                $this->site_lang = $this->config['lang_avaliable'];
                return true;
            }
        }
        return false;
    }

    /**
     * Obt�m o template do arquivo pelo URI dado como par�metro
     * @param $uri Uniform Resource Identifie (caminho absoluto)
     */
    public function getTemplate($uri) {
        return DIR_TEMPLATE_PAGES . str_replace('.php', '.html', basename($uri));
    }

    /*
     * Prote��o contra XSS, JS e SQL injection por remo��o de tags.
     */

    public function makeSafeVar(&$variable, $add_slashes = true, $remove_tags = true) {

        if ($remove_tags == true && $add_slashes == true) {
            return Protection::makeSafeVar($variable);
        }
        /* if ($remove_tags == true && $add_slashes == false) {
          Protection::sanitizeTags($str);
          return true;
          } */
    }

    public function registerUserEvent() {
        //LOG de acesso (criar uma classe para controlar o log)
        //$_SS->getvar('user');
        @$fp = fopen("./logs/" . $_SESSION['user'] . '.php', "a+");
        $reg = "";
        if (count($_GET)) {
            foreach ($_GET as $key => $value) {
                $reg = $reg . "&" . $key . "=>" . $value;
            }
        } else if (count($_POST)) {
            foreach ($_POST as $key => $value) {
                $reg = $reg . "&" . $key . "=>" . $value;
            }
        } else {
            $reg = $this->site_page;
        }

        @fwrite($fp, $_SESSION['user'] . "|" . $reg . "|" . $_SERVER['REMOTE_ADDR'] . "|" . $_SERVER['HTTP_HOST'] . "|" . $_SERVER['HTTP_USER_AGENT'] . "|" . date("d-m-Y-D-H:i:s") . "<br/>\n");
        @fclose($fp);
    }

    public function registerRequestEvent($event = 'invalid') {
        if ($event == 'invalid') {
            @$fp = fopen("./logs/" . 'invalid_access' . '.php', "a+");
        }
        if ($event == 'error') {
            @$fp = fopen("./logs/" . 'error' . '.php', "a+");
        }
        $reg = "";

        if (count($_GET)) {
            foreach ($_GET as $key => $value) {
                $reg = $reg . "&" . $key . "=>" . $value;
            }
        } else if (count($_POST)) {
            foreach ($_POST as $key => $value) {
                $reg = $reg . "&" . $key . "=>" . $value;
            }
        } else {
            $reg = $this->site_page;
        }

        @fwrite($fp, $reg . "|" . $_SERVER['REMOTE_ADDR'] . "|" . $_SERVER['HTTP_HOST'] . "|" . $_SERVER['HTTP_USER_AGENT'] . "|" . date("d-m-Y-D-H:i:s") . "<br/>\n");
        @fclose($fp);
    }

}

?>