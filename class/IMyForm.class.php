<?php

/**
 * Interface para manipula��o de formul�rios
 * 
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 0.9
 */
interface IMyForm {

    public function validate();
}

?>