<?php

class Debug {

    public function formVarGenerate($web = false) {
        if (strcmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }

        foreach ($method as $var_name => $var_value) {
            if ($web) {
                echo 'protected $' . $var_name . '="";' . "<br/>";
            } else {
                echo 'protected $' . $var_name . '="";' . "\r\n";
            }
        }
    }

    public function formGetVarGenerate($web = false, $name_var = 'frmval') {
        if (strcmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }

        foreach ($method as $var_name => $var_value) {
            if ($web) {
                echo '$' . $name_var . "->getVar('" . $var_name . "');" . "<br/>";
            } else {
                echo '$' . $name_var . "->getVar('" . $var_name . "');" . "\r\n";
            }
        }
    }

    public function dbAlterCharsetCollation() {

        $db = mysqli_connect('localhost', 'root', 'rootpass');

        if (!$db)
            echo "Cannot connect to the database - incorrect details";

        mysqli_select_db($db, 'petshop');

        $result = mysqli_query($db, 'show tables');

        while ($tables = mysqli_fetch_assoc($result)) {

            foreach ($tables as $key => $value) {
                echo "ALTER TABLE $value CONVERT TO CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'".'<br/>';
                mysqli_query($db, "ALTER TABLE $value CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci");
            }
        }
        echo "The collation of your database has been successfully changed!";
    }

}
?>