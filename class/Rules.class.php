<?php

/**
 * Classe para identifica��o das regras de exibi��o de links em p�ginas
  Foi criada espec�ficamente para organizar os m�dulos do sistema
  entretanto pode ser generaliza com algumas adapta��es
 * 
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 0.1
 *
 * @param Permiss�o do usu�rio, max 4 digitos Exemplo: 1234
 * @param O objeto inst�nciado da classe Template
 */
class Rules {

    private $user_perm; //permissao do usuario
    private $tpl; //template

    function __construct($permissao, &$tpl) {
        $this->user_perm = $permissao;
        $this->tpl = $tpl;
    }

    /**
     * Obt�m os links que o usu�rio pode acessar
     * @param $rules Um array contendo as permissoes
     */
    public function getLinksBlock($rules) {
        $links = "";
        $i = 0;
        //permissao bloco
        if ($this->user_perm >= $rules[0000][0]) {
            foreach ($rules as $rule => $v) {
                if (($rule <= $this->user_perm) && $rule != 0000) {
                    foreach ($v as $txt) {
                        //obtem a chave do elemento $i no array $v
                        $temp = array_slice($v, $i, 1, true);
                        $links[key($temp)] = $txt;
                        $i++;
                    }
                    $i = 0;
                }
            }

            $numelem = count($links);
            $links_ordered = "";

            if ($numelem > 0 && is_array($links)) {
                //ordena o array pela prioridade
                ksort($links);
                foreach ($links as $v)
                    $links_ordered .= $v;
            }

            //cria o conteudo do template ($tpl->VAR) com os links autorizados para o usuario
            $this->tpl->{"VAR_" . $rules[0000][1]} = $links_ordered;

            //cria o bloco
            $this->tpl->block($rules[0000][1]);
        }
    }

}

?>