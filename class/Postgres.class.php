<?php

//necess�rio a interface IDB
require_once ("IDB.class.php");

/**
 * Classe para manipula��o de banco de dados Postgres via PHP
 * Implementa a interface IDB
 *
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 0.9.2
 */
class Postgres implements IDB {

    /** @var $connect_id ID da conex�o com o banco * */
    private $connect_id = NULL;

    /** @var $query_id ID da consulta executada * */
    private $query_id = NULL;

    /** @var $sqlserver Host para o banco de dados * */
    private $sqlserver = "";

    /** @var $sqluser Usuario do banco de dados * */
    private $sqluser = "";

    /** @var $sqlpassword Senha do banco de dados * */
    private $sqlpassword = "";

    /** @var $database Nome do banco de dados * */
    private $database = "";

    /** @var $port Porta para conexao * */
    private $port = "";
    
    private $charset = "";
    //numero de linhas retornada por uma consulta
    public $num_rows = 0;

    /**
     * Fun��o construtora da classe
     * @param string $sqlserver Host do servidor Mysql
     * @param string $sqluser su�rido do banco de dados
     * @param string $sqlpassword Senha do banco de dados
     * @param string $database Nome do banco de dados
     * @param string $port Porta para conex�o com a base de dados
     */
    function __construct($sqlserver, $sqluser, $sqlpassword, $database, $port = "5432", $charset='utf8') {
        $this->sql_connect($sqlserver, $sqluser, $sqlpassword, $database, $port);
        $this->sqlserver = $sqlserver;
        $this->sqluser = $sqluser;
        $this->sqlpassword = $sqlpassword;
        $this->database = $database;
        $this->port = $port;
        $this->charset = $charset;
    }

    /**
     * Fun��o de conex�o com o banco de dados Postgres
     * @param string $sqlserver = Nome do servidor
     * @param string $sqluser = Nome do usu�rio do banco de dados
     * @param string $sqlpassword = Senha do usu�rio
     * @param string $database = Nome do bando de dados
     * @return resource|error ID da conex�o OU Mensagem de erro
     */
    public function sql_connect($sqlserver, $sqluser, $sqlpassword, $database, $port) {
        $this->connect_id = pg_connect('host=' . $sqlserver . ' port=' . $port . ' user=' . $sqluser . ' password=' . $sqlpassword . ' dbname=' . $database);

        if ($this->connect_id) {
            $this->set_charset($this->charset);
            return $this->connect_id;
        } else {
            return $this->error();
        }
    }

    /**
     * Executa uma consulta SQL no banco de dados
     * @param string $query A consulta SQL a ser executada
     * @return resource Query ID
     */
    public function query($query) {
        if ($query != "") {
          
            $this->query_id = pg_query($this->connect_id, $query);

            if ($this->query_id) {
                $this->num_rows = pg_affected_rows($this->query_id);
                return $this->query_id;
            } else {
                $this->num_rows = -1;
                return $this->error();
            }
        } else {
            return '<b>Postgres Error</b>: Empty Query!';
        }
    }

    /**
     * Prepara um query para ser executada
     * @param string $query A consulta SQL a ser preparada
     * @return string A consulta preparada
     */
    public function prep($query) {
        return pg_escape_string($query);
    }

    /**
     * Obt�m o n�mero de linhas da consulta
     * @param resource $query_id (Opcional) Uma id de conexao com o banco de dados
     * @return integer N�mero de linhas obtidas pela consulta
     */
    public function get_num_rows($query_id = "") {
        if ($query_id == NULL) {
            return pg_num_rows($this->query_id);
        } else {
            return pg_num_rows($query_id);
        }
    }

    /**
     * Obt�m o ID gerado pela opera��o INSERT anterior
     * @param string $s_column A coluna que � sequence/serial na tabela do banco de dados
     * @return interger ID gerada pelo ultimo INSERT (PostgreSQL >= 8.1)
     */
    public function get_insert_id($s_column = 0) {
        $this->query("SELECT lastval()");
        $data = $this->fetch_row();
        return $data[$s_column];
    }

    /**
     * Obt�m o n�mero de registros afetados por INSERT, DELETE ou UPDATE
     * @param resource $query_id (Opciona) Uma resource de query
     * @return integer O n�mero de linhas afetadas 
     */
    public function get_affected_rows($query_id = NULL) {
        if ($query_id == NULL) {
            $return = pg_affected_rows($this->query_id);
        } else {
            $return = pg_affected_rows($query_id);
        }

        return $return;
    }

    /**
     * Obt�m uma linha do resultado como um array enumerado e associativo
     * O nome de cada coluna da consulta � o �ndice do array al�m disso
     * tamb�m associa um �ndice n�merico para cada coluna no array come�ando de 0 (zero)
     * @param resource $query_id (Opciona) Uma resource de query
     * @return array Um array associativo por nome de coluna da tabela e por �ndice n�merico
     */
    public function fetch_row($query_id = NULL) {
        if ($query_id == NULL) {
            return pg_fetch_array($this->query_id);
        } else {
            return pg_fetch_array($query_id);
        }
    }

    /**
     * Obt�m uma linha do resultado como um array associativo pelo nome da coluna
     * O nome de cada coluna da consulta � o �ndice do array 
     * @param resource $query_id (Opciona) Uma resource de query
     * @return array Um array associativo por nome de coluna da tabela
     */
    public function fetch_rowname($query_id = "") {
        if ($query_id == NULL) {
            return pg_fetch_assoc($this->query_id);
        } else {
            return pg_fetch_assoc($query_id);
        }
    }

    /**
     * Obt�m uma linha do resultado como um array associativo pelo �ndice da coluna
     * O �ndice  da coluna da consulta � o �ndice do array 
     * @param resource $query_id (Opciona) Uma resource de query
     * @return array Um array associativo por �ndice n�merico
     */
    public function fetch_rowindex($query_id = "") {
        if ($query_id == NULL) {
            return pg_fetch_row($this->query_id);
        } else {
            return pg_fetch_row($query_id);
        }
    }

    /**
     * Define a codifica��o (Charset) para a conex�o com o banco de dados
     * @param type $charset ISO-8859-1, UTF8 etc
     * @return boolean
     */
    public function set_charset($charset) {
        $this->charset = $charset;
        return pg_setclientencoding($this->connect_id, $charset);
    }

    /**
     * Obt�m a codifica��o da conex�o estabelecida com o banco de dados
     * @return string A codifica��o do cliente, ou <b>FALSE</b> se houve erro.
     */
    public function get_encoding() {
        return pg_client_encoding($this->connect_id);
    }
    
    /**
     * Seleciona um banco de dados
     * @param $database Nome do bando de dados
     * @return resource ID da conex�o com o DB | erro
     */
    public function select_db($database) {

        if ($this->sql_connect($this->sqlserver, $this->sqluser, $this->sqlpassword, $database, $this->port)) {
            $this->database = $database;
        }
    }

    /**
     * Fecha a conex�o com o banco de dados
     * @return ID da conex�o
     */
    public function sql_close() {
        if ($this->connect_id) {
            return pg_close($this->connect_id);
        }
    }

    /**
     * Exibe uma mensagem de erro quando este acontece
     * @return bool Zero em caso de erro
     */
    public function error() {
        if ($this->connect_id != 0) {
            if (pg_last_error($this->connect_id)) {
                echo '<br/><strong>Postgres Error</strong>: ' . pg_last_error($this->connect_id) . '<br/>';
            }
        }

        return 0;
    }

}

?>
