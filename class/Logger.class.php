<?php

/*
 * Classe para log de a��es realizadas pelo usu�rio
 * Formato do log
 * 
 * valor1:valor2:valor3:
 */

class Logger {

    //referencia (ponteiro para arquivo ou id database)
    private $file_ref;
    private $db_ref;
    private $log_ftb_name; //nome do arquivo ou tabela 
    //tipo de log (0:arquivo ou 1:banco de dados)
    private $type;

    function Logger($type = 0, $dest = 'logger') {

        if ($type == 0) {//arquivo
            if (!is_object($dest)) {
                $this->log_ftb_name = $dest;
            } else {
                echo "Logger(erro): nome de arquivo esperado!";
                exit(0);
            }
        } else if ($type == 1) {//banco de dados
            if (is_object($dest)) {
                $this->$db_ref = $dest;
            } else {
                echo "Logger(erro): db instace esperado!";
            }
        }
    }

    /**
     * Escreve na sa�da padr�o a string $string
     * @param $logstring A string com os separadores de log
     * 
     */
    public function writeLog($logstring) {

        //log em arquivo
        if ($this->type == 0) {
            $fp = fopen('./' . $this->log_ftb_name, "a+");
            @fwrite($fp, $logstring . ":" . date("d-m-Y-N-i-s") . "\n");
            @fclose($fp);
            return;
        }

        //log em banco de dados
        if ($this->type == 1) {
            $query = "INSERT INTO KLT_LOGGER VALUES('" . $logstring . ":" . date("d-m-Y-N-i-s") . "');";
            $db->query($query);
            return;
        }
    }

}

?>