<?php

/**
 * Interface para manipula��o de banco de dados
 * 
 * @author Maiser Jos� Alves Oliva (maiseralves@gmail.com)
 * @version 1.0.0
 */
interface IDB {

    public function sql_connect($sqlserver, $sqluser, $sqlpassword, $database, $port = "");

    public function query($query);

    public function prep($query);

    public function get_num_rows($query_id = NULL);

    public function get_affected_rows($connect_id = NULL);

    public function fetch_row($query_id = NULL);

    public function fetch_rowname($query_id = NULL);

    public function fetch_rowindex($query_id = NULL);

    public function select_db($database);

    public function sql_close();

    public function error();
}

?>
