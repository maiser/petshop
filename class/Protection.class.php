<?php

class Protection {

    /**
     * Coloca barra nas para escape das aspas (duplas e simples)
     * @param type $string_var
     * @return type
     */
    public static function addSlashes(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return addslashes($string_var);
        }
    }

    /**
     * Prote��o contra XSS, JS e SQL injection por remo��o de tags.
     * 
     * @param type $string_var
     * @return type
     */
    public static function removeTags(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return strip_tags(trim($string_var));
        }
    }

    /**
     * Prote��o contra XSS, JS e SQL injection por remo��o de tags.
     * 
     * @param type $string_var
     * @return type
     */
    public static function removeTagsXSS(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($string_var) : $string_var), ENT_QUOTES))));
        }
    }

    /**
     * Converte tags html para caracteres reais de escape e
     * n�o converte as aspas duplas e simples
     * 
     * @param type $string_var
     * @return type
     */
    public static function sanitizeTagsNQ(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(htmlspecialchars($string_var, ENT_NOQUOTES));
        }
    }

    /**
     * Converte tags html para caracteres reais de escape e tamb�m
     * converte as aspas duplas para '&quot;'
     * e aspas simples para '&#039;'
     *
     * @param type $string_var
     * @return type
     */
    public static function sanitizeTags(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(htmlspecialchars($string_var, ENT_QUOTES));
        }
    }

    /**
     * Converte tags html para caracteres reais de escape e
     * n�o converte as aspas duplas e simples
     * 
     * @param type $string_var
     * @return type
     */
    public static function sanitizeAllTagsNQ(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(htmlentities($string_var, ENT_NOQUOTES));
        }
    }

    /**
     * Converte tags html para caracteres reais de escape e tamb�m
     * converte as aspas duplas para '&quot;'
     * e aspas simples para '&#039;'
     *
     * @param type $string_var
     * @return type
     */
    public static function sanitizeAllTags(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(htmlentities($string_var, ENT_QUOTES));
        }
    }

    /**
     * Converte caracteres html convertidos para caracteres html reais
     * mas n�o converte as aspas duplas e simples
     * 
     * @param type $string_var
     * @return type
     */
    public static function unSanitizeTagsNQ(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(htmlspecialchars_decode($string_var, ENT_NOQUOTES));
        }
    }

    /**
     * Converte caracteres html convertidos para caracteres html reais
     * e converte tamb�m as aspas duplas e simples
     * 
     * @param type $string_var
     * @return type
     */
    public static function unSanitizeTags(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(htmlspecialchars_decode($string_var, ENT_QUOTES));
        }
    }

    /**
     * Converte caracteres html convertidos para caracteres html reais
     * mas n�o converte as aspas duplas e simples
     * 
     * @param type $string_var
     * @return type
     */
    public static function unSanitizeAllTagsNQ(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(html_entity_decode($string_var, ENT_NOQUOTES));
        }
    }

    /**
     * Converte caracteres html convertidos para caracteres html reais
     * e converte tamb�m as aspas duplas e simples
     * 
     * @param type $string_var
     * @return type
     */
    public static function unSanitizeAllTags(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return trim(html_entity_decode($string_var, ENT_QUOTES));
        }
    }

    /**
     * Encode UTF-8
     * 
     * @param type $string_var
     * @return type
     */
    public static function encodeUTF8(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return utf8_encode($string_var);
        }
    }

    /**
     * Decode UTF-8
     * 
     * @param type $string_var
     * @return type
     */
    public static function decodeUTF8(&$string_var) {
        $callback = array(__CLASS__, __FUNCTION__);
        if (is_array($string_var)) {
            return array_map($callback, $string_var);
        } else {
            return utf8_decode($string_var);
        }
    }

    /**
     * Cria uma vari�vel segura contra alguns tipos de ataques
     * @param type $string_var
     * @return type
     */
    public static function makeSafeVar(&$string_var) {
        $string_var = self::removeTagsXSS($string_var);
        $string_var = self::addSlashes($string_var);
        return $string_var;
    }

}
