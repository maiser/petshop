<?php

/**
 * Class para gerenciamento de banco de dados
 * --> Implementar futuramente a versao O. O. para o arquvio de configura��o
 *
 * @author maiser
 */
class DB {

    /** @var $sqlserver Host para o banco de dados * */
    private $sqlserver = "";

    /** @var $sqluser Usuario do banco de dados * */
    private $sqluser = "";

    /** @var $sqlpassword Senha do banco de dados * */
    private $sqlpassword = "";

    /** @var $database Nome do banco de dados * */
    private $database = "";

    /** @var $port Porta para conexao * */
    private $port = "";

    private $charset = "";
    
    /** @var $con Inst�ncia da classe de banco de dados criada (tipo (object)) 
     * Ser� um objeto Mysql ou Postgres
     */
    public $con;

    /**
     * Fun��o construtora para conex�o com banco de dados
     * @param $type = O tipo de banco que se deseja conectar (Mysql, Postgres)
     * @param $config = Um array contendo as configura��es do banco (seguindo o padr�o estabelecido no arquivo Configure.php)
     * @param $index = (opcional) O �ndice de configura��o (para m�ltiplos databases)
     */
    function __construct($type, $config, $index = 0) {
        //include("../include/Configure.php");
        $this->setConfig($config, $index);
        $this->create($type);
    }

    /**
     * Conecta-se ao banco de dados do tipo(nome) $type
     * Por enquanto os databases suportados s�o o Mysql e Postgress
     * @param string $type  O tipo de banco de dados (Mysql ou Postgres) por equanto.
     */
    public function create($type = 'mysql') {
        $type = strtolower($type);

        if ($type == 'mysql') {
            require_once ("Mysql.class.php");
            (object) $this->con = new Mysql($this->sqlserver, $this->sqluser, $this->sqlpassword, $this->database, $this->port, $this->charset);
            return true;
        }

        if ($type == 'postgres') {
            require_once ("Postgres.class.php");
            (object) $this->con = new Postgres($this->sqlserver, $this->sqluser, $this->sqlpassword, $this->database, $this->port, $this->charset);
            return true;
        }

        throw new Exception("Tipo de banco de dados inexistente! Type: " . $type);
    }

    /**
     * Seta as configura��es para conex�o com  o banco de dados
     * @param $_CONF = Um array contendo as configura��es do banco (seguindo o padr�o estabelecido)
     * @param $index = (opcional) O �ndice de configura��o (para m�ltiplos databases)
     */
    public function setConfig($_CONF, $index = 0) {

        if (isset($_CONF['host'][$index])) {
            $this->sqlserver = $_CONF['host'][$index];
        } else {
            echo '<br/>$_CONF["host"][' . $index . '] n�o existe no arquivo de configura��o!<br/>';
        }

        if (isset($_CONF['user'][$index])) {
            $this->sqluser = $_CONF['user'][$index];
        } else {
            echo '<br/>$_CONF["user"][' . $index . '] n�o existe no arquivo de configura��o!<br/>';
        }

        if (isset($_CONF['pwd'][$index])) {
            $this->sqlpassword = $_CONF['pwd'][$index];
        } else {
            echo '<br/>$_CONF["pwd"][' . $index . '] n�o existe no arquivo de configura��o!<br/>';
        }

        if (isset($_CONF['db'][$index])) {
            $this->database = $_CONF['db'][$index];
        } else {
            echo '<br/>$_CONF["db"][' . $index . '] n�o existe no arquivo de configura��o!<br/>';
        }

        if (isset($_CONF['port'][$index])) {
            $this->port = $_CONF['port'][$index];
        }
        
        if (isset($_CONF['db_charset'][$index])) {
            $this->charset = $_CONF['db_charset'][$index];
        }else{
            $this->charset = 'utf8';
        }
        
    }

    /** Paginador de querys
     * @param $numrows = numero de registros a ser paginado
     * @param $maxrows = maximo de linhas por pagina
     * @param $linkpage = link para a pagina do paginador
     * @param $numpage = numero da pagina selecionada
     * @param $urlget = nome do parametro a ser enviado por GET em $linkpage;
     * @param $divnumpage = divide o numero total de paginas em blocos de tamanho $divnumpage
     * @param $showall = mostrar a opcao de ver todos os registros
     * @return $list_pages = o paginador em HTML com os links para as paginas
     * @return $startline = o numero onde comeca primeiro registro
     * @return $maxrows = o numero total de registros
     */
    function pageRows($query, $numrows, $maxrows, $linkpage, $numpage, $urlget = 'page', $showall = true, $divnumpage = 16, $label_all = 'todos') {
        $max = 500;
        $pageindex = 0;
        $startline = 0;
        $list_pages = '<div class="pagerow">';
        $all = $label_all;
        $show_all_pages = false;
        $line_count = 1;

        if (!is_numeric($numpage) && $numpage != "all") {
            $numpage = 1;
        }

        for ($i = 1; $i < $numrows; $i++) {
            if (($i % $maxrows) == 0) {
                $pageindex++;
                if ($pageindex != $numpage) { //gera hyperlink para as paginas exceto para a de numero $numpage
                    //if($show_all_pages == true){
                    $list_pages .= '<a href="' . $linkpage . '&amp;' . $urlget . '=' . $pageindex . '">' . $pageindex . '</a>';
                    //}else{
                    //	if($pageindex < 4)
                    //		$list_pages .= '&nbsp;<a href="'.$linkpage.'&amp;'.$urlget.'='.$pageindex.'">['.$pageindex.']</a>&nbsp;';
                    //	if($pageindex > 4)
                    //		$list_pages .= '&nbsp;<a href="'.$linkpage.'&amp;'.$urlget.'='.$pageindex.'">['.$pageindex.']</a>&nbsp;';
                    //}
                } else { //marcador [$numpage] sem hyperlink
                    $list_pages .= '<strong>' . $pageindex . '</strong>';
                }
                if ($pageindex % $divnumpage == 0) {
                    $line_count++;
                    if ($line_count > 1) {
                        $list_pages .= '</div><div class="pagerow">';
                    }
                }
            }
        }

        if (ceil($numrows / $maxrows) > 0) {
            $pageindex++;

            if ($pageindex != $numpage) { //gera hyperlink para as paginas exceto para a de numero $numpage
                $list_pages .= '<a href="' . $linkpage . '&amp;' . $urlget . '=' . $pageindex . '">' . $pageindex . '</a>';
            } else { //marcador [$numpage] sem hyperlink
                $list_pages .= '<strong>' . $pageindex . '</strong>';
            }
            if ($pageindex % $divnumpage == 0) {
                $list_pages .= '<div class="pagerow">';
            }
        }

        if ($numpage != 0) {
            if ((is_numeric($numpage) == true) && ($pageindex > 0)) {
                //se numero da pagina for menor que a quantidade real de paginas
                if ($numpage <= $pageindex) {
                    $startline = ($numpage * $maxrows) - $maxrows;
                    if ($showall == true) {
                        $list_pages .= ' <a href="' . $linkpage . '&amp;' . $urlget . '=' . 'all">' . $all . '</a> ';
                    }
                } else {
                    $maxrows = $numrows;
                    $list_pages .= '<strong>' . $all . '</strong>';
                }
            }
        } else {
            if ($showall == true) {
                $maxrows = $numrows;
                $list_pages .= '<strong>' . $all . '</strong>';
            }
        }

        //adicionando LIMIT a�query passada como par�metro
        if ($numpage != 'all') {
            $query .= ' LIMIT ' . (($numpage * $maxrows) - $maxrows) . ',' . $maxrows;
        }else{
            $query .= ' LIMIT 0,' .$max;
        }
        $list_pages .= "</div>";
        return array($query, $list_pages, $startline, $maxrows);
    }

    /*
     * Coloca barra nas para escape das aspas (duplas e simples)
     */

    public function addSlashes(&$str) {
        $callback = array($this, __CLASS__ . '::' . __FUNCTION__);
        if (is_array($str)) {
            return array_map($callback, $str);
        } else {
            return addslashes($str);
        }
    }

    /*
     * Remove tags html da string
     */

    public function removeTagsXSS(&$str) {
        $callback = array($this, __CLASS__ . '::' . __FUNCTION__);
        if (is_array($str)) {
            return array_map($callback, $str);
        } else {
            return str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES))));
        }
    }

    /*
     * Decode UTF-8
     */

    public function decodeUTF8(&$str) {
        $callback = array($this, __CLASS__ . '::' . __FUNCTION__);
        if (is_array($str)) {
            return array_map($callback, $str);
        } else {
            return utf8_decode($str);
        }
    }

    /*
     * Encode UTF-8
     */

    public function encodeUTF8(&$str) {
        $callback = array($this, __CLASS__ . '::' . __FUNCTION__);
        if (is_array($str)) {
            return array_map($callback, $str);
        } else {
            return utf8_encode($str);
        }
    }
    
    /**
     * Converte o formato convencional de data para o formato do Banco de dados
     * @param type $data No formato dd/mm/YYYY
     * @return string YYYY-mm-dd
     */
    public function toDate($data){
        $data = explode('/', $data);
        $dia = $data[0];
        $mes = $data[1];
        $ano = $data[2];
        $data = $ano.'-'.$mes.'-'.$dia;
        return $data;
    }

}

?>
