<?php

/**
 * Classe responsável por montar a estrutura do sistema/site.
 */
//<?php echo $_CONTROL->getConfig('charset');  

/*
 * Aqui constroi-se a página com seus devidos blocos HTML
 * Obs: até o momento a construção é manual
 * em uma eventual atualização futura o layout poderá ser montado
 * pelo o usuário através de uma interface front-end
 */
try {

    $_TPL = new Template(DIR_THEMES . 'layout.html');
    $_TPL->LANG = $_CONTROL->site_lang;

    $_TPL->addFile('META', './meta.php');
    $_TPL->addFile('HEADER', './header.php');
    $_TPL->addFile('NAVIGATION', './menu.php');
    $_TPL->addFile('CONTENT', $_CONTROL->site_page);
    $_TPL->addFile('FOOTER', './footer.php');

    $_TPL->block('BLK_NAVIGATION');
    $_TPL->block('BLK_HEADER');
    $_TPL->block('BLK_CONTENT');
    $_TPL->block('BLK_FOOTER');

    $_TPL->show();
} catch (Exception $e) {
    echo utf8_decode($e->getMessage());
    //header('Location: gerenc.php?link=404');
}
?>