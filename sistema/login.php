<?php
/*
* Arquivo para login no sessão administrativa
* Esta pagina é chamada de modo assíncrono via Ajax
* 
*/
require_once ('../class/Session.class.php');
require_once ('../class/DB.class.php');
require_once ('../include/Configure.php');
require_once ('../class/FormValidator.class.php');

$_SS = new Session(true);
$logged = 0;


if( isset($_SESSION['user']) && isset($_SESSION['hash'])) {
	header("Location: gerenc.php");
}else{

	
	if(isset($_POST['f_user']) && isset($_POST['f_pass'])){
		if($_POST['f_user'] != "" && $_POST['f_pass'] != "") {
			$num = 0;
			
			$db = new DB($_CONF['sgbd'][0], $_CONF);
			
			$query = "SELECT * FROM USERS
						WHERE UID='" . $db->con->prep($_POST['f_user']) . "'
						AND PASSWORD='" . $db->con->prep($_POST['f_pass']) . "'
				 ";
						
			$db->con->query($query);
			$num = $db->con->get_num_rows();
			
			
						
			//se o usuario existe no banco
			if($num == 1){
				$logged = 1;
				$logevent = 'login_sucess';
				
				$data = $db->con->fetch_row();
				$_SS->set_var('user', $data['UID']);
				$_SS->set_var('user_name', $data['NOME']);
				$_SS->set_var('hash', md5($data['UID']).date('dmYi'));
				$_SS->set_var('user_perm', $data['USER_PERM']);
						
				if(isset($_GET['ajax']))//java script habilitado
					echo json_encode( array('ok' => true));
				else//JavaScript desabilitado!
					header("Location: gerenc.php");
					//header("Location: manutencao.html");
				
			}else{//usuario nao encontrado
				$logged = 0;
				$logevent = 'wrong_login';//usario ou senha invalidos
			}
			
			$db->con->sql_close();	
			
		}else{//$_POST[] == ""
			$logged = 0;
			$logevent = 'empty_login';//usuario ou senha vazios
		}
	
	}else{//$_POST nao existe
		$logged = -1;
		$logevent = 'page_direct_access';//acesso direto a esta pagina pela url
	}
	
		//$logger = new Logger(0,'logacces4.php');
		//$logger->writeLog($logevent."|".basename(__FILE__)."|".$_POST['f_user']."|".$_SERVER['REMOTE_ADDR']."|".$_SERVER['HTTP_HOST']."|".$_SERVER[HTTP_USER_AGENT]."|".date("d-m-Y-NHis"));
		
		//LOG de acesso (criar uma classe para controlar o log)
		//@$fp = fopen("./logs/logacess3.php","a+");
		//@fwrite($fp, $logevent."|".basename(__FILE__)."|".$_POST['f_user']."|".$_SERVER['REMOTE_ADDR']."|".$_SERVER['HTTP_HOST']."|".$_SERVER[HTTP_USER_AGENT]."|".date("d-m-Y-NHis")."\n");
		//@fclose( $fp);
		
	
		if($logged == -1){
			header("Location: index.php");	
		}else if($logged == 0){
			header("Location: index.php?err");	
		}
		

}//nao existe user session
?>
