<?php if(!isset($GLOBALS['_CONTROL'])) exit(0); else global $_CONTROL; ?>

		<title><?php echo $GLOBALS['_CONTROL']->getConfig('sys_title'); ?></title>
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame  -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="description" content="Bem-vindo &aacute; Kiloutou Loca&ccedil;&atilde;o de M&aacute;quinas e equipamentos" />
		<meta name="author" content="Maiser" />
		<!-- <meta name="viewport" content="width=device-width; initial-scale=1.0" />-->
		<meta http-equiv="Content-Type" content="text/html;charset=<?php echo $_CONTROL->getConfig('charset'); ?>" >
			
		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" media="screen" href="./css/general.css" />
                               
                <link rel="stylesheet" type="text/css" media="print" href="./css/print.css"  />
		<link rel="stylesheet" type="text/css" href="./css/dropdown.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="./css/default.advanced.css" media="screen" />
		<?php
		$_CONTROL->getPageCss();//CSS
		?> 
		<script type="text/javascript" src="./js/jquery.min.js"></script>
		<script type="text/javascript" src="./js/functions.js"></script>
		<?php
		$_CONTROL->getPageJs();//JavaScript
		?> 