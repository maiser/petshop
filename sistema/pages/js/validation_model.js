$(document).ready(function ($) {

    var form_id = '#form1';
    var rules_var = {
        errorClass: "invalid",
        validClass: "valid",
        errorElement: "label",
        submitHandler: function(form){
            submitForm(form);
        },
        invalidHandler:
                function (event, validator) {
                    //var errors = validator.numberOfInvalids();
                   // if (errors) {
                        errorFormValidateMsg();
                    //}
                },
        highlight: function (element, errorClass, validClass) {
            var str1 = element.name;
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';
            $(inputElement).addClass(errorClass).removeClass(validClass);
            //alert(element.id);
            //$(element_str).addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var str1 = element.name;
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';
            $(inputElement).removeClass(errorClass).addClass(validClass);
            //$(element_str).addClass(validClass).removeClass(errorClass);
        },
        success: function (errorElement) {

            var str2 = errorElement.attr("id");
            var label_str = str2.replace('[]', '\\[\\]');
            var labelElement = '#' + label_str;
            var str1 = $(labelElement).prev().attr("name");
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';
            //var forAttr = errorElement.attr("for").replace('[]', '\\[\\]');

            if ($(labelElement).length === 2) {
                $(inputElement).next().next().remove();
            }
            if ($(labelElement).length === 1) {
                if ($(inputElement).hasClass("valid")) {
                    
                    //console.log($('[for="'+forAttr+'"]').length +" "+forAttr);
                    var errorValid = errorElement.removeClass("invalid").addClass("valid");
                    $(inputElement).next().next().remove();
                    $(inputElement).insertBefore(errorValid);
                    
                    
                   
                }
                if ($(inputElement).hasClass("invalid")) {
                   
                    $(inputElement).next().next().remove();
                }
                
            }
             $(inputElement).keypress(function(){
                       $(inputElement).focus();
                        console.log($(inputElement+':focus').attr("name"));
                    });
            //$(inputElement).on("focus", function(){
                      
             //      });
            
        },
        errorPlacement:
                function (error, element) {
                    error.insertAfter(element);
                },
        rules: {//email: {required: true, email: true } 
        },
        messages: {// nome_do_input: "mensagem"
        }
    };
    setFirst();

    //validar formulario
    $.validator.addMethod("required", $.validator.methods.required, "");
    $.validator.addMethod("email", $.validator.methods.email, "");
    $.validator.addMethod("maxlength", $.validator.methods.maxlength, "");
    $.validator.addMethod("minlength", $.validator.methods.minlength, "");
    $(form_id).validate(rules_var);
    $('#txt_cep').inputmask("mask", {"mask": "99999999", "placeholder": ""});
    $('#txt_num').inputmask("mask", {"mask": "99999", "placeholder": ""});
    $('#txt_telefone').inputmask("mask", {"mask": "(99) 9999-9999", "placeholder": ""});
    $('#txt_celular').inputmask("mask", {"mask": "(99) 9999-9999[9]", "placeholder": ""});
    $("#txt_cep").keyup(function () {
        buscacep($(this).val());
    });
    var max_fields = 5; //maximum input boxes allowed
    var wrapper = $(".petfields:first"); //fields wrapper
    var container = $(".petregs"); //fields container
    var add_button = ".addmore"; //add button ID
    var count = $('.petfields').length;
    var index = 1;
    if (count > 1) {
        index = count - 1;
        var i;
        for (i = 1; i < index; i++) {
            $(".petfields").next().append('<div class="field"><button class="rf bts red" data-index=' + index + '>X</button></div>');
        }
    }
    $(add_button).click(function (e) { //on add input button click
//console.log("added");
        var y = index;
        e.preventDefault();
        if (count < max_fields) { //max input box allowed
            y++;
            index++;
            $(wrapper).clone().appendTo(container); //add input box
            $(".petfields:last").append('<div class="field"><button class="rf bts red" data-index=' + index + '>X</button></div>');
            $(".petfields:last input:first").val("");
            $(".petfields:last label.invalid").remove();
            $(".petfields:last label.valid").remove();
            $("select#raca1:last").attr("id", "raca" + index).html('<option value=""></option>');
            $("select#especie1:last").attr("id", "especie" + index);
            $("body").on("change", "select#especie" + index, function () {
                getRacaPet($(this).val(), "select#raca" + y);
            });
            count++;
        }
    });
    $("body").on("click", "button.rf", function (e) {
        e.preventDefault();
        $("body").off("change", "select#especie" + $(this).attr("data-index"));
        $(this).parent('div').parent("div").remove();
        count--;
    });
    $(".petfields:last").on("change", "select#especie1", function () {
        getRacaPet($(this).val(), "select#raca1");
    });
//    $("input").on("keyup", function () {
//        if ($(form_id).valid() === true) {
//            $("#status_msg").hide(0);
//        }
//    });
//    $("select").on("click", function () {
//        if ($(form_id).valid() === true) {
//            $("#status_msg").hide(0);
//        }
//    });
    setAjaxForm(form_id, '#status_msg', 'POST', './forms/cad_cliente_pet_request.php?ajax=true', 'json');
});
