$(document).ready(function ($) {
    var id = parseInt($(this)._GET('id'));
    var new_cli = $(this)._GET('new_cli_agend');

    setFirst();

    var form_id = '#form1';
    var rules_var = {
        errorClass: "invalid",
        validClass: "valid",
        errorElement: "label",
        //primeiro evento
        highlight: function (element, errorClass, validClass) {
            var str1 = element.name;
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';
            var arrElement = $(inputElement).not($('.agendfields.hide').find(inputElement)).toArray();
            var invalidElement = "<label class='invalid'>!!</label>";

            if ($(arrElement).size() > 1) {
                $.each(arrElement, function (index, elem) {
                    if (!$(elem).hasClass(validClass) && !$(elem).hasClass(errorClass)) {

                        if ($(elem).val() == "") {
                            $(elem).addClass(errorClass).removeClass(validClass);
                            $(elem).next().remove();
                        }
                    }
                });
            } else {
                $(inputElement).addClass(errorClass).removeClass(validClass);

                if ($(inputElement).next().hasClass(validClass) === true) {
                    $(inputElement).next().remove();
                }
            }

        },
        //segundo evento
        unhighlight: function (element, errorClass, validClass) {
            var str1 = element.name;
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';
            var validElement = "<label class='valid'></label>";
            var arrElement = $(inputElement).not($('.agendfields.hide').find(inputElement)).toArray();


            if ($(arrElement).size() > 1) {

                $.each(arrElement, function (index, elem) {

                    if ($(element).hasClass(errorClass) || !$(element).next().hasClass(validClass)) {

                        $(element).removeClass(errorClass).addClass(validClass);

                        $(element).next().remove();
                        $(validElement).insertAfter(element);
                    }
                });


            } else {

                $(inputElement).removeClass(errorClass).addClass(validClass);

                if (!$(inputElement).next().hasClass(validClass) && !$(inputElement).next().hasClass(errorClass)) {
                    $(validElement).insertAfter(element);
                } else if (!$(inputElement).next().hasClass(validClass)) {
                    $(inputElement).next().remove();
                    $(validElement).insertAfter(element);
                }
            }
        },
        //terceiro evento
        errorPlacement: function (error, element) {
            var str1 = $(element).attr('name');
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';
            var arrElement = $(inputElement).not($('.agendfields.hide').find(inputElement)).toArray();
            var invalidElement = "<label class='invalid'></label>";

            if ($(arrElement).size() > 1) {
                $.each(arrElement, function (index, elem) {

                    if ($(elem).hasClass('invalid') && !$(elem).next().hasClass('invalid')) {
                        $(invalidElement).insertAfter(elem);
                    }
                    else if ($(elem).hasClass('invalid') && $(elem).next().hasClass('invalid')) {

                        $(elem).next().remove();
                        $(invalidElement).insertAfter(elem);
                    }
                    if ($(elem).hasClass('valid') && $(elem).next().hasClass('valid')) {
                        $(element).removeClass('valid').addClass('invalid');
                        $(element).next().remove();
                        $(invalidElement).insertAfter(element);

                    }
                });
            } else {
                if ($(element).hasClass('invalid') && !$(element).next().hasClass('invalid')) {
                    $(invalidElement).insertAfter(element);
                }
                //error.insertAfter(element);
            }


        },
        invalidHandler: function (event, validator) {
            //var errors = validator.numberOfInvalids();if (errors) {}
            errorFormValidateMsg();
        },
        rules: {
            celular: "required_fone",
            telefone: "required_fone"
        },
        groups: {
        },
        messages: {// nome_do_input: "mensagem"
        }
    };

    //validar formulario
    $.validator.addMethod("required", $.validator.methods.required, "");
    $.validator.addMethod("email", $.validator.methods.email, "");
    $.validator.addMethod("maxlength", $.validator.methods.maxlength, "");
    $.validator.addMethod("minlength", $.validator.methods.minlength, "");
    $.validator.addMethod("required_fone", function (value, element, params) {
        if ($('#txt_telefone').val() != "") {
            $('#txt_celular').removeClass('invalid');
            $('#txt_celular').next('label').remove();
            $('#txt_celular').addClass('valid');
            return true;
        }
        if ($('#txt_celular').val() != "") {
            $('#txt_telefone').removeClass('invalid');
            $('#txt_telefone').next('label').remove();
            $('#txt_telefone').addClass('valid');
            return true;
        }

        return false;
    }, "");

    $(form_id).validate(rules_var);

    function setMaskDateTime() {//Date Time Price
        $('.data_agend').inputmask("mask", {"mask": "d/m/y", "placeholder": ""});
        $('.time').inputmask("mask", {"mask": "h:s", "placeholder": ""});

        $('#txt_telefone').inputmask("mask", {"mask": "(99) 9999-9999", "placeholder": ""});
        $('#txt_celular').inputmask("mask", {"mask": "(99) 9999-9999[9]", "placeholder": ""});
    }

    function setMaskPrice() {
        var config = {radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            autoUnmask: true,
            prefix: 'R$ '};

        $.each($('.money'), function (index, elem) {
            $(elem).inputmask("decimal", config);
        });

        $.each($('.money-tot'), function (index, elem) {
            $(this).inputmask("decimal", config);
        });
    }

    function calcPrice(instancia) {
        var total_service = 0.0;
        var service_elem = $(instancia).closest('.frm-reg.services');
        var price_elem = $(service_elem).children('.field').children('input.service-price');
        var price_list = $(instancia).closest('.services-list').find('input.service-price');
        var id_service = undefined;
        var valor = 0;
        var total_service = new Number();

        if ($(instancia).hasClass('service-price')) {
            id_service = "manual_input";
        }
        if ($(instancia).hasClass('sel-service')) {
            id_service = $(instancia).val();
        }

        if (id_service !== undefined && id_service != "manual_input") {
            var valor = parseFloat($('[service-id="' + id_service + '"]').text());
            price_elem.val(valor + ",00");
        } else {
            if (id_service != "manual_input") {
                price_elem.val('0,00');
            }
        }

        $.each(price_list, function (index, elem) {
            valor = $(elem).val();
            if (valor == "")
                valor = '0,00';
            //verifica se tem . (ponto) mas nao tem , (virgula)
            if (valor.indexOf('.') > -1 && valor.indexOf(",") == -1) {
                valor = parseFloat(valor);
            } else if (valor.indexOf('.') == -1 && valor.indexOf(",") > -1) {
                valor = parseFloat((valor).replace(',', '.'));

            } else {
                valor = parseFloat(valor);
            }

            total_service += valor;
        });

        total_service = total_service.toString();

        if (total_service.indexOf(".") == -1) {
            total_service = total_service + ',00';
        }

        $(instancia).closest('.services-list').next().find('input.service-total').val(total_service);
        setMaskPrice();
    }

    function activeChangePet(index, y) {
        $("body").on("change", "select#especie" + index, function () {
            getRacaPet($(this).val(), "select#raca" + y);
        });
    }

    function setActionAgend() {
        var max_fields = 5; //maximum input boxes allowed
        var wrapper = $(".agendfields:first").clone(); //fields wrapper
        var container = $(".petregs"); //fields container
        var add_button = ".addmore"; //add button ID
        var count = $('.agendfields.show').length;
        var count_reg = $('.agendfields.hide').length;
        var index = 1;
//caso haja um pet cadastrado sendo exibido na listagem
        if (count_reg == 1) {
            index = count;

//            $.each($(".agendfields"), function (index, value) {
//                $(value).append('<div class="field"><button class="del-reg bts red" data-index=' + (index + 1) + '>Excluir</button></div>');
//            });
            $.each($(".agendfields.show"), function (index, value) {
                //$(value).append('<div class="field"><button class="del-reg bts red" data-index=' + (index + 1) + '>Excluir</button></div>');
                $(value).append('<div class="inner-frame rigth nbrd"><button class="rm-field bts red" data-index=' + (index + 1) + '>-</button></div>');
            });

            $.each($("[id='especie1']"), function (index, value) {
                $(value).attr("id", "especie" + (index + 1));
                $(value).removeClass("required");
            });

            $.each($("[id='raca1']"), function (index, value) {
                $(value).attr("id", "raca" + (index + 1));
                $(value).removeClass("required");
            });

            $.each($("[name='pet_genero\[\]']"), function (index, value) {
                $(value).removeClass("required");
            });

            $.each($("[name='nome_pet\[\]']"), function (index, value) {
                $(value).removeClass("required");
            });

//            var j = 1;
//            for (var i = 1; i <= count; i++) {
//                activeChangePet(i, j);
//                j++;
//            }

//            $("body").on("click", "button.del-reg", function (e) {
//                var bt_index = $(this).attr("data-index");
//                e.preventDefault();
//                var url = '?link=ad02&del_petid=' + $($('[name="pet_id\[\]"]').get(bt_index - 1)).val();
//                $(this).ajaxDelPet(url, true, 'Excluir permanentemente o PET do cadastro?');
//                count--;
//            });
            index++;
        }

        formParse();
        setDateTimeUI();
        setMaskDateTime();
        setMaskPrice();

        $(add_button).click(function (e) { //on add input button click
            var y = index;
            e.preventDefault();
            formUnParse();
            if (count < max_fields && count >= 0) { //max input box allowed
                y++;
                index++;
                wrapper.removeClass("hide");
                var copywrapper = $(wrapper).clone();
//                var wrp_arr = $(copywrapper).find('input, select').toArray();
//
//                $.each(wrp_arr, function (index, elem) {
//                    //console.log(elem);
//                    $(elem).removeClass('required');
//                });
                copywrapper.appendTo(container); //add input box
                $(".agendfields:last").append('<div class="inner-frame rigth nbrd"><button class="rm-field bts red" data-index=' + index + '>-</button></div>');
                //$(".agendfields:last input:first").removeClass("valid").val("");
                //$(".agendfields:last label.invalid").remove();
                //$(".agendfields:last button.del-reg").parent().remove();//remove o botao excluir caso exista
                $("select#raca1:last").attr("id", "raca" + index).html('<option value=""></option>');
                $("select#especie1:last").attr("id", "especie" + index);
                activeChangePet(index, y);
                setDateTimeUI();
                setMaskDateTime();
                setMaskPrice();
                count++;
                $('#geral_numpets').text(count);
            }
            formParse();
        });

        $("body").on("click", "button.rm-field", function (e) {
            e.preventDefault();
            $("body").off("change", "select#especie" + $(this).attr("data-index"));
            $(this).parent('div').parent("div").remove();
            count--;
            $('#geral_numpets').text(count);
        });

        $(".agendfields:last").on("change", "select#especie1", function () {
            getRacaPet($(this).val(), "select#raca1");
        });

        var max_services = 3;
        var wrapper_service = $(".services:first").clone();

        $("body").on("click", "button.add_service", function (e) {
            var service_list = $($(this).closest('.frm-reg')).next('.services-list');
            var service_last_elemen;
            var count_service = service_list.children('.services').length;
            e.preventDefault();
            if (count_service < max_services) {
                $(wrapper_service).clone().appendTo(service_list);
                service_last_elemen = service_list.children('.services:last');
                $(service_last_elemen).append('<div class="field"><button class="rm-service bts red" data-index=' + count_service + '>X</button></div>');
                $(service_list).next().find('input.num_services').val(count_service + 1);
            }
        });

        $("body").on("click", "button.rm-service", function (e) {
            e.preventDefault();
            var service_list = $($(this).closest('.services-list'));
            var count_service = service_list.children('.services').length;
            calcPrice(this);
            $(this).parent('div').parent('div').remove();
            $(service_list).next().find('input.num_services').val(count_service - 1);
        });

        $("body").on("change", "select.sel-service", function (e) {
            e.preventDefault();
            calcPrice(this);

        });

        $("body").on("keyup", ".money", function (e) {
            e.preventDefault();
            calcPrice(this);
        });

        $("body").on("focusout", ".money", function (e) {
            e.preventDefault();
            if ($(this).val() == 0) {
                $(this).val('0,00');
            }
            calcPrice(this);
        });

    }

    (function ($) {
        $.fn.generateInfoAgend = function () {
            var horarios = [];
            var datas = [];
            var pets = "<br/>";
            var agend = "<br/>";
            var total_serv = 0.0;
            var total = "";


            $.each($('.time').not($('.agendfields.hide').find('.time')), function (index, elem) {
                if ($(elem).val() != "") {
                    horarios[index] = " - " + $(elem).val() + "</p><br/>";
                } else {
                    horarios[index] = "";
                }
            });

            $.each($('.data_agend').not($('.agendfields.hide').find('.data_agend')), function (index, elem) {

                if ($(elem).val() != "" && horarios[index] != "") {
                    datas[index] = "<p>" + $(elem).val();
                    agend += datas[index] + horarios[index];
                } else {
                    agend += "<p class='red'>(Vazio) n&atilde;o ser&aacute; agendado!</p><br/>";
                }
            });

            $.each($('[name="nome_pet\[\]"]').not($('.agendfields.hide').find('[name="nome_pet\[\]"]')), function (index, elem) {
                if ($(elem).val() != "") {
                    pets += "<p><strong>" + $(elem).val() + "</strong></p><br/>";
                } else {
                    pets += "<p class='red'>(Vazio) Este PET n&atilde;o ser&aacute; agendado!</p><br/>";
                }
            });

            $.each($('.service-total').not($('.agendfields.hide').find('.service-total')), function (index, elem) {
                console.log(parseFloat($(elem).val().replace(',', '.')));
                total_serv += parseFloat($(elem).val().replace(',', '.'));
            });

            total_serv = total_serv.toString();
            total_serv = total_serv.replace('.', ',');
            if (total_serv.indexOf(',') == -1) {
                total = total_serv + ',00';
            } else {
                total = total_serv;
            }

            total = 'R$ ' + total;
            $('#geral_pets').html(pets);
            $('#geral_horario').html(agend);
            $('#geral_total').text(total);
            $('#geral_agend').fadeIn(900)
        };
    })(jQuery);

    (function ($) {
        var show = 0;
        $.fn.newAgend = function (page_url, dialog, msg_dialog) {
            if (show === 0) {
                var func_exec = function (obj, data) {
                    $(obj).hide(0);
                    $(obj).html(data).fadeIn(500);
                    setActionAgend();
                };
                if (!isNaN(id) && ((typeof id) == 'number') && id != "") {
                    locPage('?link=agendar&new_cli_agend=true');
                }
                ajaxRequest(this, func_exec, page_url, dialog, msg_dialog);
                show = 1;
            }
        };
    })(jQuery);

    (function ($) {
        var show = 0;
        $.fn.agendID = function (page_url, dialog, msg_dialog) {
            if (show === 0) {

                var func_exec = function (obj, data) {
                    $(obj).hide(0);
                    $(obj).html(data).fadeIn(500);
                    setActionAgend();
                };
                ajaxRequest(this, func_exec, page_url, dialog, msg_dialog);
                show = 1;
            }
        };
    })(jQuery);

    if (!isNaN(id) && ((typeof id) == 'number') && id != "") {
        $('.content-box').agendID('?link=agc02&action=find_agend&id=' + id, false, '');
    }

    if (((typeof new_cli) == 'string') && new_cli != "") {
        $('.content-box').newAgend('?link=agc01&action=new_cli_agend', false, '');
    }

    setAjaxForm(form_id, '#status_msg', 'POST', './forms/agendar_servico_request.php?ajax=true', 'json');

});
