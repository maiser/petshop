
$(document).ready(function ($) {

    $('[name="busca"]').bind('keypress', function (e) {
        if (e.keyCode === 13) {
            var len = $(this).val().length;
            if (len >= 2) {
                $('.lst-table').html('<div class="status"><img src="./img/loader.gif" title="Carregando..." alt="Carregando..." /></div>');
               
                ajaxSearch(this, '.lst-table', '?link=af03');

                $('.field-desc p').fadeOut(400, function () {
                    $(this).fadeIn(400).html("<p class='green'>Busca realizada!</p>");
                });


            } else {
                location.reload();
            }
        }
    }).focus();

});

//function ajaxDelRequest(obj, dialog, page_url, msg_dialog) {
//        ajaxRequest(obj, func_exec, dialog, page_url, msg_dialog);
//    }