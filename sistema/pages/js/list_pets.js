
$(document).ready(function ($) {


    (function ($) {

        $.fn.ajaxDelRequest = function (obj, page_url, dialog, msg_dialog) {
            var func_exec = function (obj, data) {
                $(obj).parent().parent().fadeOut(500, function () {
//                    var parsed = $.parseHTML(data);
//                    $.each(parsed, function (i, el) {
//                         if(el.nodeName === 'P'){
//                             $('.field-desc p').addClass('orange').text($(el).text());
//                             return true;
//                        }
//                    });

                    $(obj).parent().parent().html(data).fadeIn(500).delay(900).fadeOut(900, function () {
                        if ($('.field-desc p strong').length >= 1) {
                            //console.log($($('.field-desc p.info-reg strong').get(0)).text());
                            var reg_exib = $($('.field-desc p.info-reg strong').get(0)).text();
                            var reg_tot = $($('.field-desc p.info-reg strong').get(1)).text();
                            reg_exib--;
                            reg_tot--;
                            if (reg_exib === 0) {
                                location.reload();
                            } else {
                                $($('.field-desc p.info-reg strong').get(0)).text(reg_exib);
                                $($('.field-desc p.info-reg strong').get(1)).text(reg_tot);
                            }

                        }
                    });
                });
            };
            ajaxRequest(obj, func_exec, page_url, dialog, msg_dialog);
        };
    })(jQuery);

    $('[name="busca"]').bind('keypress', function (e) {
        if (e.keyCode === 13) {
            var len = $(this).val().length;
            if (len >= 2) {
                $('.lst-table').html('<div class="status"><img src="./img/loader.gif" title="Carregando..." alt="Carregando..." /></div>');
               
                ajaxSearch(this, '.lst-table', '?link=af01');

                $('.field-desc p').fadeOut(400, function () {
                    $(this).fadeIn(400).html("<p class='green'>Busca realizada!</p>");
                });


            } else {
                location.reload();
            }
        }
    }).focus();

});

//function ajaxDelRequest(obj, dialog, page_url, msg_dialog) {
//        ajaxRequest(obj, func_exec, dialog, page_url, msg_dialog);
//    }