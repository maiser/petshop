$(document).ready(function ($) {
    setFirst();
    formParse();

    var form_id = '#form1';
    var rules_var = {
        errorClass: "invalid",
        validClass: "valid",
        errorElement: "label",
        highlight: function (element, errorClass, validClass) {
            var str1 = element.name;
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';

            $(inputElement).addClass(errorClass).removeClass(validClass);

            if ($(inputElement).next().hasClass(validClass) === true) {
                $(inputElement).next().remove();
            }

        },
        unhighlight: function (element, errorClass, validClass) {
            var str1 = element.name;
            var element_str = str1.replace('[]', '\\[\\]');
            var inputElement = '[name="' + element_str + '"]';
            var validElement = "<label class='valid'></label>";

            $(inputElement).removeClass(errorClass).addClass(validClass);

            if ($(inputElement).next().hasClass(validClass) === false) {

                if ($(inputElement).val() !== "") {
                    $(validElement).insertAfter(element);
                }

            }
        },
        invalidHandler: function (event, validator) {
            //var errors = validator.numberOfInvalids();if (errors) {}
            errorFormValidateMsg();
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        rules: {
            celular: "required_fone",
            telefone: "required_fone"
        },
        groups: {
        },
        messages: {// nome_do_input: "mensagem"
        }
    };

    //validar formulario
    $.validator.addMethod("required", $.validator.methods.required, "");
    $.validator.addMethod("email", $.validator.methods.email, "");
    $.validator.addMethod("maxlength", $.validator.methods.maxlength, "");
    $.validator.addMethod("minlength", $.validator.methods.minlength, "");
    $.validator.addMethod("required_fone", function (value, element, params) {
        if ($('#txt_telefone').val() != "") {
            $('#txt_celular').removeClass('invalid');
            $('#txt_celular').next('label').remove();
            $('#txt_celular').addClass('valid');
            return true;
        }
        if ($('#txt_celular').val() != "") {
            $('#txt_telefone').removeClass('invalid');
            $('#txt_telefone').next('label').remove();
            $('#txt_telefone').addClass('valid');
            return true;
        }

        return false;
    }, "");

    $(form_id).validate(rules_var);
    $('#txt_cep').inputmask("mask", {"mask": "99999999", "placeholder": ""});
    $('#txt_num').inputmask("mask", {"mask": "99999", "placeholder": ""});
    $('#txt_telefone').inputmask("mask", {"mask": "(99) 9999-9999", "placeholder": ""});
    $('#txt_celular').inputmask("mask", {"mask": "(99) 9999-9999[9]", "placeholder": ""});
    $("#txt_cep").keyup(function () {
        if ($(this).val().length === 8) {
            buscacep($(this).val());
        }
    });

    function activeChangePet(index, y) {
        $("body").on("change", "select#especie" + index, function () {
            getRacaPet($(this).val(), "select#raca" + y);
        });
    }

    var max_fields = 5; //maximum input boxes allowed
    var wrapper = $(".petfields:first").clone(); //fields wrapper
    var container = $(".petregs"); //fields container
    var add_button = ".addmore"; //add button ID
    var count = $('.petfields.show').length;
    var count_reg = $('.petfields.hide').length;
    var index = 1;

    //caso haja um pet cadastrado sendo exibido na listagem
    if (count_reg == 1) {
        index = count;

        $.each($(".petfields"), function (index, value) {
            $(value).append('<div class="field"><button class="del-reg bts red" data-index=' + (index + 1) + '>Excluir</button></div>');
        });

        $.each($("[id='especie1']"), function (index, value) {
            $(value).attr("id", "especie" + (index + 1));
            $(value).removeClass("required");
        });

        $.each($("[id='raca1']"), function (index, value) {
            $(value).attr("id", "raca" + (index + 1));
            $(value).removeClass("required");
        });

        $.each($("[name='pet_genero\[\]']"), function (index, value) {
            $(value).removeClass("required");
        });

        $.each($("[name='nome_pet\[\]']"), function (index, value) {
            $(value).removeClass("required");
        });

//        var j = 1;
//        for (var i = 1; i <= count; i++) {
//            activeChangePet(i, j);
//            j++;
//        }

        $("body").on("click", "button.del-reg", function (e) {
            var bt_index = $(this).attr("data-index");
            e.preventDefault();
            var url = '?link=ad02&del_petid=' + $($('[name="pet_id\[\]"]').get(bt_index - 1)).val();
            $(this).ajaxDelPet(url, true, 'Excluir permanentemente o PET do cadastro?');
            count--;
        });
        index++;
    }

    $(add_button).click(function (e) { //on add input button click
        var y = index;
        e.preventDefault();

        if (count < max_fields && count >= 0) { //max input box allowed
            y++;
            index++;
            wrapper.removeClass("hide");
            $(wrapper).clone().appendTo(container); //add input box
            $(".petfields:last").append('<div class="field"><button class="rm-field bts red" data-index=' + index + '>X</button></div>');
            $(".petfields:last input:first").removeClass("valid").val("");
            $(".petfields:last label.invalid").remove();
            $(".petfields:last button.del-reg").parent().remove();
            $("select#raca1:last").attr("id", "raca" + index).html('<option value=""></option>');
            $("select#especie1:last").attr("id", "especie" + index);
            activeChangePet(index, y);
            count++;
        }
    });

    $("body").on("click", "button.rm-field", function (e) {
        e.preventDefault();
        $("body").off("change", "select#especie" + $(this).attr("data-index"));
        $(this).parent('div').parent("div").remove();
        count--;
    });

    $(".petfields:last").on("change", "select#especie1", function () {
        getRacaPet($(this).val(), "select#raca1");
    });

    (function ($) {
        $.fn.ajaxDelPet = function (page_url, dialog, msg_dialog) {
            var func_exec = function (obj, data) {
                $(obj).parent('div').parent('div').fadeOut(500, function () {
                    $(obj).parent('div').parent('div').html(data).fadeIn(500).delay(900).fadeOut(900);
                });
            };
            ajaxRequest(this, func_exec, page_url, dialog, msg_dialog);
        };
    })(jQuery);

    setAjaxForm(form_id, '#status_msg', 'POST', './forms/cad_cliente_pet_request.php?ajax=true', 'json');

});
