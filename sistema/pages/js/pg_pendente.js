
$(document).ready(function ($) {

    setMaskPrice();

    function setMaskPrice() {
        var config = {radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            autoUnmask: true,
            prefix: 'R$ '};

        $.each($('.money'), function (index, elem) {
            $(elem).inputmask("decimal", config);
        });

        $.each($('.desconto'), function (index, elem) {
            $(this).inputmask("decimal", config);
        });
    }

    (function ($) {

        $.fn.ajaxPagServiceRequest = function (obj, page_url, dialog, msg_dialog) {
            var func_exec = function (obj, data) {

                $(obj).closest('.lst-row').fadeOut(500, function () {

                    $(obj).closest('.lst-row').html(data).fadeIn(900, function () {

                        if ($('.field-desc p strong').length >= 1) {
                            //console.log($($('.field-desc p.info-reg strong').get(0)).text());
                            var reg_exib = $($('.field-desc p.info-reg strong').get(0)).text();
                            var reg_tot = $($('.field-desc p.info-reg strong').get(1)).text();
                            reg_exib--;
                            reg_tot--;

                            $($('.field-desc p.info-reg strong').get(0)).text(reg_exib);
                            $($('.field-desc p.info-reg strong').get(1)).text(reg_tot);


                        }
                    });
                });
            };
            console.log('teste');
            ajaxFormRequestP('#' + $(obj).closest('form').attr('id'), obj, 'html', func_exec, './_async.php' + page_url, dialog, msg_dialog);

        };
    })(jQuery);

    (function ($) {

        $.fn.ajaxPagAllServiceRequest = function (obj, page_url, dialog, msg_dialog) {
            var func_exec = function (obj, data) {

                $(obj).closest('.lst-reg').fadeOut(500, function () {

                    $(obj).closest('.lst-reg').html(data).fadeIn(500, function () {

                    });
                });
            };
            console.log('teste');
            ajaxFormRequestP('#' + $(obj).closest('form').attr('id'), obj, 'html', func_exec, './_async.php' + page_url, dialog, msg_dialog);

        };
    })(jQuery);

    (function ($) {

        $.fn.remarkAgend = function (url_redirect, msg_dialog, url_remove) {
            var callback = function () {
                var rm_page = url_remove;
                var rd_page = url_redirect;
                $(this).ajaxDelAgendRequest(this, rm_page, false, '');
                locPage(rd_page);
            }
            myDialog(msg_dialog, 'confirm', "Aten&ccedil;&atilde;o!", callback);
        }
    })(jQuery);


    $('[name="busca"]').bind('keypress', function (e) {
        if (e.keyCode === 13) {
            var len = $(this).val().length;
            if (len >= 2) {
                $('.lst-table').html('<div class="status"><img src="./img/loader.gif" title="Carregando..." alt="Carregando..." /></div>');

                ajaxSearch(this, '.lst-table', '?link=af02', setMaskPrice);
                
                $('.field-desc p').fadeOut(400, function () {
                    $(this).fadeIn(400).html("<p class='green'>Busca realizada!</p>");
                    
                });

            } else {
                location.reload();
            }
        }
    }).focus();

});
