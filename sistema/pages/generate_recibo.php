<?php

global $_CONTROL;
global $I18N;
global $_SS;
$userule = $_SS->get_var('user_perm');
if ($userule < 6000) {
    header('Location: 405.php');
}
$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$tpl->addFile('PAGE_HEAD', './pages/blocks/page_head.html');


// Incluindo scripts espec�ficos para esta p�gina
//$_CONTROL->getCustomCss('./css/print_recibo.css');

$id_fatura = $_CONTROL->getGETVar('id');
$id_fatura = Protection::makeSafeVar($id_fatura);

$detalhado = $_CONTROL->getGETVar('detalhado');
$detalhado = Protection::makeSafeVar($detalhado);

$_CONTROL->getCustomCssPrint('./pages/css/generate_recibo.css');

$tpl->LBL_TITLE = $I18N->getr('Imprimir recibo');
$tpl->DESC_PAGE = $I18N->getr('Imprima o recibo de pagamento');

$tpl->ENDERECO = $_CONTROL->getConfig('sys_endereco');
$tpl->TEL = $_CONTROL->getConfig('sys_tel');


if ($id_fatura != "" && is_numeric($id_fatura)) {

    $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());


    $query = "SELECT * FROM FATURA WHERE id='" . $id_fatura . "'";
    $db->con->query($query);
    $numrows = $db->con->num_rows;
    $data = $db->con->fetch_rowname();


    $tpl->NUM_REC = sprintf('%08d', $id_fatura);

    $total_s_desconto = $data['total'];
    $desconto = $data['desconto'];

    $total = $data['total'] - $data['desconto'];
    $total = str_replace('.', ',', $total, $count);
    if ($count) {
        $tpl->VAL_REC = 'R$ ' . $total;
    } else {
        $tpl->VAL_REC = 'R$ ' . $total . ',00';
    }

    $query = "SELECT nome FROM DONO WHERE id='" . $data['fk_dono'] . "'";
    $db->con->query($query);
    $numrows = $db->con->num_rows;
    $data = $db->con->fetch_rowname();

    $tpl->NOME_CLI = $data['nome'];


    if ($detalhado == 'true') {
        
    } else {

        $_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
        $query = "SELECT * FROM PAGAMENTO WHERE fk_fatura='" . $id_fatura . "'";
        $db->con->query($query);
        $numrows = $db->con->num_rows;

        $db2 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

        for ($i = 0; $i < $numrows; $i++) {

            $list = $db->con->fetch_rowname();
            //$list = Protection::encodeUTF8($list);
            $data = Protection::sanitizeAllTags($list);

            $query = "SELECT PET.nome, SERVICOS.servico, valor_servico, data "
                    . "FROM REL_AGENDA_SERV, PET, SERVICOS, AGENDA "
                    . "WHERE "
                    . "id_agenda='" . $data['id_agenda'] . "' AND "
                    . "PET.id = REL_AGENDA_SERV.id_pet AND "
                    . "SERVICOS.id = REL_AGENDA_SERV.id_servico AND "
                    . "AGENDA.id = id_agenda";
            $db2->con->query($query);
            $numrows2 = $db2->con->num_rows;
            $services = '';
            $eol = "";
            for ($j = 0; $j < $numrows2; $j++) {

                $list = $db2->con->fetch_rowname();
                //$list = Protection::encodeUTF8($list);
                $data2 = Protection::sanitizeAllTags($list);
                $nome_pet = $data2['nome'];
                $data_mes = explode('-', $data2['data']);
                $eol .= '<br/>';
                $services .= '<div class="left pp">' . $data2['servico'] . '</div><div class="right pp">R$ ' . str_replace('.', ',', $data2['valor_servico']) . '</div><br/>';
            }
            
            $data_agend = $data_mes[1] . '/' . $data_mes[2] . '/' . $data_mes[0]; //mm/dd/yyyy

            if (($timestamp = strtotime($data_agend)) !== false) {

                $dia_semana = date('w', $timestamp);
                $dia_semana = $I18N->getDia($dia_semana);
                $mes = date('n', $timestamp);
                $mes = $I18N->getMes($mes);
                $ano = $data_mes[0];
                //$data_agend = $dia_semana . ', ' . $data_mes[2] . ' de ' . $mes . ' de ' . $ano;
            }
            
            $eol .= '<br/>';
            $data_service = '<div class="frm-reg center"><strong>Realizado em: </strong>' . $dia_semana . ', ' . $data_mes[2] . '/' . $data_mes[1] . '/' . $data_mes[0] . '</div>';
            $tpl->NOME_PET = $nome_pet . $eol;
            $tpl->block('BLK_PET');
            $tpl->SERVICES = $data_service . $services;
            $tpl->block('BLK_SERVICES');
            $tpl->block('BLK_PET_SERVICES');

            $tpl->TOTAL_S_DESC = 'R$ ' . str_replace('.', ',', $total_s_desconto);
            $tpl->DESCONTO = '<strong>Desconto:</strong> R$ ' . str_replace('.', ',', $desconto);
            $tpl->DATA = date('d') . ' de ' . $I18N->getMes(date('m')) . ' de ' . date('Y');
        }
    }
}

$tpl->ACT_VOLTAR = '<button type="button" onclick="javascript:history.back()" class="btn blue">' . $I18N->getr('VOLTAR ->>') . '</button>';


$tpl->show();
?>