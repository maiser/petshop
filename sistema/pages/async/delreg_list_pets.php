<?php

header('Content-Type: text/html; charset='.$_CONTROL->getConfig('charset'));

$userule = $_SS->get_var('user_perm');

if ($userule > 6000) {
    $id_dono = filter_input(INPUT_GET, 'id');
    $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

    $count = 0;

    $query = "DELETE FROM DONO WHERE id=" . $db->con->prep($id_dono) . ";";
    $db->con->query($query);
    $numrows = $db->con->num_rows;

    $count += $numrows;

    $query = "DELETE FROM PET WHERE fk_dono=" . $db->con->prep($id_dono) . ";";
    $db->con->query($query);
    $numrows = $db->con->num_rows;

    $count += $numrows;

    if ($count >= 1) {
        $query = "DELETE FROM REL_DONO_PET WHERE id_dono=" . $db->con->prep($id_dono) . ";";
        $db->con->query($query);
        $numrows = $db->con->num_rows;

        if ($numrows) {           
            echo "<div class='msg-box green'></br>Registro exlcu&iacute;do com sucesso!</br></br></div>";
        }else{
            echo "<div class='msg-box red'></br>Refer&ecirc;ncia inv&aacute;lida!</br></br></div>";
        }
    } else {
        echo "<div class='msg-box red'></br>Erro ao tentar excluir o registro!</br></br></div>";
    }

    $db->con->sql_close();
}
?>