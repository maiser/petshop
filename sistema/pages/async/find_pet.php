<?php

$search = str_replace('\\', '\\\\', filter_input(INPUT_GET, 'search'));

if ($search != "" && strlen($search) >= 2) {
    $tpl = new Template($_CONTROL->getTemplate('../templates/list_pets.html'));
    
    $db2 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
    
    $_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
    $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
    
    $tpl->LBL_BT_EDIT = $I18N->getr("Editar");
    $tpl->LBL_BT_DEL = $I18N->getr("Excluir");
    $tpl->DIALOG_MSG_DEL = $I18N->getr("Excluir este registro? Os dados do cliente e seus PETs ser�o removidos do cadastro!");

    $tpl->CSS_HEAD = '_search';
    $page_del = '?link=ad01';
    $page_edit = '?link=cad_cli';
    $page_agend = '?link=agendar';

    $limit = 20;

    $query2 = "SELECT DISTINCT PET.fk_dono as id_dono FROM DONO, PET"
    . " WHERE "
    . " DONO.id = PET.fk_dono "
    . " AND ("
    . " DONO.nome LIKE '%" . $db2->con->prep($search) . "%' "
    . " OR "
    . " PET.nome LIKE '%" . $db2->con->prep($search) . "%' )"
    . " ORDER BY id_dono DESC "
    . " LIMIT 0," . $limit;

    if ($search == '0*') {
        $query2 = " SELECT dn.id as id_dono "
                . " FROM DONO dn"
                . " LEFT JOIN PET pt "
                . " ON dn.id = pt.fk_dono "
                . " WHERE pt.fk_dono IS NULL"
                . " ORDER BY dn.id DESC";
    }
    $db2->con->query($query2);
    $numrows_search = $db2->con->num_rows;

    $aux_id = null;
    $um_pet = false;
    $j = 1;

    for ($i = 0; $i < $numrows_search; $i++) {

        $data2 = $db2->con->fetch_rowname();
        $id_dono = $data2['id_dono'];

        $query = "SELECT "
                . "DONO.nome as nome_dono, tel_fixo, tel_cel "
                . "FROM DONO "
                . "WHERE "
                . "id = '" . $id_dono . "' "
                . "ORDER BY id DESC";

        $db->con->query($query);

        $list = $db->con->fetch_rowname();
        //$list = Protection::encodeUTF8($list);
        $data = Protection::sanitizeAllTags($list);

        $tpl->TXT_DONO1 = '<strong>Dono:</strong> ' . $data['nome_dono'];
        $tpl->TXT_DONO2 = '<strong>Tel.:</strong> ' . $data['tel_fixo'] . ' / <strong>Cel.:</strong> ' . $data['tel_cel'];

        $query2 = "SELECT PET.nome as nome_pet, PET.genero, RACAS.raca, RACAS.tipo_animal "
                . "FROM PET, RACAS "
                . "WHERE "
                . "(PET.fk_dono = '" . $id_dono . "') "
                . "AND (PET.fk_raca = RACAS.id) "
                . "ORDER BY PET.id DESC";

        $db->con->query($query2);
        $numrows = $db->con->num_rows;

        if ($numrows == 0) {
            $tpl->MSG_NENHUM_PET = $I18N->getr("Nenhum PET cadastrado para este dono!");
            $tpl->block("BLK_NENHUM_PET");
        } else {
            $tpl->LBL_BT_AGEND = $I18N->getr('Agendar');
            $tpl->PAGE_AGEND = $page_agend . '&id=' . $id_dono;
            $tpl->block('BLK_BT_AGEND');
        }

        for ($j = 0; $j < $numrows; $j++) {

            $list = $db->con->fetch_rowname();
            //$list = Protection::encodeUTF8($list);
            $data = Protection::sanitizeAllTags($list);

            $tpl->TXT_PET1 = '<strong>Pet:</strong> ' . $data['nome_pet'];
            $tpl->TXT_PET2 = "<strong>Ra&ccedil;a:</strong> " . $data['raca'];
            $tpl->TXT_PET3 = "<strong>Esp&eacute;cie:</strong> " . ($data['tipo_animal'] == "C" ? "C&atilde;o" : "Gato");
            $tpl->TXT_PET4 = "<strong>G&ecirc;nero:</strong> " . $data['genero'];

            if ((($j) % 2) == 0) {
                $tpl->CSS_PET = 'green';
            } else {
                $tpl->CSS_PET = 'gray';
            }

            $tpl->block('BLK_PET');
        }

        if ((($i) % 2) == 0) {
            $tpl->CSS_REG = 'blue';
        } else {
            $tpl->CSS_REG = 'pink';
        }

        $tpl->PAGE_DEL = $page_del . '&id=' . $id_dono;
        $tpl->PAGE_EDIT = $page_edit . '&edit=true&id=' . $id_dono;
        $tpl->block('BLK_REG');
    }

    if ($numrows_search == 0) {
        $tpl->MSG_NOREG = $I18N->getr("Nenhum DONO ou PET encontrado!");
        $tpl->block('BLK_NOREG');
    } else {
        $tpl->MSG_INFO_REG = '<p class="info-reg gray">Sua busca encontrou <strong>' . $numrows_search . '</strong> registros... </p>';
        $tpl->block("BLK_INFO_REG");
    }

    $tpl->show();
    $db->con->sql_close();
    $db2->con->sql_close();
}
?>