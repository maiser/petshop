<?php

$userule = $_SS->get_var('user_perm');

if ($userule > 6000) {
    
    $id_pet = filter_input(INPUT_GET, 'del_petid');
    
    if ($id_pet != "" && is_numeric($id_pet)) {

        $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

        $query = "DELETE FROM PET WHERE id=" . $db->con->prep($id_pet) . ";";
        $db->con->query($query);
        $numrows = $db->con->num_rows;
        $count = $numrows;
        if ($numrows == 1) {
            $query = "UPDATE REL_DONO_PET SET removido=1 WHERE id_pet=" . $db->con->prep($id_pet) . ";";
            $db->con->query($query);
            $numrows2 = $db->con->num_rows;
            $count += $numrows2;
            if ($count == 2) {
                echo "<div class='msg-box green'>PET exlcu&iacute;do com sucesso!</div>";
            }else if($count == 1){
                echo "<div class='msg-box orange'>PET exlcu&iacute;do com sucesso "
                . "mas a rela&ccedil;&atilde;o PET/DONO n&atilde;o pode ser removida!</div>";
            }
        } else {
            echo "<div class='msg-box red'>Erro ao tentar excluir o registro do PET!</div>";
        }

        $db->con->sql_close();
    }
}
?>