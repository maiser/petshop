<?php

$search = str_replace('\\', '\\\\', filter_input(INPUT_GET, 'search'));

if ($search != "" && strlen($search) >= 2) {
    $page_recibo = '?link=gen_recibo';

    $tpl = new Template($_CONTROL->getTemplate('./pages/templates/faturas.html'));
    $tpl->LBL_BT_RECIBO = $I18N->getr('Ver recibo');
    $tpl->CSS_HEAD = '1';
    
    $_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
    $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
    $db2 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

    $query = "SELECT *,FATURA.id as id_fatura FROM FATURA, DONO "
            . "WHERE (FATURA.fk_dono=DONO.id AND DONO.nome LIKE '%" . $search . "%') "
            . "ORDER BY FATURA.id DESC LIMIT 30";

    $db->con->query($query);
    $numrows_tot = $db->con->num_rows;

    for ($i = 0; $i < $numrows_tot; $i++) {
        $data = $db->con->fetch_rowname();
        $tpl->PAGE_RECIBO = $page_recibo . '&id=' . $data['id_fatura'];

        $desconto = str_replace('.', ',', $data['desconto'], $count2);
        $total_s_desconto = str_replace('.', ',', $data['total'], $count3);

        $total = $data['total'] - $data['desconto'];
        $total = str_replace('.', ',', $total, $count);
        if (!$count) {
            $total = $total . ',00';
        }
        if (!$count2) {
            $desconto = $desconto . ',00';
        }
        if (!$count3) {
            $total_s_desconto = $total_s_desconto . ',00';
        }

        $tpl->VAL_FAT = 'R$ ' . $total;
        $tpl->VAL_DESC = 'R$ ' . $desconto;
        $tpl->VAL_S_DESC = 'R$ ' . $total_s_desconto;


        $query2 = "SELECT nome FROM DONO WHERE id='" . $data['fk_dono'] . "'";
        $db2->con->query($query2);
        $data2 = $db2->con->fetch_rowname();

        $tpl->NOME_CLI = $data2['nome'];

        if ($i % 2 == 0) {
            $tpl->CSS_COLOR = 'gray';
        } else {
            $tpl->CSS_COLOR = 'white';
        }
        $tpl->block('BLK_FATURA');
    }

    if ($numrows_tot >= 1) {
        $tpl->block('BLK_FRAME');
    } else {
        $tpl->MSG_NOREG = $I18N->getr("Nenhuma fatura encontrada com essa busca!");
        $tpl->block('BLK_NOREG');
    }
    $tpl->show();
}
?>
