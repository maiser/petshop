<?php

$userule = $_SS->get_var('user_perm');

if ($userule > 6000) {
    $page_recibo = '?link=gen_recibo';
    $id_agend = $_CONTROL->getGETVar('id_agend');
    $id_dono = $_CONTROL->getGETVar('id_dono');

    if ($id_agend != "" && is_numeric($id_agend) && is_numeric($id_dono)) {

        $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
        $all = $_CONTROL->getGETVar('all');
        
        if (($all == 'false') === true) {
            $total = str_replace(',', '.', $_CONTROL->getPOSTVar('service_total'));
            $desconto = str_replace(',', '.', $_CONTROL->getPOSTVar('service_desconto'));
            $key = 0;
            $query = "INSERT INTO FATURA "
                    . "VALUES ("
                    . "NULL, "
                    . "'" . $db->con->prep($total[$key]) . "',"
                    . "'" . $db->con->prep($desconto[$key]) . "',"
                    . "'" . $db->con->prep($id_dono) . "')";
            $db->con->query($query);
            $numrows = $db->con->num_rows;

            if ($numrows == 1) {
                $numrows = 0;
                $id_fatura = $db->con->get_insert_id();

                $service_total = $_CONTROL->getPOSTVar('service_total');
                $service_desconto = $_CONTROL->getPOSTVar('service_desconto');
                $id_agend = $_CONTROL->getPOSTVar('id_agend');
                //$id_dono = $_CONTROL->getPOSTVar('id_dono');


                $service_total[$key] = str_replace(',', '.', $service_total[$key]);
                $service_desconto[$key] = str_replace(',', '.', $service_desconto[$key], $crep);
              
                if ($crep == 0) {
                    $service_desconto[$key] = 0.00;
                }

                $query = "UPDATE PAGAMENTO SET "
                . "total= '" . $service_total[$key] . "', "
                . "desconto='" . $service_desconto[$key] . "', "
                . "fk_fatura='" . $id_fatura . "', "
                . "pago='1',"
                . "data_pg='" . date("Y-m-d H:i:s") . "' "
                . "WHERE id_agenda='" . $db->con->prep($id_agend[$key]) . "' AND "
                . "id_dono='" . $db->con->prep($id_dono) . "'";

                $db->con->query($query);
                $numrows = $db->con->num_rows;

                if ($numrows >= 1) {
                    echo '<div class="msg-box green">Faturado com sucesso!'
                    . '<br/><br/>'
                    . '<button class="btn white" type="button" onclick="locPage(\'' . $page_recibo . '&id=' . $id_fatura . '\');">Imprimir recibo</button>'
                    . '<br/></div>';
                } else {
                    echo '<div class="msg-box red"><br/>N&atilde;o foi poss&iacute;vel atualizar os dados de pagamento!<br/></div>';
                }
            } else {
                echo '<div class="msg-box red"><br/>Erro ao realizar faturamento!<br/></div>';
            }
        }
        else if (($all == 'true') === true) {
            $total = str_replace(',', '.', $_CONTROL->getPOSTVar('total_all'));
            $desconto = str_replace(',', '.', $_CONTROL->getPOSTVar('desconto_all'));
            if($desconto == "") $desconto = 0;
            
            $query = "INSERT INTO FATURA "
                    . "VALUES ("
                    . "NULL, "
                    . "'" . $db->con->prep($total) . "',"
                    . "'" . $db->con->prep($desconto) . "',"
                    . "'" . $db->con->prep($id_dono) . "')";

            $db->con->query($query);
            $numrows = $db->con->num_rows;

            if ($numrows == 1) {
                $numrows = 0;
                $id_fatura = $db->con->get_insert_id();

                $service_total = $_CONTROL->getPOSTVar('service_total');
                $service_desconto = $_CONTROL->getPOSTVar('service_desconto');
                $id_agend = $_CONTROL->getPOSTVar('id_agend');
                $id_dono = $_CONTROL->getPOSTVar('id_dono');

                foreach ($service_total as $key => $value) {
                    $service_total[$key] = str_replace(',', '.', $service_total[$key]);
                    $service_desconto[$key] = str_replace(',', '.', $service_desconto[$key], $crep);
                    if ($crep == 0) {
                        $service_desconto[$key] = 0.00;
                    }
                    
                    $query = "UPDATE PAGAMENTO SET "
                            . "total= '" . $db->con->prep($service_total[$key]) . "', "
                            //. "desconto='" . $service_desconto[$key] . "', "
                            . "fk_fatura='" . $id_fatura . "', "
                            . "pago='1',"
                            . "data_pg='" . date("Y-m-d H:i:s") . "' "
                            . "WHERE id_agenda='" . $db->con->prep($id_agend[$key]) . "' AND "
                            . "id_dono='" . $db->con->prep($id_dono) . "'";

                    $db->con->query($query);
                    $numrows += $db->con->num_rows;
                }
                if ($numrows >= 1) {
                    echo '<div class="msg-box green">Todos os registros foram faturados com sucesso!'
                    . '<br/><br/>'
                    . '<button class="btn white" type="button" onclick="locPage(\'' . $page_recibo . '&id=' . $id_fatura . '\');">Imprimir recibo</button>'
                    . '<br/></div>';
                } else {
                    echo '<div class="msg-box red"><br/>N&atilde;o foi poss&iacute;vel atualizar os dados de pagamento!<br/></div>';
                }
            } else {
                echo '<div class="msg-box red"><br/>Erro ao realizar faturamento!<br/></div>';
            }
        }
        $db->con->sql_close();
    }
}

?>