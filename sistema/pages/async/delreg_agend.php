<?php

$userule = $_SS->get_var('user_perm');

if ($userule > 6000) {
    $page_pag_service = '?link=pend_pg';
    $id_agend = $_CONTROL->getGETVar('id');

    if ($id_agend != "" && is_numeric($id_agend)) {

        $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

        if ($_CONTROL->getGETVar('concluido') == '') {
            //operacao = 2 - desagendamento
            $query = "UPDATE AGENDA SET operacao='2' WHERE id=" . $db->con->prep($id_agend) . ";";
            $db->con->query($query);
            $numrows = $db->con->num_rows;

            if ($numrows == 1) {
                echo "<div class='msg-box green'>PET desagendado com sucesso!</div>";
            } else {
                echo "<div class='msg-box red'>Erro ao tentar desagendar o PET!</div>";
            }
        }
        if ($_CONTROL->getGETVar('concluido') == 'true') {
            $id_dono = $_CONTROL->getGETVar('id_dono');

            if (is_numeric($id_dono)) {
                //operacao = 1 - conclu�do!
                $query = "UPDATE AGENDA SET operacao='1' WHERE id=" . $db->con->prep($id_agend) . ";";
                $db->con->query($query);
                $numrows = $db->con->num_rows;

                if ($numrows == 1) {
                    $query = "UPDATE REL_AGENDA_SERV SET realizado='1' WHERE id_agenda=" . $db->con->prep($id_agend) . ";";
                    $db->con->query($query);
                    $numrows = $db->con->num_rows;

                    if ($numrows >= 1) {

                        $query = "INSERT INTO PAGAMENTO "
                                . "VALUES ("
                                . "NULL,"
                                . "'" . $db->con->prep($id_agend) . "',"
                                . "'".$id_dono."',"
                                . "NULL,"//fk_fatura
                                . "'0.00',"//preco
                                . "'0.00',"//desconto
                                . "0,"//pago?
                                . "NULL,"//data_pg
                                . "'".date("Y-m-d H:i:s")."')";
                        $db->con->query($query);
                        $numrows = $db->con->num_rows;
                        
                        if ($numrows == 1) {
                            echo "<div class='msg-box green'>O servi&ccedil;o foi inclu&iacute;do na lista de pagamento!"
                            . "<br/><br/>"
                                    . '<button class="btn orange" type="button" onclick="locPage(\''.$page_pag_service.'\');" >Realizar pagamento</button>'
                                    . "<br/></div>";
                        }else{
                            echo "<div class='msg-box red'>Problemas ao incluir servi&ccedil;o na lista de pagamento!</div>";
                        }
                        
                    } else {
                        echo "<div class='msg-box red'>Problemas ao atualizar conclus&atilde;o de servi&ccedil;o!</div>";
                    }
                } else {
                    echo "<div class='msg-box red'>Erro ao tentar conc PET!</div>";
                }
            }else{
                 echo "<div class='msg-box red'>Problemas com a identificacao do registro</div>";
            }
        }
        $db->con->sql_close();
    }
}
?>