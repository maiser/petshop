<?php

$tipo = filter_input(INPUT_GET, 'tipo');

if (isset($tipo) && $tipo != "") {
    //$_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
    $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

    $query = "SELECT * FROM RACAS WHERE tipo_animal='" . $db->con->prep($tipo) . "'";
    $db->con->query($query);
    $numrows = $db->con->get_num_rows();

    $option = '<option value="">Selecione um ra&ccedil;a</option>';
    for ($i = 0; $i < $numrows; $i++) {
        $data = $db->con->fetch_rowname();
        $option .= '<option value="' . trim($data['id']) . '">' . trim($data['raca']) . '</option>' . "\r\n";
    }

    echo $option;
} else {
    echo '<option value="">Selecione um animal</option>';
}
?>