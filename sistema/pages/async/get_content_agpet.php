<?php

$action = $_CONTROL->getGETVar('action');
$action = Protection::makeSafeVar($action);

if (isset($action) && $action != "") {
    $_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
    $temp_page = './pages/content/agendar_novo_cliente.html';

    if ($action == 'new_cli_agend') {
        $tpl = new Template($temp_page);
        $tpl->BT_NAME = 'bt_agendar';
        $tpl->BT_VALUE = $I18N->getr('agend_new');
        $tpl->BT_LABEL = $I18N->getr("Incluir na agenda");
        $tpl->BT_INFO_LABEL = $I18N->getr('Info. Agend');
        $tpl->PET_REGS_VISIBLE = "show";


        $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
        $query = "SELECT DISTINCT tipo_animal FROM RACAS";
        $db->con->query($query);
        $numrows = $db->con->num_rows;


        $tpl->OPT_VAZIO = '<option value=""></option>';
        for ($i = 0; $i < $numrows; $i++) {

            $data = $db->con->fetch_rowname();
            $tpl->OPT_ANIMAL_VALUE = $data['tipo_animal'];
            if ($data['tipo_animal'] == 'C') {
                $tpl->OPT_ANIMAL_DESC = "C&atilde;o";
            }
            if ($data['tipo_animal'] == 'G') {
                $tpl->OPT_ANIMAL_DESC = "Gato";
            }

            $tpl->block('BLK_OPT_ANIMAL_FULL');
        }
        $tpl->block('BLK_ANIMAL_FULL');
        $tpl->block('BLK_RACA');
        $tpl->block('BLK_OPT_GENERO');

        $query = "SELECT id, servico, preco FROM SERVICOS";
        $db->con->query($query);
        $numrows = $db->con->num_rows;
        $span_precos = "";

        for ($i = 0; $i < $numrows; $i++) {
            $data = $db->con->fetch_rowname();
            $tpl->OPT_SERVICE_VALUE = $data['id'];
            $tpl->OPT_SERVICE_DESC = Protection::encodeUTF8($data['servico']);
            $tpl->block('BLK_OPT_SERVICE');
            $span_precos .= '<span service-id=' . $data['id'] . '>' . $data['preco'] . '</span>' . "\r\n";
        }
        $tpl->SPAN_PRECOS = $span_precos;

        $tpl->block('BLK_AGEND_REGS');
        $tpl->block('BLK_NEW_CLI');

        $tpl->show();
    } else if ($action == 'find_agend') {
        $tpl = new Template($temp_page);
        $id = $_CONTROL->getGETVar('id');
        $id = Protection::makeSafeVar($id);

        $tpl->BT_VALUE = $I18N->getr('Incluir na agenda');
        $tpl->BT_LABEL = $I18N->getr("Incluir na agenda");
        $tpl->TXT_ID = $id;
        $tpl->BT_NAME = 'bt_agendar_include';
        $tpl->BT_INFO_LABEL = $I18N->getr('Info. Agend');

        //adiciona os campos invisiveis na p�gina
        $tpl->PET_REGS_VISIBLE = "hide";
        $tpl->OPT_GENERO_SELECTED = "";
        $tpl->NOME_PET = "";
        $tpl->HIDDEN_VALUE_PET_ID = "";

        $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
        $query = "SELECT DISTINCT tipo_animal FROM RACAS";
        $db->con->query($query);
        $numrows = $db->con->num_rows;

        $tpl->OPT_VAZIO = '<option value=""></option>';
        for ($i = 0; $i < $numrows; $i++) {

            $data = $db->con->fetch_rowname();
            $tpl->OPT_ANIMAL_VALUE = $data['tipo_animal'];
            if ($data['tipo_animal'] == 'C') {
                $tpl->OPT_ANIMAL_DESC = "C&atilde;o";
            }
            if ($data['tipo_animal'] == 'G') {
                $tpl->OPT_ANIMAL_DESC = "Gato";
            }
            $tpl->SELECTED_ANIMAL = "";
            $tpl->block('BLK_OPT_ANIMAL_FULL');
        }
        $tpl->SELECTED_RACA = "";
        $tpl->block('BLK_ANIMAL_FULL');
        $tpl->block('BLK_RACA');

        $query = "SELECT id, servico, preco FROM SERVICOS";
        $db->con->query($query);
        $numrows = $db->con->num_rows;

        $opt_filled_service = "";
        $span_precos = "";

        for ($i = 0; $i < $numrows; $i++) {
            $data = $db->con->fetch_rowname();
            $tpl->OPT_SERVICE_VALUE = $data['id'];
            $tpl->OPT_SERVICE_DESC = $data['servico'];
            $tpl->block('BLK_OPT_SERVICE');
            $opt_filled_service .= '<option value="' . $data['id'] . '">' . $data['servico'] . '</option>' . "\r\n";
            $span_precos .= '<span service-id=' . $data['id'] . '>' . $data['preco'] . '</span>' . "\r\n";
        }
        $tpl->FILLED_OPT_SERVICE = "";
        $tpl->SPAN_PRECOS = $span_precos;

        $tpl->block('BLK_PET_ID');
        $tpl->block('BLK_OPT_GENERO');
        $tpl->block('BLK_AGEND_REGS');

        $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
        $query = "SELECT * "
                . " FROM DONO "
                . " WHERE "
                . " id = '" . $db->con->prep($id) . "' "
                . " ORDER BY id DESC";

        $db->con->query($query);

        $list = $db->con->fetch_rowname();
        //$list = Protection::encodeUTF8($list);
        $data = Protection::sanitizeAllTags($list);

        $tpl->TXT_NOME_CLIENTE = $data['nome'];
        $tpl->TXT_TELEFONE = $data['tel_fixo'];
        $tpl->TXT_CELULAR = $data['tel_cel'];
        $tpl->OPT_VAZIO = "";

        $query2 = "SELECT PET.id as pet_id, PET.nome as nome_pet, RACAS.id as raca_id, PET.genero, RACAS.raca, RACAS.tipo_animal "
                . "FROM PET, RACAS "
                . "WHERE "
                . "(PET.fk_dono = " . $db->con->prep($id) . ") "
                . "AND (PET.fk_raca = RACAS.id) "
                . "ORDER BY PET.id ASC";

        $db->con->query($query2);
        $numrows = $db->con->num_rows;

        for ($i = 0; $i < $numrows; $i++) {

            $list = $db->con->fetch_rowname();
            //$list = Protection::encodeUTF8($list);
            $data = Protection::sanitizeAllTags($list);

            $tpl->NOME_PET = $data['nome_pet'];

            if ($data['tipo_animal'] == 'C') {
                $tpl->OPT_ANIMAL_DESC = "C&atilde;o";
            }
            if ($data['tipo_animal'] == 'G') {
                $tpl->OPT_ANIMAL_DESC = "Gato";
            }
            $tpl->OPT_ANIMAL_VALUE = $data['tipo_animal'];

            $tpl->SELECTED_ANIMAL = 'selected="selected"';
            $tpl->block('BLK_OPT_ANIMAL');
            $tpl->block('BLK_ANIMAL');

            $tpl->OPT_RACA_VALUE = $data['raca_id'];
            $tpl->OPT_RACA_DESC = trim(utf8_encode($data['raca']));
            $tpl->SELECTED_RACA = 'selected="selected"';
            $tpl->block('BLK_OPT_RACA');
            $tpl->block('BLK_RACA');

            if ($data['genero'] == 'M') {
                $genero = 'Macho';
            }
            if ($data['genero'] == 'F') {
                $genero = 'F&ecirc;mea';
            }
            $tpl->OPT_GENERO_SELECTED = '<option value="' . $data['genero'] . '">' . $genero . '</option>';

            $tpl->PET_REGS_VISIBLE = "show";
            $tpl->HIDDEN_VALUE_PET_ID = $data['pet_id'];
            $tpl->FILLED_OPT_SERVICE = $opt_filled_service;
            $tpl->block('BLK_PET_ID');
            $tpl->block('BLK_AGEND_REGS');
        }

        $tpl->block('BLK_NEW_CLI');

        $tpl->show();
        $db->con->sql_close();
    }
}
?>