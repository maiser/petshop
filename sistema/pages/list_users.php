<?php
global $_CONTROL;
global $I18N;
global $_SS;
/* Instanciando objetos necessários para este script php */
$tpl = new Template($_CONTROL -> getTemplate(__FILE__));
$db = new DB($_CONTROL -> getConfig('sgbd', 0), $_CONTROL -> getConfigVar());

/* Descrição do formulário / página */
$tpl -> LBL_TITLE = $I18N -> getr('Usu&aacute;rios do sistema');
$tpl -> DESC_PAGE = $I18N -> getr('Exibindo a lista de usu&aacute;rios do sistema Kiloutou.');

/* Header da tabela */					
$tpl -> TXT_REG1 = $I18N -> getr('UID');
$tpl -> TXT_REG2 = $I18N -> getr('PERMISS&Atilde;O');

//$tpl -> TXT_REG5 = $I18N -> getr('FUN&Ccedil;&Otilde;ES');
$tpl -> block('BLK_TBTITLE');	


/** Exclusão de registro **/

/* Primeiro criamos a query */
$query = "SELECT * FROM KLT_USERS";

/* Realizando a consulta com a query criada 
 * O valor de retorno fica no objeto instânciado do banco de dados (no caso  a variável $db)*/
$db -> con -> query($query);
$numrows = $db -> con -> get_num_rows();

/* (opcional) gera um paginador para a consulta realizada em $db */
$paginator = $db -> pageRows($query, $numrows, 20, '?link=list_users', $_CONTROL -> getGETVar('page'));
$db -> con -> query($paginator[0]);



$tpl -> TXT_PAGINATOR = $paginator[1];
$tpl -> VAL_PART_PAG = $db -> con -> get_num_rows();
$tpl -> VAL_TOTAL_PAG = $numrows;

	

	//gerando tuplas dos registros
	for ($i = 0; $i < $db -> con -> get_num_rows(); $i++) {
	
		$data = $db -> con -> fetch_row();
		
		$tpl -> TXT_REG1 = $data['UID'];
		$tpl -> TXT_REG2 = $data['USER_PERM'];
		//se existir inscrição estadual então cadastre
		
		/*if($_SS->get_var('user_perm') >= 6000){
			$tpl -> VIEW_REG = $I18N -> getr('Ver registro');
			$tpl -> LINK_ACT_EDIT = "?link=cad_cli&update=&id=".$data['PK_CLI'];
			$tpl -> LINK_VIEW_REG = "?link=cad_cli&view=1&id=".$data['PK_CLI'];
		}
		
		if($_SS->get_var('user_perm') >= 6000){
			$tpl -> ACT_EDIT = $I18N -> getr('Editar');
			$tpl -> LINK_ACT_EDIT = "?link=cad_cli&update=&id=".$data['PK_CLI'];
		}
		
		if($_SS->get_var('user_perm') >= 6000){
			$tpl -> ACT_DEL = $I18N -> getr('Excluir');
			$tpl -> LINK_ACT_DEL = "?link=list_cli&del=".$data['PK_CLI'];
		}*/
		
		$tpl -> CSS = 'gray'; 
		
		$tpl -> block('BLK_REG');
		
	}

$tpl->ACT_VOLTAR = '<a href="javascript:history.back()" id="lk_voltar">'. $I18N->getr('VOLTAR ->>'). '</a>';
$db->con->sql_close();
$tpl -> show();

/*
 $query = "SELECT type, COUNT(name) FROM products GROUP BY type";

 $result = mysql_query($query) or die(mysql_error());

 // Print out result
 while($row = mysql_fetch_array($result)){
 echo "There are ". $row['COUNT(name)'] ." ". $row['type'] ." items.";
 echo "<br />";
 }

 */
/*if(isset($_CONTROL->getGETVar('group'))){

 }*/

/**
 * Aqui é possível usar dois métodos de envio do formulário.
 * No caso estamos usando o método assíncrono via JQuery.
 * 
 * $tpl->FRM_ACTION = '?link=list_equip'; (o action do form)
 */
//
// '?link=cad_cli';
//require_once('./forms/form_cad_cliente.class.php');
//$valid = new FormValidator();
//$valid->string($this->razao_social);
//$valid->CNPJ("47.526.417/0001-56");

?>