<?php

global $_CONTROL;
global $I18N;
global $_SS;
$userule = $_SS->get_var('user_perm');
if ($userule < 6000) {
    header('Location: 405.php');
}
$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$tpl->addFile('PAGE_HEAD', './pages/blocks/page_head.html');
$tpl->LBL_TITLE = $I18N->getr('Cadastrar cliente e PET');

// Incluindo scripts espec�ficos para esta p�gina
$_CONTROL->getCustomJs('./js/jquery.inputmask.min.js');
$_CONTROL->getCustomJs('./js/jquery.form.min.js');
$_CONTROL->getCustomJs('./js/jquery.validate.min.js');
$_CONTROL->getCustomJs('./js/jquery.msgbox.min.js');
$_CONTROL->getCustomCss('./css/msgbox.css');

$id = $_CONTROL->getGETVar('id');
$id = Protection::makeSafeVar($id);
$edit = $_CONTROL->getGETVar('edit');
$edit = Protection::makeSafeVar($edit);
$view = $_CONTROL->getGETVar('view');
$view = Protection::makeSafeVar($view);

$_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
$db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$query = "SELECT DISTINCT tipo_animal FROM RACAS";
$db->con->query($query);
$numrows = $db->con->num_rows;


$tpl->OPT_VAZIO = '<option value=""></option>';
for ($i = 0; $i < $numrows; $i++) {

    $data = $db->con->fetch_rowname();
    $tpl->OPT_ANIMAL_VALUE = $data['tipo_animal'];
    if ($data['tipo_animal'] == 'C') {
        $tpl->OPT_ANIMAL_DESC = "C&atilde;o";
    }
    if ($data['tipo_animal'] == 'G') {
        $tpl->OPT_ANIMAL_DESC = "Gato";
    }

    $tpl->block('BLK_OPT_ANIMAL_FULL');
}
$tpl->block('BLK_ANIMAL_FULL');
$tpl->block('BLK_RACA');


if ($edit == "") {
    $tpl->LBL_TITLE = $I18N->getr('Cadastrar cliente e PET');
    $tpl->DESC_PAGE = $I18N->getr('Use este formul&aacute;rio para cadastrar o cliente e seu(s) pet(s).');
    $tpl->BT_NAME = 'bt_cad';
    $tpl->BT_VALUE = $I18N->getr('Cadastrar');
    $tpl->BT_LABEL = $I18N->getr("Cadastrar");
    $tpl->PET_REGS_VISIBLE = "show";
    $tpl->block('BLK_OPT_GENERO');
    $tpl->block('BLK_PET_REGS');
} else if ($edit == "true" && is_numeric($id)) {
    $tpl->LBL_TITLE = $I18N->getr('Atualizar cadastro');
    $tpl->DESC_PAGE = $I18N->getr('Use este formul&aacute;rio para alterar os dados do cliente e pet');
    $tpl->BT_VALUE = $I18N->getr('Atualizar');
    $tpl->BT_LABEL = $I18N->getr("Atualizar");
    $tpl->TXT_ID = $id;
    $tpl->BT_NAME = 'bt_update';

    //adiciona os campos invisiveis na p�gina
    $tpl->PET_REGS_VISIBLE = "hide";
    $tpl->OPT_GENERO_SELECTED = "";
    $tpl->NOME_PET = "";
    $tpl->HIDDEN_VALUE_PET_ID = "";
    $tpl->block('BLK_PET_ID');
    $tpl->block('BLK_OPT_GENERO');
    $tpl->block('BLK_PET_REGS');

    $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
    $query = "SELECT * "
            . " FROM DONO "
            . " WHERE "
            . " id = '" . $id . "' "
            . " ORDER BY id DESC";

    $db->con->query($query);
    
    $list = $db->con->fetch_rowname();
    //$list = Protection::encodeUTF8($list);
    $data = Protection::sanitizeAllTags($list);
    
    $tpl->TXT_NOME_CLIENTE = $data['nome'];
    $tpl->TXT_CEP = $data['cep'];
    $tpl->TXT_CIDADE = $data['cidade'];
    $tpl->TXT_UF = $data['uf'];
    $tpl->TXT_COUNTRY = $data['country'];
    $tpl->TXT_ENDERECO = $data['endereco'];
    $tpl->TXT_BAIRRO = $data['bairro'];
    $tpl->TXT_NUM = $data['numero'];
    $tpl->TXT_COMPLEMENTO = $data['complemento'];
    $tpl->TXT_TELEFONE = $data['tel_fixo'];
    $tpl->TXT_CELULAR = $data['tel_cel'];
    $tpl->TXT_EMAIL = $data['email'];
    $tpl->OPT_VAZIO = "";
    
    $query2 = "SELECT PET.id as pet_id, PET.nome as nome_pet, RACAS.id as raca_id, PET.genero, RACAS.raca, RACAS.tipo_animal "
            . "FROM PET, RACAS "
            . "WHERE "
            . "(PET.fk_dono = '" . $id . "') "
            . "AND (PET.fk_raca = RACAS.id) "
            . "ORDER BY PET.id ASC";

    $db->con->query($query2);
    $numrows = $db->con->num_rows;
    
    for ($i = 0; $i < $numrows; $i++) {

        $list = $db->con->fetch_rowname();
        //$list = Protection::encodeUTF8($list);
        $data = Protection::sanitizeAllTags($list);

        $tpl->NOME_PET = $data['nome_pet'];

        if ($data['tipo_animal'] == 'C') {
            $tpl->OPT_ANIMAL_DESC = "C&atilde;o";
        }
        if ($data['tipo_animal'] == 'G') {
            $tpl->OPT_ANIMAL_DESC = "Gato";
        }
        $tpl->OPT_ANIMAL_VALUE = $data['tipo_animal'];
        $tpl->block('BLK_OPT_ANIMAL');
        $tpl->block('BLK_ANIMAL');

        $tpl->OPT_RACA_VALUE = $data['raca_id'];
        $tpl->OPT_RACA_DESC = trim($data['raca']);
        $tpl->block('BLK_OPT_RACA');
        $tpl->block('BLK_RACA');

        if ($data['genero'] == 'M') {
            $genero = 'Macho';
        }
        if ($data['genero'] == 'F') {
            $genero = 'F&ecirc;mea';
        }
        $tpl->OPT_GENERO_SELECTED = '<option value="' . $data['genero'] . '">' . $genero . '</option>';


        $tpl->PET_REGS_VISIBLE = "show";
        $tpl->HIDDEN_VALUE_PET_ID = $data['pet_id'];
        $tpl->block('BLK_PET_ID');
        $tpl->block('BLK_PET_REGS');
    }



    $db->con->sql_close();
} else if ($view == "true" && is_numeric($id)) {
    $tpl->LOCK_TXT = 'readonly="readonly"';
    $_CONTROL->getCustomCssPrint('./css/print.css'); //MyFormValidate
    $tpl->LBL_TITLE = $I18N->getr('Cliente: ') . $tpl->TXT_RAZ_SOCIAL;
    $tpl->DESC_PAGE = $I18N->getr('Informa&ccedil;&otilde;es de cadastro do cliente');
    $tpl->CST_CSS = '<style>input{border:0px;}#header,#footer{display:none;}</style>';

    $tpl->BT_NAME = "bt_cad";
    $tpl->BT_VALUE = $I18N->getr("Cadastrar");
    $tpl->BT_LABEL = $I18N->getr("Cadastrar");
} else {
    header("Location: ?link=cad_cli&invalid=1");
}

$tpl->ACT_VOLTAR = '<button type="button" onclick="javascript:history.back()" class="btn blue">' . $I18N->getr('VOLTAR ->>') . '</button>';
$tpl->FRM_ACTION = '?link=403';

$tpl->show();
?>