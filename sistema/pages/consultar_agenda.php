<?php

global $_CONTROL;
global $I18N;
global $_SS;
if ($userule = $_SS->get_var('user_perm') < 6500)
    header('Location: 405.php');

//$tpld = new Template("./blocks/page_head.html");

$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$tpl->addFile('PAGE_HEAD', './pages/blocks/page_head.html');

$_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
$db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db2 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db3 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db4 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

$_CONTROL->getCustomJs('./js/jquery.msgbox.min.js');
$_CONTROL->getCustomCSS('./css/msgbox.css');

$tpl->LBL_TITLE = $I18N->getr('Agenda');
$tpl->DESC_PAGE = $I18N->getr('Exibindo o agenda de servi&ccedil;os a realizar.');
$tpl->ACT_VOLTAR = '<button type="button" onclick="javascript:history.back()" class="btn blue">' . $I18N->getr('VOLTAR ->>') . '</button>';

$tpl->LBL_BT_EDIT = $I18N->getr("Editar info.");
$tpl->LBL_BT_DEL = $I18N->getr("Desagendar");
$tpl->LBL_BT_AGEND = $I18N->getr('Remarcar');

$tpl->LBL_BT_CONCLUDE = $I18N->getr('Conclu&iacute;do');

//$tpl->block('BT1');
//$tpl->block('ET1');

$page_del_agend = '?link=ad03'; //remover e concluir agendamento
$page_edit = '?link=cad_cli';
$page_agend = '?link=agendar';

$query2 = "SELECT * FROM AGENDA WHERE operacao='0' ORDER BY data,hora ASC";
$db2->con->query($query2);
$numrows_tot = $db2->con->num_rows;
$numrows_pag = 30;


/* (opcional) gera um paginador para a consulta realizada em $db */
$paginator = $db2->pageRows($query2, $numrows_tot, $numrows_pag, '?link=list_pets', $_CONTROL->getGetVar('page'));
$db2->con->query($paginator[0]);
$numrows_pag = $db2->con->num_rows;
$tpl->TXT_PAGINATOR = $paginator[1];
$tpl->VAL_PART_PAG = $numrows_pag;
$tpl->VAL_TOTAL_PAG = $numrows_tot;

$aux_id = null;
$um_pet = false;
$j = 1;
$css_color = 'green';
$novo_dia = "";
$hoje = date('Y-m-d');
$anterior_diferente = false;

if ($numrows_tot > 0) {
//gerando tuplas dos registros
    for ($i = 0; $i < $numrows_pag; $i++) {

        $list = $db2->con->fetch_rowname();
        //$list = Protection::encodeUTF8($list);
        $data2 = Protection::sanitizeAllTags($list);

        $id_pet = $data2['fk_pet'];
        $id_agenda = $data2['id'];

        $data_mes = explode('-', $data2['data']);
        $data_agend = $data_mes[1] . '/' . $data_mes[2] . '/' . $data_mes[0]; //mm/dd/yyyy

        if (($timestamp = strtotime($data_agend)) !== false) {

            $dia_semana = date('w', $timestamp);
            $dia_semana = $I18N->getDia($dia_semana);
            $mes = date('n', $timestamp);
            $mes = $I18N->getMes($mes);
            $ano = $data_mes[0];
            $data_agend = $dia_semana . ', ' . $data_mes[2] . ' de ' . $mes . ' de ' . $ano;
        }

        $query2 = "SELECT PET.nome as nome_pet, PET.genero, RACAS.raca, RACAS.tipo_animal, fk_dono "
                . "FROM PET, RACAS "
                . "WHERE "
                . "(PET.id = '" . $id_pet . "') "
                . "AND (PET.fk_raca = RACAS.id) "
                . "ORDER BY PET.id DESC";

        $db->con->query($query2);
        $numrows_pet = $db->con->num_rows;

        $tpl->ANCORA = $j;

        if ($numrows_pet == 1) {
            $list = $db->con->fetch_rowname();
            //$list = Protection::encodeUTF8($list);
            $data = Protection::sanitizeAllTags($list);
            
            $hora = explode(':', $data2['hora']);
            $horario = '<strong>Hor&aacute;rio</strong>: ' . $hora[0] . ':' . $hora[1] . 'h';
            
            $tpl->TXT_PET1 = '<strong>Pet:</strong> ' . $data['nome_pet'];
            $tpl->TXT_PET2 = "<strong>Ra&ccedil;a:</strong> " . $data['raca'];
            $tpl->TXT_PET3 = "<strong>Esp&eacute;cie:</strong> " . ($data['tipo_animal'] == "C" ? "C&atilde;o" : "Gato");
            $tpl->TXT_PET4 = "<strong>G&ecirc;nero:</strong> " . $data['genero'];
            $tpl->DIALOG_MSG_DEL = $I18N->getr("Desagendar o pet <strong>" . Protection::SanitizeAllTags($data['nome_pet']) . "</strong>, agendado nesse hor&aacute;rio?");
            $tpl->DIALOG_MSG_CONCLUDE = $I18N->getr("Concluir o(s) servi&ccedil;os para o pet <strong>" . Protection::SanitizeAllTags($data['nome_pet']) . "</strong>? <br/>Ser&aacute; inclu&iacute;do na lista de pagamento!");
            $tpl->DIALOG_REMARK = $I18N->getr("Remarcar o pet  <strong>" . Protection::SanitizeAllTags($data['nome_pet']) . "</strong>? <br/><br/> O hor&aacute;rio <strong>".$hora[0].':'.$hora[1]."h</strong> ser&aacute; liberado!");
            $tpl->block('BLK_PET');



            $query4 = "SELECT * "
                    . "FROM REL_AGENDA_SERV "
                    . "INNER JOIN SERVICOS "
                    . "WHERE "
                    . "((id_pet = " . $id_pet . ") "
                    . "AND (id_agenda = " . $id_agenda . "))"
                    . "AND (SERVICOS.id = REL_AGENDA_SERV.id_servico) "
                    . "ORDER BY id_servico ASC";

            $db4->con->query($query4);
            $numrows_serv = $db4->con->num_rows;

            $service = "";
            for ($k = 0; $k < $numrows_serv; $k++) {
                $list = $db4->con->fetch_rowname();
                //$list = Protection::encodeUTF8($list);
                $data4 = Protection::sanitizeAllTags($list);
                $service .= '<p>' . $data4['servico'] . ' - R$ ' . str_replace('.', ',', $data4['preco']) . '</p>';
            }
            $tpl->LIST_SERVICES = $service;
            $tpl->block('BLK_SERVICE');


            $id_dono = $data['fk_dono'];


            if ($id_dono != $aux_id) {

                $aux_id = $id_dono;

                //verifica se o registro anterior eh do mesmo dono para agrupar 
                if ($anterior_diferente == true) {

                    $j++;
                    $tpl->HORA_AGEND = $horario;
                    $tpl->block('BLK_REG');
                    $anterior_diferente = false;
                }



                $query = "SELECT "
                        . "* "
                        . "FROM DONO "
                        . "WHERE "
                        . "id = " . $id_dono . " "
                        . "ORDER BY id DESC";

                $db3->con->query($query);

                $list = $db3->con->fetch_rowname();
                //$list = Protection::encodeUTF8($list);
                $data3 = Protection::sanitizeAllTags($list);

                $tpl->TXT_DONO1 = '<strong>Dono:</strong> ' . $data3['nome'];
                $tpl->TXT_DONO2 = '<strong>Tel.:</strong> ' . $data3['tel_fixo'] . ' / <strong>Cel.:</strong> ' . $data3['tel_cel'];

                if ($data3['endereco']) {
                    $endereco = '<strong>' . $I18N->getr('Endere&ccedil;o:') . '</strong> '
                            . $data3['endereco']
                            . ', ' . $data3['numero']
                            . ' - ' . $data3['complemento']
                            . '/ <strong>' . $I18N->getr('Bairro:') . '</strong> '
                            . $data3['bairro']
                            . ' - ' . $data3['cidade']
                            . ' / ' . $data3['uf'];

                    $tpl->DONO_ENDERECO = $endereco;
                    $tpl->block('BLK_DONO_ENDERECO');
                }

                $tpl->PAGE_AGEND = $page_agend . '&id=' . $id_dono;
                //$tpl->block('BLK_BT_AGEND');
                $diferente = true;
            } else {
                $diferente = false;
            }

            $tpl->PAGE_CONCLUDE_SERV = $page_del_agend . '&id=' . $id_agenda . '&id_dono=' . $id_dono . '&concluido=true';
            $tpl->PAGE_DEL_AGEND = $page_del_agend . '&id=' . $id_agenda; // . '&id_pet='.$id_pet;
            $tpl->PAGE_EDIT = $page_edit . '&edit=true&id=' . $id_dono;

            if ($data2['data'] != $novo_dia) {
                if ($hoje == $data2['data']) {
                    $data_agend.= ' - (Hoje) ';
                }

                $tpl->DATA = $data_agend;
                $tpl->FDATA = $data_mes[2] . '/' . $data_mes[1] . '/' . $data_mes[0];


                if ($diferente == false) {
                    $j++;
                    $tpl->block('BLK_REG'); //tem que vir primeiro

                    $tpl->PAGE_AGEND = $page_agend . '&id=' . $id_dono;
                    $tpl->block('BLK_DATA');
                    //$tpl->block('BLK_BT_AGEND');
                } else {
                    $tpl->block('BLK_DATA');
                }
            }


            $anterior_diferente = true;
            $tpl->HORA_AGEND = $horario;
            $tpl->block('BLK_HORA_AGEND');
            $tpl->block('BLK_PET_REG');


            if ($data2['data'] != $novo_dia) {
                $novo_dia = $data2['data'];
            }

            if ((($j) % 2) == 0) {
                $tpl->CSS_REG = 'blue';
            } else {
                $tpl->CSS_REG = 'pink';
            }

            $css_color = 'green';
            if ($css_color == 'gray') {
                $tpl->CSS_PET = $css_color;
                $css_color = 'green';
            } else {
                $tpl->CSS_PET = $css_color;
                $css_color = 'gray';
            }
        } else {
            $msg = "<div class='field'><strong class='red'>PET exclu&iacute;do do cadastro! Agora n&atilde;o consta nesse agendamento!</strong></div>";

            $tpl->PAGE_DEL_AGEND = $page_del_agend . '&id=' . $id_agenda;
            $tpl->LBL_BT_DEL2 = $I18N->getr("Remover");
            $tpl->DIALOG_MSG_DEL2 = $I18N->getr("Remover entrada inv&aacute;lida?");
            $tpl->MSG_NENHUM_PET = $msg;
            $tpl->block("BLK_NENHUM_PET");
            $tpl->block('BLK_PET_REG');
        }
    }

    if ((($j) % 2) == 1) {
        $tpl->CSS_REG = 'pink';
    }

    $tpl->ANCORA = $j;
    $tpl->block('BLK_REG');

    $tpl->block('BLK_PAGINATOR_HEAD');
    $tpl->block('BLK_PAGINATOR_FOOT');


    $db->con->sql_close();
    $db2->con->sql_close();
    $db3->con->sql_close();
} else {
    $tpl->MSG_NOREG = $I18N->getr("Nenhum registro na agenda!");
    $tpl->block('BLK_NOREG');
}

$tpl->show();
?>