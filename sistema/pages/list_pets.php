<?php

global $_CONTROL;
global $I18N;
global $_SS;
if(!isset($_SS))    header('Location: ../405.php');
if ($userule = $_SS->get_var('user_perm') < 6500)
    header('Location: 405.php');

//$tpld = new Template("./blocks/page_head.html");

$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$tpl->addFile('PAGE_HEAD', './pages/blocks/page_head.html');

$_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
$db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db2 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

$_CONTROL->getCustomJs('./js/jquery.msgbox.min.js');
$_CONTROL->getCustomCSS('./css/msgbox.css');

$tpl->LBL_TITLE = $I18N->getr('PETs - Capricho no Bixo');
$tpl->DESC_PAGE = $I18N->getr('Exibindo uma lista dos PETs da loja.');
$tpl->ACT_VOLTAR = '<button type="button" onclick="javascript:history.back()" class="btn blue">' . $I18N->getr('VOLTAR ->>') . '</button>';

$tpl->LBL_BT_EDIT = $I18N->getr("Editar");
$tpl->LBL_BT_DEL = $I18N->getr("Excluir");
$tpl->DIALOG_MSG_DEL = $I18N->getr("Excluir este registro? Os dados do cliente e seus PETs ser&atilde;o removidos do cadastro!");

$tpl->block('BT1');
$tpl->block('ET1');

$page_del = '?link=ad01';
$page_edit = '?link=cad_cli';
$page_agend = '?link=agendar';

$query2 = "SELECT DISTINCT id_dono FROM REL_DONO_PET ORDER BY id_dono DESC";
$db2->con->query($query2);
$numrows_tot = $db2->con->num_rows;
$numrows_pag = 15;


/* (opcional) gera um paginador para a consulta realizada em $db */
$paginator = $db2->pageRows($query2, $numrows_tot, $numrows_pag, '?link=list_pets', $_CONTROL->getGetVar('page'));
$db2->con->query($paginator[0]);
$numrows_pag = $db2->con->num_rows;
$tpl->TXT_PAGINATOR = $paginator[1];
$tpl->VAL_PART_PAG = $numrows_pag;
$tpl->VAL_TOTAL_PAG = $numrows_tot;

$aux_id = null;
$um_pet = false;
$j = 1;

//gerando tuplas dos registros
for ($i = 0; $i < $numrows_pag; $i++) {

    $data2 = $db2->con->fetch_rowname();
    $id_dono = $data2['id_dono'];

    $query = "SELECT "
            . "DONO.nome as nome_dono, tel_fixo, tel_cel "
            . "FROM DONO "
            . "WHERE "
            . "id = '" . $id_dono . "' "
            . "ORDER BY id DESC";

    $db->con->query($query);
    
    $list = $db->con->fetch_rowname();
    //$list = Protection::encodeUTF8($list);
    $data = Protection::sanitizeAllTags($list);

    $tpl->TXT_DONO1 = '<strong>Dono:</strong> ' . $data['nome_dono'];
    $tpl->TXT_DONO2 = '<strong>Tel.:</strong> ' . $data['tel_fixo'] . ' / <strong>Cel.:</strong> ' . $data['tel_cel'];


    $query2 = "SELECT PET.nome as nome_pet, PET.genero, RACAS.raca, RACAS.tipo_animal "
            . "FROM PET, RACAS "
            . "WHERE "
            . "(PET.fk_dono = '" . $id_dono . "') "
            . "AND (PET.fk_raca = RACAS.id) "
            . "ORDER BY PET.id DESC";

    $db->con->query($query2);
    $numrows = $db->con->num_rows;

    if ($numrows == 0) {
        $tpl->MSG_NENHUM_PET = $I18N->getr("Nenhum PET cadastrado para este dono!");
        $tpl->block("BLK_NENHUM_PET");
    } else {
        $tpl->LBL_BT_AGEND = $I18N->getr('Agendar');
        $tpl->PAGE_AGEND = $page_agend . '&id=' . $id_dono;
        $tpl->block('BLK_BT_AGEND');
    }

    for ($j = 0; $j < $numrows; $j++) {


        $list = $db->con->fetch_rowname();
        //$list = Protection::decodeUTF8($list);
        $data = Protection::sanitizeAllTags($list);

        $tpl->TXT_PET1 = '<strong>Pet:</strong> ' . $data['nome_pet'];
        $tpl->TXT_PET2 = "<strong>Ra&ccedil;a:</strong> " . $data['raca'];
        $tpl->TXT_PET3 = "<strong>Esp&eacute;cie:</strong> " . ($data['tipo_animal'] == "C" ? "C&atilde;o" : "Gato");
        $tpl->TXT_PET4 = "<strong>G&ecirc;nero:</strong> " . $data['genero'];

        if ((($j) % 2) == 0) {
            $tpl->CSS_PET = 'green';
        } else {
            $tpl->CSS_PET = 'gray';
        }

        $tpl->block('BLK_PET');
    }

    if ((($i) % 2) == 0) {
        $tpl->CSS_REG = 'blue';
    } else {
        $tpl->CSS_REG = 'pink';
    }

    $tpl->PAGE_DEL = $page_del . '&id=' . $id_dono;
    $tpl->PAGE_EDIT = $page_edit . '&edit=true&id=' . $id_dono;
    $tpl->block('BLK_REG');
}

if ($numrows_tot > 0) {
    $tpl->block('BLK_PAGINATOR_HEAD');
    $tpl->block('BLK_PAGINATOR_FOOT');
} else {
    $tpl->MSG_NOREG = $I18N->getr("Nenhum PET cadastrado!");
    $tpl->block('BLK_NOREG');
}

$db->con->sql_close();
$db2->con->sql_close();

$tpl->show();
?>