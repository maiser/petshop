<?php
global $_CONTROL;
global $I18N;

$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$db = new DB($_CONTROL->getConfig('sgbd',0), $_CONTROL->getConfigVar());

/*** CUSTOM JS ***/
$_CONTROL->getCustomJs('./js/jquery.inputmask.js');
$_CONTROL->getCustomJs('./js/jquery.validate.js'); //MyFormValidate
$_CONTROL->getCustomJs('./js/jquery.form.js'); //funcao ajaxForm
/*************/

/*** PAGE ***/
$tpl->LBL_TITLE = $I18N->getr('Cadastrar usu&aacute;rio do sistema');
$tpl->DESC_PAGE = $I18N->getr('Use este formul&aacute;rio para cadastrar usu&aacuterios que far&atilde;o login. O usu&aacute;rio deve estar associado a uma filial por motivos de seguran&ccedil;a.');
/*************/

$query = "SELECT PK_FUNC, NOME FROM KLT_FUNCIONARIOS";
$db->con->query($query);
	
	for ($i = 0; $i < $db -> con -> get_num_rows(); $i++) {
		$data = $db -> con -> fetch_row();
		$tpl->OPT_VALUE = $data['PK_FUNC'];
		
		//if( $data['PK_FUNC'] == $i){
		///	$tpl->OPT_SELECTED = 'selected="selected"';
		//}
		$tpl->OPT_DESC = $data['NOME'];	
		$tpl->block('OPT_RESP');
	}


/*** NO AJAX ***/
$tpl->FRM_ACTION = '?link=403';
/*************/

$tpl->show();
?>