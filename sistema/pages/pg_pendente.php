<?php

global $_CONTROL;
global $I18N;
global $_SS;
if ($userule = $_SS->get_var('user_perm') < 6500)
    header('Location: 405.php');

//$tpld = new Template("./blocks/page_head.html");

$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$tpl->addFile('PAGE_HEAD', './pages/blocks/page_head.html');

$_CONTROL->setConfig('db_charset', $_CONTROL->getConfig('db_decode_charset', 0), 0);
$db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db2 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db3 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db4 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db5 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());


$_CONTROL->getCustomCSS('./css/msgbox.css');
$_CONTROL->getCustomJs('./js/jquery.form.min.js');
$_CONTROL->getCustomJs('./js/jquery.inputmask.min.js');
$_CONTROL->getCustomJs('./js/jquery.inputmask.numeric.extensions.min.js');
$_CONTROL->getCustomJs('./js/jquery.msgbox.min.js');

$tpl->LBL_TITLE = $I18N->getr('Pagamento');
$tpl->DESC_PAGE = $I18N->getr('Exibindo os servi&ccedil;os que foram realizados e est&atilde;o com pagamento pendente.');
$tpl->ACT_VOLTAR = '<button type="button" onclick="javascript:history.back()" class="btn blue">' . $I18N->getr('VOLTAR ->>') . '</button>';

$tpl->LBL_BT_EDIT = $I18N->getr("Editar info.");

$tpl->LBL_BT_PAG = $I18N->getr('(Pagar)');
$tpl->LBL_BT_PAG_ALL = $I18N->getr('Pagar todos');
$tpl->block('BT1');
$tpl->block('ET1');

$page_del_agend = '?link=ad03'; //remover e concluir agendamento
$page_edit = '?link=cad_cli';
$page_agend = '?link=agendar';
$page_pag_service = '?link=ar01';

$query2 = "SELECT * FROM PAGAMENTO WHERE pago='0' ORDER BY data_include DESC";
$db2->con->query($query2);
$numrows_tot = $db2->con->num_rows;
$numrows_pag = 30;


/* (opcional) gera um paginador para a consulta realizada em $db */
$paginator = $db2->pageRows($query2, $numrows_tot, $numrows_pag, '?link=list_pets', $_CONTROL->getGetVar('page'));
$db2->con->query($paginator[0]);
$numrows_pag = $db2->con->num_rows;
$tpl->TXT_PAGINATOR = $paginator[1];
$tpl->VAL_PART_PAG = $numrows_pag;
$tpl->VAL_TOTAL_PAG = $numrows_tot;

$aux_id = null;
$um_pet = false;
$j = 1;
$css_color = 'green';
$novo_dia = "";
$hoje = date('Y-m-d');
$total_all = 0.0;
$flag = false;

if ($numrows_tot > 0) {
//gerando tuplas dos registros
    for ($i = 0; $i < $numrows_pag; $i++) {

        $list = $db2->con->fetch_rowname();
        //$list = Protection::encodeUTF8($list);
        $data2 = Protection::sanitizeAllTags($list);
        $id_agenda = $data2['id_agenda'];

        $query5 = "SELECT * FROM AGENDA WHERE id='" . $id_agenda . "'";
        $db5->con->query($query5);
        $list = $db5->con->fetch_rowname();
        //$list = Protection::encodeUTF8($list);
        $data5 = Protection::sanitizeAllTags($list);

        $id_pet = $data5['fk_pet'];

        $data_mes = explode('-', $data5['data']);
        $data_agend = $data_mes[1] . '/' . $data_mes[2] . '/' . $data_mes[0]; //mm/dd/yyyy

        if (($timestamp = strtotime($data_agend)) !== false) {

            $dia_semana = date('w', $timestamp);
            $dia_semana = $I18N->getDia($dia_semana);
            $mes = date('n', $timestamp);
            $mes = $I18N->getMes($mes);
            $ano = $data_mes[0];
            $data_agend = $dia_semana . ', ' . $data_mes[2] . ' de ' . $mes . ' de ' . $ano;
        }

        $query2 = "SELECT PET.nome as nome_pet, PET.genero, RACAS.raca, RACAS.tipo_animal, fk_dono "
                . "FROM PET, RACAS "
                . "WHERE "
                . "(PET.id = '" . $id_pet . "') "
                . "AND (PET.fk_raca = RACAS.id) "
                . "ORDER BY PET.id DESC";

        $db->con->query($query2);
        $numrows_pet = $db->con->num_rows;

        $tpl->ANCORA = $j;

        if ($numrows_pet == 1) {
            $list = $db->con->fetch_rowname();
            //$list = Protection::encodeUTF8($list);
            $data = Protection::sanitizeAllTags($list);

            if ($hoje == $data5['data']) {
                $dia_semana = 'Hoje';
            }

            $hora = explode(':', $data5['hora']);
            $horario = '<strong>Desde</strong>: ' . $dia_semana . ', ' . $data_mes[2] . '/' . $data_mes[1] . '/' . $data_mes[0] . ' - ' . $hora[0] . ':' . $hora[1] . 'h';

            $tpl->TXT_PET1 = '<strong>Pet:</strong> ' . $data['nome_pet'];
            $tpl->TXT_PET2 = "<strong>Ra&ccedil;a:</strong> " . $data['raca'];
            $tpl->TXT_PET3 = "<strong>Esp&eacute;cie:</strong> " . ($data['tipo_animal'] == "C" ? "C&atilde;o" : "Gato");
            $tpl->TXT_PET4 = "<strong>G&ecirc;nero:</strong> " . $data['genero'];
            $tpl->DIALOG_MSG_PAG = $I18N->getr("Efetuar pagamento dos servi&ccedil;os realizados no pet <strong>" . Protection::SanitizeAllTags($data['nome_pet']) . "</strong>? <br/><br/>Ser&aacute; criada uma fatura para este pagamento!<br/><br/>");
            $tpl->block('BLK_PET');



            $query4 = "SELECT * "
                    . "FROM REL_AGENDA_SERV "
                    . "INNER JOIN SERVICOS "
                    . "WHERE "
                    . "((id_pet = " . $id_pet . ") "
                    . "AND (id_agenda = " . $id_agenda . "))"
                    . "AND (SERVICOS.id = REL_AGENDA_SERV.id_servico) "
                    . "ORDER BY id_servico ASC";

            $db4->con->query($query4);
            $numrows_serv = $db4->con->num_rows;

            $service = "";
            $total_service = 0.0001;

            for ($k = 0; $k < $numrows_serv; $k++) {
                $list = $db4->con->fetch_rowname();
                //$list = Protection::encodeUTF8($list);
                $data4 = Protection::sanitizeAllTags($list);
                $total_service += floatval($data4['preco']);
                $service .= '<p>' . $data4['servico'] . ' - R$ ' . str_replace('.', ',', $data4['preco']) . '</p>';
            }
            $tpl->LIST_SERVICES = $service;
            $tpl->block('BLK_SERVICE');

            $tpl->TXT_TOTAL_SERVICE = str_replace('.', ',', $total_service);

            $id_dono = $data['fk_dono'];


            if ($id_dono != $aux_id) {



                //apenas um flag para corrigir um problema
                if ($flag == true) {

                    if ($count >= 2) {

                        $tpl->TXT_TOTAL_SERVICE_ALL = $total_all;
                        $tpl->DIALOG_MSG_PAG_ALL = $I18N->getr("Efetuar pagamento de TODOS os servi&ccedil;os realizados nos pets do cliente <strong>" . Protection::SanitizeAllTags($data3['nome']) . "</strong>? <br/><br/>Ser&aacute; criada uma &uacute;nica fatura para todos!<br/><br/>");
                        $tpl->PAGE_PAG_SERV = $page_pag_service . '&id_agend=' . $id_agenda . '&id_dono=' . $id_dono . '&all=true';
                        $tpl->block('BLK_PAG_ALL');
                    }

                    $total_all = 0;
                    $j++;
                    $tpl->HORA_AGEND = $horario;
                    $tpl->block('BLK_REG');


                    // $anterior_diferente = false;
                } else {
                    $flag = true;
                }
                $aux_id = $id_dono;

                $query = "SELECT "
                        . "* "
                        . "FROM DONO "
                        . "WHERE "
                        . "id = " . $id_dono . " "
                        . "ORDER BY id DESC";

                $db3->con->query($query);

                $list = $db3->con->fetch_rowname();
                //$list = Protection::encodeUTF8($list);
                $data3 = Protection::sanitizeAllTags($list);

                $tpl->TXT_DONO1 = '<strong>Dono:</strong> ' . $data3['nome'];
                $tpl->TXT_DONO2 = '<strong>Tel.:</strong> ' . $data3['tel_fixo'] . ' / <strong>Cel.:</strong> ' . $data3['tel_cel'];

                if ($data3['endereco']) {
                    $endereco = '<strong>' . $I18N->getr('Endere&ccedil;o:') . '</strong> '
                            . $data3['endereco']
                            . ', ' . $data3['numero']
                            . ' - ' . $data3['complemento']
                            . '/ <strong>' . $I18N->getr('Bairro:') . '</strong> '
                            . $data3['bairro']
                            . ' - ' . $data3['cidade']
                            . ' / ' . $data3['uf'];

                    $tpl->DONO_ENDERECO = $endereco;
                    $tpl->block('BLK_DONO_ENDERECO');
                }

                //$tpl->PAGE_AGEND = $page_agend . '&id=' . $id_dono;
                //$tpl->block('BLK_BT_AGEND');
                //$total_all = 0;
                $count = 1;
                $diferente = true;
            } else {
                $count += 1;
                $diferente = false;
            }

            $total_all += $total_service;
            $tpl->ID_PAG_AGEND = $id_agenda;
            $tpl->PAGE_PAG_SERV = $page_pag_service . '&id_agend=' . $id_agenda . '&id_dono=' . $id_dono . '&all=false';
            $tpl->PAGE_EDIT = $page_edit . '&edit=true&id=' . $id_dono;
            $tpl->ID_DONO = $id_dono;
            /* if ($data5['data'] != $novo_dia) {
              if ($hoje == $data5['data']) {
              $data_agend.= ' - (Hoje) ';
              }

              $tpl->DATA = $data_agend;
              $tpl->FDATA = $data_mes[2] . '/' . $data_mes[1] . '/' . $data_mes[0];


              if ($diferente == false) {
              $j++;
              $tpl->block('BLK_REG'); //tem que vir primeiro
              //$tpl->PAGE_AGEND = $page_agend . '&id=' . $id_dono;
              $tpl->block('BLK_DATA');
              //$tpl->block('BLK_BT_AGEND');
              } else {
              $tpl->block('BLK_DATA');
              }
              } */

            $tpl->HORA_AGEND = $horario;
            $tpl->block('BLK_HORA_AGEND');
            $tpl->block('BLK_PET_REG');

            if ($data5['data'] != $novo_dia) {
                $novo_dia = $data5['data'];
            }

            if ((($j) % 2) == 0) {
                $tpl->CSS_REG = 'blue';
            } else {
                $tpl->CSS_REG = 'pink';
            }

            $css_color = 'green';
            if ($css_color == 'gray') {
                $tpl->CSS_PET = $css_color;
                $css_color = 'green';
            } else {
                $tpl->CSS_PET = $css_color;
                $css_color = 'gray';
            }
        } else {
            $msg = "<div class='field'><strong class='red'>PET exclu&iacute;do do cadastro! Agora n&atilde;o consta nesse agendamento!</strong></div>";

            $tpl->PAGE_DEL_AGEND = $page_del_agend . '&id=' . $id_agenda;
            $tpl->LBL_BT_DEL2 = $I18N->getr("Remover");
            $tpl->DIALOG_MSG_DEL2 = $I18N->getr("Remover entrada inv&aacute;lida?");
            $tpl->MSG_NENHUM_PET = $msg;
            $tpl->block("BLK_NENHUM_PET");
            $tpl->block('BLK_PET_REG');
        }
    }

    if ((($j) % 2) == 1) {
        $tpl->CSS_REG = 'pink';
    }

    if ($count >= 2) {

        $tpl->TXT_TOTAL_SERVICE_ALL = $total_all;
        $tpl->DIALOG_MSG_PAG_ALL = $I18N->getr("Efetuar pagamento de TODOS os servi&ccedil;os realizados nos pets do cliente <strong>" . Protection::SanitizeAllTags($data3['nome']) . "</strong>? <br/><br/>Ser&aacute; criada uma &uacute;nica fatura para todos!<br/><br/>");
        $tpl->PAGE_PAG_SERV = $page_pag_service . '&id_agend=' . $id_agenda . '&id_dono=' . $id_dono . '&all=true';
        $tpl->block('BLK_PAG_ALL');
    }

    $tpl->ANCORA = $j;
    $tpl->block('BLK_REG');

    $tpl->block('BLK_PAGINATOR_HEAD');
    $tpl->block('BLK_PAGINATOR_FOOT');


    $db->con->sql_close();
    $db2->con->sql_close();
    $db3->con->sql_close();
    $db4->con->sql_close();
    $db5->con->sql_close();
} else {
    $tpl->MSG_NOREG = $I18N->getr("Nenhum registro de pagamento pendente!");
    $tpl->block('BLK_NOREG');
}

$tpl->show();
?>