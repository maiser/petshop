<?php

global $_CONTROL;
global $I18N;
global $_SS;
$userule = $_SS->get_var('user_perm');
if ($userule < 6000) {
    header('Location: 405.php');
}

$page_recibo = '?link=gen_recibo';


$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$tpl->addFile('PAGE_HEAD', './pages/blocks/page_head.html');

$tpl->LBL_TITLE = $I18N->getr('Faturas emitidas');
$tpl->DESC_PAGE = $I18N->getr('Uma lista simples de faturas emitidas');
$tpl->LBL_BT_RECIBO = $I18N->getr('Ver recibo');

$db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
$db2 = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

$query = "SELECT * FROM FATURA ORDER BY id DESC";
$db->con->query($query);
$numrows_tot = $db->con->num_rows;
$numrows_pag = 30;

$paginator = $db->pageRows($query, $numrows_tot, $numrows_pag, '?link=faturas', $_CONTROL->getGetVar('page'));
$db->con->query($paginator[0]);
$numrows_pag = $db->con->num_rows;
$tpl->TXT_PAGINATOR = $paginator[1];
$tpl->VAL_PART_PAG = $numrows_pag;
$tpl->VAL_TOTAL_PAG = $numrows_tot;

if ($numrows_pag >= 1) {
    $tpl->block('BLK_PAGINATOR_HEAD');

    $tpl->block('BT1');
    $tpl->block('ET1');

    for ($i = 0; $i < $numrows_pag; $i++) {
        $data = $db->con->fetch_rowname();
        $tpl->PAGE_RECIBO = $page_recibo . '&id=' . $data['id'];

        $desconto = str_replace('.', ',', $data['desconto'], $count2);
        $total_s_desconto = str_replace('.', ',', $data['total'], $count3);

        $total = $data['total'] - $data['desconto'];
        $total = str_replace('.', ',', $total, $count);
        if (!$count) {
            $total = $total . ',00';
        }
        if (!$count2) {
            $desconto = $desconto . ',00';
        }
        if (!$count3) {
            $total_s_desconto = $total_s_desconto . ',00';
        }

        $tpl->VAL_FAT = 'R$ ' . $total;
        $tpl->VAL_DESC = 'R$ ' . $desconto;
        $tpl->VAL_S_DESC = 'R$ ' . $total_s_desconto;


        $query2 = "SELECT nome FROM DONO WHERE id='" . $data['fk_dono'] . "'";
        $db2->con->query($query2);
        $data2 = $db2->con->fetch_rowname();

        $tpl->NOME_CLI = $data2['nome'];

        if ($i % 2 == 1) {
            $tpl->CSS_COLOR = 'gray';
        } else {
            $tpl->CSS_COLOR = 'white';
        }
        $tpl->block('BLK_FATURA');
    }

    $tpl->block('BLK_FRAME');
} else {
    $tpl->MSG_NOREG = $I18N->getr("Nenhuma fatura cadastrada!");
    $tpl->block('BLK_NOREG');
}

$tpl->ACT_VOLTAR = '<button type="button" onclick="javascript:history.back()" class="btn blue">' . $I18N->getr('VOLTAR ->>') . '</button>';


$tpl->show();
?>