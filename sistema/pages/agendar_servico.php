<?php

global $_CONTROL;
global $I18N;
global $_SS;
$userule = $_SS->get_var('user_perm');
if ($userule < 6000) {
    header('Location: 405.php');
}
$tpl = new Template($_CONTROL->getTemplate(__FILE__));
$tpl->addFile('PAGE_HEAD', './pages/blocks/page_head.html');
$tpl->LBL_TITLE = $I18N->getr('Agendar servi&ccedil;o PET');

// Incluindo scripts espec�ficos para esta p�gina
$_CONTROL->getCustomJs('./js/jquery.inputmask.min.js');
$_CONTROL->getCustomJs('./js/jquery.inputmask.date.extensions.min.js');
$_CONTROL->getCustomJs('./js/jquery.inputmask.numeric.extensions.min.js');
$_CONTROL->getCustomJs('./js/jquery.form.min.js');
$_CONTROL->getCustomJs('./js/jquery.validate.min.js');
$_CONTROL->getCustomJs('./js/jquery.ui.min.js');
$_CONTROL->getCustomJs('./js/jquery.ui.timepicker.min.js');
$_CONTROL->getCustomCss('./css/jquery-ui.min.css');
$_CONTROL->getCustomCss('./css/jquery.ui.timepicker.css');


$tpl->LBL_TITLE = $I18N->getr('Agendar servi&ccedil;o para PET');
$tpl->DESC_PAGE = $I18N->getr('Use este formul&aacute;rio para agendar os servi&ccedil;os para os clientes.');


$tpl->ACT_VOLTAR = '<button type="button" onclick="javascript:history.back()" class="btn blue">' . $I18N->getr('VOLTAR ->>') . '</button>';
$tpl->FRM_ACTION = '?link=403';

$tpl->show();
?>