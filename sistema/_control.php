<?php


//carregando as classes automaticamente
function __autoload($class_name) {
    if (file_exists('../class/' . $class_name . '.class.php')) {
        require_once ('../class/' . $class_name . '.class.php');
    } else {
        echo "Classe n&atilde;o existe: '. $class_name . '.class.php";
    }
}

$_SS = new Session(true);

//verifica se o usu�rio est� logado
if ($_SS->var_isset('user') && $_SS->var_isset('hash')) {

    $_CONTROL = new Control('../');
    $_CK = new Cookie();
    header('Content-Type: text/html; charset='.$_CONTROL->getConfig('charset'));
//checando se existe o cookie de idioma
    if ($_CK->varIsset('locale') && !$_CONTROL->getGETvar('locale')) {
        $language = $_CK->getVar('locale');
    } else {
        $language = $_CONTROL->getGETvar('locale', $_CONTROL->site_lang);
    }

//selecao do idioma do site
    $_CONTROL->setSiteLang($language) ? $_CK->setVar('locale', $language) : false;

//setando a pagina do site
    $_CONTROL->setSitePage($_CONTROL->getGETvar('link', 'modules.php'));

//seta o css da pagina
    $_CONTROL->setPageCss();

//seta o JavaScript da pagina
    $_CONTROL->setPageJs();

//registra evento do usuario (acesso as páginas e ações)
    $_CONTROL->registerUserEvent();

//var_dump($_SESSION);
    /*     * ************************************ */
    $I18N = new Translator($_CONTROL->site_lang, '../' . DIR_LOCALE);
    /*     * ************************************ */
} else {
    header('Location: 406.php');
}
?>
