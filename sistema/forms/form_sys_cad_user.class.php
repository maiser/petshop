<?php
require_once('../../class/IMyForm.class.php');
require_once('../../class/FormManipulator.class.php');
require_once('../../class/FormValidator.class.php');

class form_sys_cad_user extends FormManipulator implements IMyForm{
	//variaveis devem ser inicializadas
	protected $user_login="";
	protected $user_perm="";
	protected $pass1="";
	protected $fk_resp="";
	
	private $validated = array();
	
	public function __construct(){
		parent::__construct();
	}
	
	public function validate(){
		$valid = new FormValidator();
		
		return $valid;
	}
	
}
?>