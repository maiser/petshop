<?php

//sleep(1);
if (filter_input(INPUT_GET, 'ajax') == true) {

    function __autoload($class_name) {
        require_once '../../class/' . $class_name . '.class.php';
    }

    require_once('./form_agendar_servico.class.php');
    $_CONTROL = new Control('../../');
    header('Content-Type: application/json; charset=' . $_CONTROL->getConfig('charset'));
    
    //instanciando o formulário para validação
    $frmval = new form_agendar_servico();

    //recebe os valores dos campos por $_POST ou $_GET e seta-os no objeto $frmval
    $frmval->setFormClassVars();

    if (!empty($_POST)) {
        $return['error'] = "";
        $num_servicos = 0;
        $total_serv = 0;
        $count_serv = 0;
        $numrows_pet = 0; //registros afetados na atualizacao e/ou cadastro do pet
        $numrows_pet_insert = 0;
        $numrows_cli_insert = 0;

        if ($_CONTROL->getPOSTVar('bt_agendar') != "") {
            $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());

            $query = "INSERT INTO DONO 
                (id, nome, tel_fixo, tel_cel) 
                 VALUES 
                    (   NULL,"
                    . "'" . $frmval->getVar('nome_cliente') . "',"
                    . "'" . $frmval->getVar('telefone') . "',"
                    . "'" . $frmval->getVar('celular') . "'"
                    . ");";

            $db->con->query($query);
            $numrows_cli_insert = $db->con->num_rows;

            if ($numrows_cli_insert == 1) {
                $id_dono = $db->con->get_insert_id();
                $num_pets = count($frmval->getVar('nome_pet'));

                $return['error'] = false;
                $return['msg'] = "Agendamento realizado com sucesso!";

                $nome_pet = $frmval->getVar('nome_pet');
                $especie = $frmval->getVar('especie');
                $raca = $frmval->getVar('raca');
                $genero = $frmval->getVar('pet_genero');

                for ($i = 0; $i < $num_pets; $i++) {
                    if ($nome_pet[$i] != "" &&
                            $especie[$i] != "" &&
                            $raca[$i] != "") {

                        $query = "INSERT INTO PET VALUES 
                            (   NULL,"
                                . "'" . $nome_pet[$i] . "',"
                                . "'" . $genero[$i] . "',"
                                . "'" . $raca[$i] . "',"
                                . "'" . $id_dono . "'"
                                . ");";

                        $db->con->query($query);
                        $numrows_pet_insert += $db->con->num_rows;

                        if ($numrows_pet_insert >= 1) {
                            $id_pet = $db->con->get_insert_id();
                            $query = "INSERT INTO REL_DONO_PET VALUES (" . $id_pet . "," . $id_dono . ",'" . date("Y-m-d") . "', '0');";
                            $db->con->query($query);
                            $numrows = $db->con->num_rows;

                            if ($numrows == 1) {

                                $data = $frmval->getVar('data');
                                $hora = $frmval->getVar('hora');

                                $query = "INSERT INTO AGENDA"
                                        . " VALUES ("
                                        . "NULL,"
                                        . "'" . $db->toDate($data[$i]) . "',"
                                        . "'" . $hora[$i] . "',"
                                        . "'" . $id_pet . "',"
                                        . "'0'"
                                        . ");";
                                $db->con->query($query);
                                $numrows = $db->con->num_rows;

                                if ($numrows == 1) {
                                    $id_agenda = $db->con->get_insert_id();
                                    $numrows = 0;
                                    $n_serv = $frmval->getVar('num_services');
                                    $num_servicos += $n_serv[$i];
                                    $index_serv = $count_serv;

                                    for ($j = $index_serv; $j < $num_servicos; $j++) {

                                        $id_service = $frmval->getVar('pet_services');
                                        $id_service = $id_service[$j];

                                        $price_service = $frmval->getVar('service_price');
                                        $price_service = str_replace(',', '.', $price_service[$j]);

                                        if ($id_service != "" && is_numeric($id_service)) {
                                            $query = "INSERT INTO REL_AGENDA_SERV "
                                                    . "VALUES ("
                                                    . "'" . $id_agenda . "',"
                                                    . "'" . $id_service . "',"
                                                    . "'" . $id_pet . "',"
                                                    . "'" . $price_service . "',"
                                                    . "'0'"
                                                    . ");";
                                            $db->con->query($query);
                                            $numrows += $db->con->num_rows;
                                            $total_serv += 1;
                                        }
                                        $count_serv++;
                                    }
                                    if ($numrows == 0) {
                                        $return['error'] = true;
                                        $return['msg'] = "Problemas ao tentar realizar cadastro (AGENDA-SERV)!";
                                    }
                                } else {
                                    $return['error'] = true;
                                    $return['msg'] = "Problemas ao tentar realizar agendamento na AGENDA!";
                                }
                            } else {
                                $return['error'] = true;
                                $return['msg'] = "Problemas ao cadastrar relacionamento DONO/PET!";
                            }
                        } else {
                            $return['error'] = true;
                            $return['msg'] = "Problemas ao cadastrar o PET!";
                        }
                    }
                }
            } else {
                $return['error'] = true;
                $return['msg'] = "Houver algum erro ao tentar cadastrar o cliente!";
                //$return['invalid'] = true;
                //$return['fields'] = array("cep" => "Cep errado!");
            }
        } else if ($_CONTROL->getPOSTVar('bt_agendar_include') != "") {

            $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
            $id_dono = $db->con->prep($_CONTROL->getPOSTVar('id'));

            if (!is_numeric($id_dono))
                die("ID ERRADA!");

            $query = "UPDATE DONO SET "
                    . "nome='" . $frmval->getVar('nome_cliente') . "',"
//                    . "cep='" . $frmval->getVar('cep') . "',"
//                    . "cidade='" . $frmval->getVar('cidade') . "',"
//                    . "uf='" . $frmval->getVar('uf') . "',"
//                    . "country='" . $frmval->getVar('country') . "',"
//                    . "endereco='" . $frmval->getVar('endereco') . "',"
//                    . "bairro='" . $frmval->getVar('bairro') . "',"
//                    . "numero='" . $frmval->getVar('numero') . "',"
//                    . "complemento='" . $frmval->getVar('complemento') . "',"
                    . "tel_fixo='" . $frmval->getVar('telefone') . "',"
                    . "tel_cel='" . $frmval->getVar('celular') . "'"
//                    . "email='" . $frmval->getVar('email') . "'"
                    . " WHERE id=" . $id_dono;

            $db->con->query($query);
            $numrows_cli = $db->con->num_rows;

            if ($numrows_cli == 1 || $numrows_cli == 0) {

                $num_pets = count($frmval->getVar('nome_pet'));

                for ($i = 0; $i < $num_pets; $i++) {

                    $pet_id = $frmval->getVar('pet_id');
                    $pet_id = $pet_id[$i];

                    $nome_pet = $frmval->getVar('nome_pet');
                    $especie = $frmval->getVar('especie');
                    $raca = $frmval->getVar('raca');
                    $genero = $frmval->getVar('pet_genero');

                    //atualiza os dados dos pets
                    if ($nome_pet[$i] != "" &&
                            $especie[$i] != "" &&
                            $raca[$i] != "" &&
                            is_numeric($pet_id)) {

                        $query = "UPDATE PET SET "
                                . "nome='" . $nome_pet[$i] . "' "
//                                . "genero='" . $genero[$i] . "',"
//                                . "fk_raca=" . $raca[$i] . " "
                                . "WHERE fk_dono=" . $id_dono . " AND id=" . $pet_id;

                        $db->con->query($query);
                        $numrows_pet += $db->con->num_rows;

                        $data = $frmval->getVar('data');
                        $hora = $frmval->getVar('hora');

                        $query = "INSERT INTO AGENDA"
                                . " VALUES ("
                                . "NULL,"
                                . "'" . $db->toDate($data[$i]) . "',"
                                . "'" . $hora[$i] . "',"
                                . "'" . $pet_id . "',"
                                . "'0'"
                                . ");";
                        $db->con->query($query);
                        $numrows = $db->con->num_rows;

                        if ($numrows == 1) {
                            $id_agenda = $db->con->get_insert_id();
                            $id_pet = $pet_id;
                            $numrows = 0;
                            $n_serv = $frmval->getVar('num_services');
                            $num_servicos += $n_serv[$i];
                            $index_serv = $count_serv;

                            for ($j = $index_serv; $j <= $num_servicos; $j++) {
                                $id_service = $frmval->getVar('pet_services');
                                $id_service = $id_service[$j];

                                $price_service = $frmval->getVar('service_price');
                                $price_service = str_replace(',', '.', $price_service[$j]);

                                if ($id_service != "" && is_numeric($id_service)) {
                                    $query = "INSERT INTO REL_AGENDA_SERV "
                                            . "VALUES ("
                                            . "'" . $id_agenda . "',"
                                            . "'" . $id_service . "',"
                                            . "'" . $id_pet . "',"
                                            . "'" . $price_service . "',"
                                            . "'0'"
                                            . ");";
                                    $db->con->query($query);
                                    $numrows += $db->con->num_rows;
                                    $total_serv += 1;
                                }
                                $count_serv++;
                            }
                            if ($numrows == 0) {
                                $frmval->form_dump();
                                $return['error'] = true;
                                $return['msg'] = "Problemas ao tentar realizar cadastro (AGENDA-SERV)!";
                            }
                        } else {
                            $return['error'] = true;
                            $return['msg'] = "Problemas ao tentar realizar agendamento na AGENDA!";
                        }
                    } else if ($nome_pet[$i] != "" && //insere novos pets no cadastro
                            $especie[$i] != "" &&
                            $raca[$i] != "" &&
                            $pet_id == null) {

                        $query = "INSERT INTO PET VALUES 
                            (   NULL,"
                                . "'" . $nome_pet[$i] . "',"
                                . "'" . $genero[$i] . "',"
                                . "'" . $raca[$i] . "',"
                                . "'" . $id_dono . "'"
                                . ");";

                        $db->con->query($query);
                        $numrows_pet_insert += $db->con->num_rows;

                        if ($db->con->num_rows == 1) {
                            $id_pet = $db->con->get_insert_id();
                            $query = "INSERT INTO REL_DONO_PET VALUES (" . $id_pet . "," . $id_dono . ",'" . date("Y-m-d") . "', '0');";
                            $db->con->query($query);
                            $numrows = $db->con->num_rows;

                            if ($numrows == 1) {

                                $data = $frmval->getVar('data');
                                $hora = $frmval->getVar('hora');

                                $query = "INSERT INTO AGENDA"
                                        . " VALUES ("
                                        . "NULL,"
                                        . "'" . $db->toDate($data[$i]) . "',"
                                        . "'" . $hora[$i] . "',"
                                        . "'" . $id_pet . "',"
                                        . "'0'"
                                        . ");";
                                $db->con->query($query);
                                $numrows = $db->con->num_rows;

                                if ($numrows == 1) {
                                    $id_agenda = $db->con->get_insert_id();
                                    $numrows = 0;
                                    $n_serv = $frmval->getVar('num_services');
                                    $num_servicos += $n_serv[$i];
                                    $index_serv = $count_serv;

                                    for ($j = $index_serv; $j <= $num_servicos; $j++) {
                                        $id_service = $frmval->getVar('pet_services');
                                        $id_service = $id_service[$j];

                                        $price_service = $frmval->getVar('service_price');
                                        $price_service = str_replace(',', '.', $price_service[$j]);

                                        if ($id_service != "" && is_numeric($id_service)) {
                                            $query = "INSERT INTO REL_AGENDA_SERV "
                                                    . "VALUES ("
                                                    . "'" . $id_agenda . "',"
                                                    . "'" . $id_service . "',"
                                                    . "'" . $id_pet . "',"
                                                    . "'" . $price_service . "',"
                                                    . "'0"
                                                    . "');";
                                            $db->con->query($query);
                                            $numrows += $db->con->num_rows;
                                            $total_serv += 1;
                                        }
                                        $count_serv++;
                                    }
                                    if ($numrows == 0) {
                                        $return['error'] = true;
                                        $return['msg'] = "Problemas ao tentar realizar cadastro (AGENDA-SERV)!";
                                        //$frmval->form_dump();
                                    }
                                } else {
                                    $return['error'] = true;
                                    $return['msg'] = "Problemas ao tentar realizar agendamento na AGENDA!";
                                }
                            } else {
                                $return['error'] = true;
                                $return['msg'] = "Problemas ao cadastrar relacionamento DONO/PET!";
                            }
                        } else {
                            $return['error'] = true;
                            $return['msg'] = "Problemas ao cadastrar o PET!";
                        }
                    }
                }
            } else {
                $return['error'] = true;
                $return['msg'] = "Houver algum erro ao tentar atualizar os dados do cliente!";

                //$return['invalid'] = true;
                //$return['fields'] = array("cep" => "Cep errado!");
            }
        }

        if ($return['error'] != true) {
            $return['error'] = false;
            $return['msg'] = '<div class="frm-reg">';
            $return['msg'] = "Agendamento realizado com sucesso!";
            if ($numrows_cli_insert == 1) {
                $return['msg'] .= "<p><br/>Novo cliente inserido no cadastro!</p>";
            }
            if ($numrows_cli == 1) {
                $return['msg'] .= "<p><br/>Informações do cliente atualizadas com sucesso!</p>";
            }
            if ($numrows_pet >= 1) {
                $return['msg'] .= "<p><br/>Dados do PET atualizados com sucesso!</p>";
            }
            if ($numrows_pet_insert >= 1) {
                $return['msg'] .= "<p><br/>Novo(s) PET(s) inseridos no cadastro!</p>";
            }
            if ($total_serv > 0) {
                $return['error'] = false;
                $return['msg'] .= "<p><br/><strong>" . $total_serv . " </strong> servi&ccedil;o(s) foram cadastrados!</p>";
            }
            $return['msg'] .= '</div>';
        }
    }
    $return['msg'] = Protection::encodeUTF8($return['msg']);
    echo json_encode($return);
}
?>