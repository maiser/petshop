<?php

require_once('../../class/IMyForm.class.php');
require_once('../../class/FormManipulator.class.php');
require_once('../../class/FormValidator.class.php');

class form_agendar_servico extends FormManipulator implements IMyForm {

    protected $id = "";//id_dono
    protected $nome_cliente = "";
    protected $telefone = "";
    protected $celular = "";
    protected $nome_pet = "";
    protected $especie = "";
    protected $raca = "";
    protected $pet_genero = "";
    protected $pet_id = "";
    protected $data = "";
    protected $hora = "";
    protected $pet_services = "";//id_service
    protected $service_price = "";
    protected $num_services="";
    
    //array contendo as variaveis validadas
    private $validated = array();

    //constroi a classe pai (FormManipulator)
    public function __construct() {
        parent::__construct();
    }

    public function validate() {
        return true;
    }

}

?>