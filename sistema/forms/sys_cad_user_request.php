<?php
sleep(1);
if(isset($_GET['ajax'])){
	function __autoload($class_name){
		require_once '../../class/'. $class_name . '.class.php';
	}
	require_once('./form_sys_cad_user.class.php');
	$_CONTROL = new Control('../../');
	
	//instanciando o formulário para validação
	$frmval = new form_sys_cad_user();
	
	//recebe os valores dos campos por $_POST ou $_GET e seta-os no objeto $frmval
	$frmval->setFormClassVars();
	
	//valida o formulário de acordo com a classe modelo; retorna true se todos os campos passaram pela validação.
	//caso contrário, retorna um array contendo os campos e mensagens
	$validate = $frmval->validate();//valida formulário
	$validate = $validate->isInvalidForm();//se validou com sucesso então o retorno é false

	//var_dump($validate);
	//formulario foi validado
	if($validate==false){
			
		$db = new DB($_CONTROL->getConfig('sgbd',0), $_CONTROL->getConfigVar());
				
		/** teste basico **/
		$query = 'INSERT INTO KLT_USERS
						VALUES (
						"'.$frmval->getVar('user_login').'",
						"'.$frmval->getVar('user_perm').'",
						"'.$frmval->getVar('pass1').'",
						"'.$frmval->getVar('fk_resp').'"
						)';
					
			$db->con->query($query);
			$return['error'] = false;
			$return['msg'] = "Cadastro realizado com sucesso!";
		
		
	}else{//problemas ao validar o formulario
		$return['error'] = true;
		$return['invalid'] = $validate;
		$return['msg'] = "O formulário não pode ser validado! Confira as informações inseridas!";
	}
	
	//$frmval->form_dump();
	//envia resposta para o cliente
	echo json_encode($return);
}
	
		
?>