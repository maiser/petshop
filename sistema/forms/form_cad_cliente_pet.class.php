<?php

require_once('../../class/IMyForm.class.php');
require_once('../../class/FormManipulator.class.php');
require_once('../../class/FormValidator.class.php');

class form_cad_cliente extends FormManipulator implements IMyForm {

    protected $id = "";
    protected $nome_cliente = "";
    protected $cep = "";
    protected $cidade = "";
    protected $uf = "";
    protected $country = "";
    protected $endereco = "";
    protected $bairro = "";
    protected $numero = "";
    protected $complemento = "";
    protected $telefone = "";
    protected $celular = "";
    protected $email = "";
    protected $nome_pet = "";
    protected $especie = "";
    protected $raca = "";
    protected $pet_genero = "";
    protected $pet_id = "";
    
    //array contendo as variaveis validadas
    private $validated = array();

    //constroi a classe pai (FormManipulator)
    public function __construct() {
        parent::__construct();
    }

    public function validate() {
        return true;
    }

}

?>