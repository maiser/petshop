<?php

if (filter_input(INPUT_GET, 'ajax') == true) {

    function __autoload($class_name) {
        require_once '../../class/' . $class_name . '.class.php';
    }

    require_once('./form_cad_cliente_pet.class.php');
    $_CONTROL = new Control('../../');
    header('Content-Type: application/json; charset=' . $_CONTROL->getConfig('charset'));
    //instanciando o formulário para validação
    $frmval = new form_cad_cliente();

    //recebe os valores dos campos por $_POST ou $_GET e seta-os no objeto $frmval
    $frmval->setFormClassVars();

    if (!empty($_POST)) {
        $return['error'] = "";
        if ($_CONTROL->getPOSTVar('bt_cad') != "") {
            $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
            //$db->con->set_charset('utf8');
            //echo 'ENCODE:'.$db->con->get_encoding();
            $query = "INSERT INTO DONO 
                (id, nome, cep, cidade, uf, country, endereco, bairro, numero, complemento, tel_fixo, tel_cel, email) 
                 VALUES 
                    (   NULL,"
                    . "'" . $frmval->getVar('nome_cliente') . "',"
                    . "'" . $frmval->getVar('cep') . "',"
                    . "'" . $frmval->getVar('cidade') . "',"
                    . "'" . $frmval->getVar('uf') . "',"
                    . "'" . $frmval->getVar('country') . "',"
                    . "'" . $frmval->getVar('endereco') . "',"
                    . "'" . $frmval->getVar('bairro') . "',"
                    . "'" . $frmval->getVar('numero') . "',"
                    . "'" . $frmval->getVar('complemento') . "',"
                    . "'" . $frmval->getVar('telefone') . "',"
                    . "'" . $frmval->getVar('celular') . "',"
                    . "'" . $frmval->getVar('email') . "'"
                    . ");";

            $db->con->query($query);
            $numrows = $db->con->get_affected_rows();



            if ($numrows == 1) {
                $id_dono = $db->con->get_insert_id();
                $num_pets = count($frmval->getVar('nome_pet'));

                $return['error'] = false;
                $return['msg'] = "Dono e PET cadastrados com sucesso!";

                for ($i = 0; $i < $num_pets; $i++) {
                    $nome_pet = $frmval->getVar('nome_pet');
                    $especie = $frmval->getVar('especie');
                    $raca = $frmval->getVar('raca');
                    $genero = $frmval->getVar('pet_genero');
                    if ($nome_pet[$i] != "" &&
                            $especie[$i] != "" &&
                            $raca[$i] != "") {

                        $query = "INSERT INTO PET VALUES 
                            (   NULL,"
                                . "'" . $nome_pet[$i] . "',"
                                . "'" . $genero[$i] . "',"
                                . "'" . $raca[$i] . "',"
                                . "'" . $id_dono
                                . "');";

                        $db->con->query($query);
                        $numrows = $db->con->get_affected_rows();

                        if ($numrows == 1) {
                            $id_pet = $db->con->get_insert_id();
                            $query = "INSERT INTO REL_DONO_PET VALUES (" . $id_pet . "," . $id_dono . ",'" . date("Y-m-d") . "', '0');";
                            $db->con->query($query);
                            $numrows = $db->con->get_affected_rows();

                            if ($numrows != 1) {
                                $return['error'] = true;
                                $return['msg'] = "Problemas ao cadastrar relacionamento DONO/PET!";
                            }
                        } else {
                            $return['error'] = true;
                            $return['msg'] = "Problemas ao cadastrar o PET!";
                        }
                    }
                }
            } else {
                $return['error'] = true;
                $return['msg'] = "Houver algum erro ao tentar cadastrar o cliente!";
                //$return['invalid'] = true;
                //$return['fields'] = array("cep" => "Cep errado!");
            }
        } else if ($_CONTROL->getPOSTVar('bt_update') != "") {

            $db = new DB($_CONTROL->getConfig('sgbd', 0), $_CONTROL->getConfigVar());
            $id_dono = $db->con->prep($_CONTROL->getPOSTVar('id'));
            if (!is_numeric($id_dono))
                die("ID ERRADA!");

            $query = "UPDATE DONO SET "
                    . "nome='" . $frmval->getVar('nome_cliente') . "',"
                    . "cep='" . $frmval->getVar('cep') . "',"
                    . "cidade='" . $frmval->getVar('cidade') . "',"
                    . "uf='" . $frmval->getVar('uf') . "',"
                    . "country='" . $frmval->getVar('country') . "',"
                    . "endereco='" . $frmval->getVar('endereco') . "',"
                    . "bairro='" . $frmval->getVar('bairro') . "',"
                    . "numero='" . $frmval->getVar('numero') . "',"
                    . "complemento='" . $frmval->getVar('complemento') . "',"
                    . "tel_fixo='" . $frmval->getVar('telefone') . "',"
                    . "tel_cel='" . $frmval->getVar('celular') . "',"
                    . "email='" . $frmval->getVar('email') . "'"
                    . " WHERE id=" . $id_dono;

            $db->con->query($query);
            $numrows_cli = $db->con->num_rows;

            if ($numrows_cli == 1 || $numrows_cli == 0) {

                $num_pets = count($frmval->getVar('nome_pet'));
                $numrows_pet = 0; //registros afetados na atualizacao e/ou cadastro do pet
                $numrows_pet_insert = 0;
                for ($i = 0; $i < $num_pets; $i++) {

                    $pet_id = $frmval->getVar('pet_id');
                    $pet_id = $pet_id[$i];
                    $nome_pet = $frmval->getVar('nome_pet');
                    $especie = $frmval->getVar('especie');
                    $raca = $frmval->getVar('raca');
                    $genero = $frmval->getVar('pet_genero');

                    //atualiza os dados dos pets
                    if ($nome_pet[$i] != "" &&
                            $especie[$i] != "" &&
                            $raca[$i] != "" &&
                            is_numeric($pet_id)) {

                        $query = "UPDATE PET SET "
                                . "nome='" . $nome_pet[$i] . "',"
                                . "genero='" . $genero[$i] . "',"
                                . "fk_raca='" . $raca[$i] . "' "
                                . "WHERE fk_dono='" . $id_dono . "' AND id='" . $pet_id . "'";

                        $db->con->query($query);
                        $numrows_pet += $db->con->num_rows;
                    }

                    //insere novos pets no cadastro
                    if ($nome_pet[$i] != "" &&
                            $especie[$i] != "" &&
                            $raca[$i] != "" &&
                            $pet_id == null) {

                        $query = "INSERT INTO PET VALUES 
                            (   NULL,"
                                . "'" . $nome_pet[$i] . "',"
                                . "'" . $genero[$i] . "',"
                                . "'" . $raca[$i] . "',"
                                . "'" . $id_dono
                                . "');";

                        $db->con->query($query);
                        $numrows_pet_insert += $db->con->num_rows;

                        if ($db->con->num_rows == 1) {
                            $id_pet = $db->con->get_insert_id();
                            $query = "INSERT INTO REL_DONO_PET VALUES ('" . $id_pet . "','" . $id_dono . "','" . date("Y-m-d") . "', '0');";
                            $db->con->query($query);

                            if ($db->con->num_rows != 1) {
                                $return['error'] = true;
                                $return['msg'] = "Problemas ao cadastrar relacionamento DONO/PET!";
                            }
                        } else {
                            $return['error'] = true;
                            $return['msg'] = "Problemas ao cadastrar o PET!";
                        }
                    }
                }
                if ($return['error'] != true) {
                    $return['error'] = false;
                    $return['msg'] = '<div class="frm-reg">';
                    if (($numrows_cli + $numrows_pet + $numrows_pet_insert) == 0) {
                        $return['msg'] .= "Nenhuma alteração no cadastro!";
                    }
                    if ($numrows_cli == 1) {
                        $return['msg'] .= "<p>Informações do cliente atualizadas com sucesso!</p>";
                    }
                    if ($numrows_pet >= 1) {
                        $return['msg'] .= "<p>Dados do PET atualizados com sucesso!</p>";
                    }
                    if ($numrows_pet_insert >= 1) {
                        $return['msg'] .= "<p>Novo(s) PET(s) inseridos no cadastro!</p>";
                    }
                    $return['msg'] .= '</div>';
                }
            } else {
                $return['error'] = true;
                $return['msg'] = "Houver algum erro ao tentar cadastrar o cliente!";

                //$return['invalid'] = true;
                //$return['fields'] = array("cep" => "Cep errado!");
            }
        }
    }
    $return['msg'] = Protection::encodeUTF8($return['msg']);
    echo json_encode($return);
}
?>