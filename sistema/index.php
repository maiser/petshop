<?php
require_once ('../include/Configure.php');
require_once ('../include/Functions.class.php');
require_once ('../class/Template.class.php');
require_once ('../class/Translator.class.php');
require_once ('../class/Session.class.php');
require_once ('../class/FormValidator.class.php');

$tpl = new Template(DIR_TEMPLATES."login.html");
$I18N = new Translator($_CONF['lang'],'../'.DIR_LOCALE);
$ss = new Session(true);

//$frm = new FormValidator();
//echo var_dump($frm->safe);
//header('Location: manutencao.html');

$msg_err = $I18N->getr('Usu&aacute;rio ou senha inv&aacute;lido(s)!');

$tpl->DOMAIN = DOMAIN;
$tpl->PAGE_TITLE = $_CONF['sys_title'];
$tpl->TITLE = $I18N->getr("Ir para o site!");
$tpl->ALT = $tpl->TITLE;


$tpl->CSS_ACTION = 'msg_err hide';
$tpl->MSG_ACTION = $msg_err;
	
if(isset($_GET['expire'])){
	$tpl->CSS_ACTION = 'msg_logout';
	$tpl->MSG_ACTION = $I18N->getr('Desconectado com sucesso!');
	$ss->destroy();
}
if(isset($_GET['err'])){
	$tpl->CSS_ACTION = 'msg_err show';
	$tpl->MSG_ACTION = $msg_err;
}

$tpl->LBL_LOGIN = $I18N->getr('Usu&aacute;rio');
$tpl->LBL_PWD = $I18N->getr('Senha');
$tpl->BTL_LOGIN = $I18N->getr('Acessar');

$tpl->show();

$ss->destroy();
//debconf
?>