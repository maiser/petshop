<?php

require_once ('../include/Configure.php');
require_once ('../class/Template.class.php');
require_once ('../class/Translator.class.php');
require_once ('../class/Session.class.php');
require_once ('../class/Cookie.class.php');

$tpl = new Template(DIR_TEMPLATES."logout.html");
$I18N = new Translator($_CONF['lang'],'../'.DIR_LOCALE);
$SS = new Session(true);
$CK = new Cookie();

$tpl->DOMAIN = DOMAIN;
$tpl->PAGE_TITLE = $_CONF['sys_title'];
$tpl->TITLE = $I18N->getr("Ir para o site!");
$tpl->ALT = $tpl->TITLE;
$tpl->MSG_LOGOUT = $I18N->getr('Desconectado com sucesso!');
$tpl->BTL_LOGIN = $I18N->getr('Acessar novamente');
$tpl->BTL_GOHOME = $I18N->getr('Ir para o site!');
$tpl->show();

$SS->destroy();
$CK->destroyVars(array('locale'), false);

?>