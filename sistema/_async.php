<?php

function __autoload($class_name) {
    $path = "../";
    if (file_exists($path . 'class/' . $class_name . '.class.php')) {
        require_once ($path . 'class/' . $class_name . '.class.php');
    } else {
        echo "Classe nao existe:'. $class_name . '.class.php";
    }
}

$_SS = new Session(true);
$_CONTROL = new Control('../');
$I18N = new Translator($_CONTROL->site_lang, '../' . DIR_LOCALE);

header('Content-Type: text/html; charset='.$_CONTROL->getConfig('charset'));

$rule = $_SS->get_var('user_perm');

if ($rule >= 6000) {
    $_CONTROL->registerUserEvent();
    $page = (string) $_CONTROL->getAsyncPage($_CONTROL->getGETVar('link'));
    if (file_exists($page)) {
        require_once($page);
    } else {
        echo "<div class='msg-box error'>O arquivo ".$page." n&atilde;o existe!</div>";
    }
} else {
    if ($rule == "" || $rule == null) {
        $_CONTROL->registerRequestEvent();
    } else {
        $_CONTROL->registerUserEvent();
    }
}
?>