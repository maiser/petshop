/**
 * @author MJAlves
 */
$( function() {

	$("input:text:visible:first").focus();	
	
	$('#msg_err').hide();
	
	$("#bt_login2").click( function() {
		// validate and process form here
		$('.msg_err').hide();
		var user = $("input#txtuser").val();
		var pass = $("input#txtpass").val();
		if (user == "" || pass == "") {
			$('.msg_err').fadeIn(1000);
			return false;
		}
	
		var dataString = 'f_user='+ user + '&f_pass=' + pass;
		//alert (dataString);return false;
		$.ajax({
			cache: false,
			type: "POST",
			url: "login.php?ajax=true",
			data: dataString,
			beforeSend: function(x) {
	        	if(x && x.overrideMimeType) {
	           		x.overrideMimeType("application/json;charset=UTF-8");
	       		 }
	   		 },
			success: function(info) {
				if(info == null) $('.msg_err').fadeIn(1000);
				if(info.ok==true) document.location='login.php';
			},
			error: function(err){
    			alert('Houve um problema no envio informações!');
  			},
		});
		return false;

	});
});