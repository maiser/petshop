
function setFirst() {
    $("input:text:visible:first").focus();
    $("input:text:visible:first").addClass("focus");

    $("input").focus(function () {
        $(this).addClass("focus");
    });
    $("input").blur(function () {
        $(this).removeClass("focus");
    });
}

function formParse() {
    var requiredTag = '<span class="asterisk">*</span>';
    $.each($("input, textarea, select"), function (index, value) {
        if ($(value).hasClass("required") || $(value).attr("required")) {
            $(requiredTag).insertBefore($(value).prev());
        }
    });
}

function formUnParse() {
    var requiredClass = '.asterisk';
    $(requiredClass).remove();
}

function buscacep(cep) {
    if (cep.length === 8) {

        var options = {
            type: 'GET',
            url: './_async.php?link=ag02&cep=' + cep,
            dataType: 'json',
            success: function (data) {
                //alert(data.sucesso);
                if (data.sucesso === true) {
                    $('#txt_endereco').val(data.dados.logradouro);
                    if ($('#txt_endereco').valid() == true) {
                        $('#txt_endereco').addClass("valid");
                    }
                    $('#txt_bairro').val(data.dados.bairro);
                    if ($('#txt_bairro').valid() == true) {
                        $('#txt_bairro').addClass("valid");
                    }
                    $('#txt_uf').val(data.dados.uf);
                    if ($('#txt_uf').valid() == true) {
                        $('#txt_uf').addClass("valid");
                    }
                    $('#txt_cidade').val(data.dados.cidade);
                    if ($('#txt_cidade').valid() == true) {
                        $('#txt_cidade').addClass("valid");
                    }
                    $('#txt_country').val(data.dados.country);
                    if ($('#txt_country').valid() == true) {
                        $('#txt_country').addClass("valid");
                    }
                    $("#txt_num").focus();
                } else {

                }
            },
            error: ajaxError
        };
        $.ajax(options);
    }
}
function getRacaPet(especie, select) {
    var options = {
        type: 'GET',
        url: './_async.php?link=ag01&tipo=' + especie,
        dataType: 'html',
        success: function (data) {
            $(select).html(data);
        },
        error: ajaxError
    };
    $.ajax(options);
}

function locPage(page) {
    window.location.assign(page);
}

function ajaxSearch(obj, target_content, page_url, callback) {
    var search_data = 'search=';
    search_data = search_data + $(obj).val();

    var options = {
        type: 'GET',
        url: './_async.php' + page_url + '&' + search_data,
        dataType: 'html',
        success: function (data) {
            $(target_content).html(data);

            if (typeof (callback) == 'function') {
                callback();
            }
            return true;
        },
        error: ajaxError
    };

    return $.ajax(options);

}

function ajaxRequest(obj, f_exec, page_url, dialog, msg_dialog) {

    var options = {
        type: 'GET',
        url: './_async.php' + page_url,
        dataType: 'html',
        success: function (data) {
            f_exec(obj, data);
        },
        error: ajaxError
    };

    if (dialog === true) {
        $.msgBox({
            title: "Aten&ccedil;&atilde;o!",
            content: msg_dialog,
            type: "confirm", //'alert','confirm','error','info','prompt'
            buttons: [{value: "Sim"}, {value: "N&atilde;o"}, {value: "Cancelar"}],
            success: function (result) {
                if (result === "Sim") {
                    $.ajax(options);
                }
            }
        });
    } else {
        $.ajax(options);
    }
}

function ajaxFormRequestP(form_id, obj, data_type, f_exec, request_url, dialog, msg_dialog) {
    var options = {
        type: 'POST', //post, get
        url: request_url, //url
        dataType: data_type, //xml, script, json, html
        resetForm: false,
        scriptCharset: 'UTF-8',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            f_exec(obj, data);
        },
        error: function () {
            $(form_id).fadeOut(900);
        }
    };

    if (dialog === true) {
        $.msgBox({
            title: "Aten&ccedil;&atilde;o!",
            content: msg_dialog,
            type: "confirm", //'alert','confirm','error','info','prompt'
            buttons: [{value: "Sim"}, {value: "N&atilde;o"}, {value: "Cancelar"}],
            success: function (result) {
                if (result === "Sim") {
                    $(form_id).ajaxForm(options);
                    $(form_id).submit();
                }
            }
        });
    } else {
        //$(form_id).ajaxForm(options);
    }

}

function myDialog(msg_dialog, type_dlg, title, callback) {
    if (type_dlg == 'confirm') {
        $.msgBox({
            title: title,
            content: msg_dialog,
            type: "confirm", //'alert','confirm','error','info','prompt'
            buttons: [{value: "Sim"}, {value: "N&atilde;o"}, {value: "Cancelar"}],
            success: function (result) {
                if (result === "Sim") {
                    callback();
                }
            }
        });
    }
}

function setAjaxForm(form_id, css_target, request_type, request_url, data_type) {

    $(form_id).ajaxForm({
        type: request_type, //post, get
        url: request_url, //url
        dataType: data_type, //xml, script, json, html
        resetForm: false,
        scriptCharset: 'UTF-8',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        beforeSubmit: function () {
            $(css_target).hide(0);
            $('#waiting').show(0);
            $(form_id).hide(0);
        },
        success: function (data) {
            $(css_target).hide(0);
            $('#waiting').hide(0);
            $(css_target).removeClass().addClass((data.error === true) ? 'error' : 'success');

            if (data.error === false) {
                $(css_target).html(data.msg).fadeIn(900, function () {
                    $(form_id).fadeIn(1200);
                    $(css_target).delay(3500).fadeOut(900, function () {
                        //$(form_id)[0].reset();
                        //$(form_id).fadeIn(300, function () {
                        //window.setTimeout('location.reload()', 100);
                        location.reload();
                        //});


                    });
                });

            } else {
                $(css_target).append("<div class='frm-reg'>" + data.msg + "</div>");
                if (data.invalid === true) {
                    $(css_target).append("<br/>");
                    $.each(data.fields, function (index, value) {
                        $(css_target).append("<div class='frm-reg'><strong>" + index + ":</strong> " + value + "</div>");
                    });
                    $(form_id).show(0);
                    $(css_target).fadeIn(900);
                } else {
                    $(css_target).fadeIn(900);
                    setTimeout(function () {
                        $(form_id).fadeIn(900);
                    }, 3000);
                }
            }



        },
        error: ajaxError
    });
}

function submitForm(form) {
    $(form).submit();
}

function _errorFormValidateMsg(selector, css_error_target, msg, delay) {
    $(selector).removeClass().addClass(css_error_target).text(msg).fadeIn(900);
    $(selector).delay(delay).fadeOut(900);
}

function errorFormValidateMsg() {
    $('#status_msg').removeClass().addClass('error').text('Existem campos incompletos ou preenchidos incorretamente!').fadeIn(900);
    $('#status_msg').delay(3500).fadeOut(900);
}

function ajaxError(XMLHttpRequest, textStatus, errorThrown) {
    //alert(XMLHttpRequest.responseText + " " + textStatus + " " + errorThrown);
    $('#waiting').hide(0);
    $('#status_msg').removeClass().addClass('error').html('Erro ao receber resposta do servidor:<br/><br/>' + XMLHttpRequest.responseText + " " + textStatus + "<br/>" + errorThrown).fadeToggle(600);
    return false;
}

//declara��o alternativa: $.GET = function(){};
(function ($) {
    $.fn._GET = function (key) {
        var results = new RegExp('[\?&]' + key + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || '';
        }
    }
})(jQuery);

(function ($) {
    $.fn.validacnpj = function () {
        this.change(function () {
            CNPJ = $(this).val();
            if (!CNPJ) {
                return false;
            }
            erro = new String;


            if (CNPJ == "00.000.000/0000-00") {
                erro += "CNPJ incorreto\n\n";
            }

            CNPJ = CNPJ.replace(".", "");
            CNPJ = CNPJ.replace(".", "");
            CNPJ = CNPJ.replace("-", "");
            CNPJ = CNPJ.replace("/", "");

            var a = [];
            var b = new Number;
            var c = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
            for (i = 0; i < 12; i++) {
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i + 1];
            }
            if ((x = b % 11) < 2) {
                a[12] = 0
            } else {
                a[12] = 11 - x
            }
            b = 0;
            for (y = 0; y < 13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) {
                a[13] = 0;
            } else {
                a[13] = 11 - x;
            }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])) {
                erro += "CNPJ incorreto!";
                $(this).focus();
            }
            if (erro.length > 0) {
                $(this).val('');
                alert(erro);
                setTimeout(function () {
                    $(this).focus()
                }, 50);
            }
            return $(this);
        });

    }
})(jQuery);

(function ($) {
//plugin retirado de http://www.fernandowobeto.com/blog/2011/04/plugin-jquery-validacao-cpf/
    jQuery.fn.validacpf = function () {
        this.change(function () {
            CPF = $(this).val();
            if (!CPF) {
                return false;
            }
            erro = new String;
            cpfv = CPF;
            if (cpfv.length == 14 || cpfv.length == 11) {
                cpfv = cpfv.replace('.', '');
                cpfv = cpfv.replace('.', '');
                cpfv = cpfv.replace('-', '');

                var nonNumbers = /\D/;

                if (nonNumbers.test(cpfv)) {
                    erro = "A verificacao de CPF suporta apenas n�meros!";
                } else {
                    if (cpfv == "00000000000" ||
                            cpfv == "11111111111" ||
                            cpfv == "22222222222" ||
                            cpfv == "33333333333" ||
                            cpfv == "44444444444" ||
                            cpfv == "55555555555" ||
                            cpfv == "66666666666" ||
                            cpfv == "77777777777" ||
                            cpfv == "88888888888" ||
                            cpfv == "99999999999") {

                        erro = "CPF incorreto!"
                        $('#txt_cpf').focus();
                    }
                    var a = [];
                    var b = new Number;
                    var c = 11;

                    for (i = 0; i < 11; i++) {
                        a[i] = cpfv.charAt(i);
                        if (i < 9)
                            b += (a[i] * --c);
                    }
                    if ((x = b % 11) < 2) {
                        a[9] = 0
                    } else {
                        a[9] = 11 - x
                    }
                    b = 0;
                    c = 11;
                    for (y = 0; y < 10; y++)
                        b += (a[y] * c--);

                    if ((x = b % 11) < 2) {
                        a[10] = 0;
                    } else {
                        a[10] = 11 - x;
                    }
                    if ((cpfv.charAt(9) != a[9]) || (cpfv.charAt(10) != a[10])) {
                        erro = "CPF incorreto!"
                        $('#txt_cpf').focus();
                    }
                }
            } else {
                if (cpfv.length == 0) {
                    return false;
                } else {
                    erro = "CPF incorreto!";
                    $('#txt_cnpj').focus();
                }
            }
            if (erro.length > 0) {
                $(this).val('');
                alert(erro);
                setTimeout(function () {
                    $(this).focus();
                }, 100);
                return false;
            }
            return $(this);
        });
    };
})(jQuery);

function setDateTimeUI() {
    $('.data_agend').on('focusin', function () {
        $(this).not('hasDatepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo', 'Segunda', 'Ter�a', 'Quarta', 'Quinta', 'Sexta', 'S�bado', 'Domingo'],
            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'S�b', 'Dom'],
            monthNames: ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            nextText: 'Pr�ximo',
            prevText: 'Anterior',
            onClose: function () {
                $(this).focus();
            }
        });
    });
    $('.time').on('focusin', function () {
        $(this).not('hasTimePicker').timepicker({
            myPosition: 'rigth top', // Corner of the dialog to position, used with the jQuery UI Position utility if present.
            atPosition: 'rigth bottom',
            hourText: 'Hora',
            minuteText: 'Minuto',
            amPmText: ['AM', 'PM'],
            closeButtonText: 'Fechar',
            nowButtonText: 'Agora',
            deselectButtonText: 'Limpar',
            showCloseButton: true, // shows an OK button to confirm the edit
            showNowButton: true, // Shows the 'now' button
            showDeselectButton: true, // Shows the deselect time button
            onSelect: function () {
                $(this).focus();
            }
        });
    });
}

$(document).ready(function ($) {
    function dumpFormVars(target, selector, atrib) {
        var varray = $(selector).toArray();
        $.each(varray, function (index, element) {
            console.log("index:(" + index + ") - value:(" + $(element).attr(atrib) + ")");
            if ($(element).attr(atrib) !== undefined) {
                $(target).append('$tpl->' + $(element).attr(atrib) + ' = "";<br/>');
            }
        });
    }

    //debugForm("#status_msg", "input, select, textarea", "value");
});