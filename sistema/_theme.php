<!DOCTYPE html>
<html lang="<?php echo $_CONTROL->site_lang; ?>">
	<head>
		<?php
			require_once ($_CONTROL->site_meta);//META 
		?> 
	</head>

	<body>
		<div class="container">
			
			<header id="header">
				<?php
					require_once ('header.php');//{HEADER}
					require_once ('menu.php');//{NAVIGATION}
				?> 
			</header>
			
			<?php 
				require_once ($_CONTROL->site_page);//{BODY}
			?> 

			<footer id="footer">
				<?php
					require_once ('footer.php');//{FOOTER}
				?> 
			</footer>
		</div>
		
	</body>
</html>
