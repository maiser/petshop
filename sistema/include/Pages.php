<?php

$_PAGE["404"] = '404.php'; //pagina nao existe
$_PAGE["403"] = '403.php'; //JavaScript desabilitado
$_PAGE['modules'] = 'modules.php';
$_PAGE['about'] = 'sobre.php';

/** ASYNC **/
$_ASYNC['ar01'] = DIR_ASYNC.'register_pagamento.php';
//ad = async del
$_ASYNC['ad01'] = DIR_ASYNC.'delreg_list_pets.php';
$_ASYNC['ad02'] = DIR_ASYNC.'delreg_pet.php';
$_ASYNC['ad03'] = DIR_ASYNC.'delreg_agend.php';
//ag = async get
$_ASYNC['ag01'] = DIR_ASYNC.'get_raca_pet.php';
$_ASYNC['ag02'] = DIR_ASYNC.'get_cep.php';
//af = async find
$_ASYNC['af01'] = DIR_ASYNC.'find_pet.php';
$_ASYNC['af02'] = DIR_ASYNC.'find_pg_pendente.php';
$_ASYNC['af03'] = DIR_ASYNC.'find_fatura.php';
//agc = async get content
$_ASYNC['agc01'] = DIR_ASYNC.'get_content_agpet.php';
$_ASYNC['agc02'] = DIR_ASYNC.'get_content_agpet.php';

/* PAGINAS RELATIVAS AOS MODULOS */
$_PAGE['cad_cli'] = DIR_PAGES . 'cad_cliente_pet.php';
$_PAGE['agendar'] = DIR_PAGES . 'agendar_servico.php';
$_PAGE['check_agenda'] = DIR_PAGES . 'consultar_agenda.php';
$_PAGE['pend_pg'] = DIR_PAGES . 'pg_pendente.php';
$_PAGE['faturas'] = DIR_PAGES . 'faturas.php';
/* * ** LISTAS *** */
$_PAGE['list_pets'] = DIR_PAGES . 'list_pets.php';
$_PAGE['gen_recibo'] = DIR_PAGES . 'generate_recibo.php';
$_PAGE['list_users'] = DIR_PAGES . 'list_users.php';
$_PAGE['relat_fatfilial'] = DIR_PAGES . 'relat_fatfilial.php';

/* * *** VIEWS ******* */

/* * *** EDICAO ****** */
$_PAGE['sys_cad_user'] = DIR_PAGES . 'sys_cad_user.php';

?>
