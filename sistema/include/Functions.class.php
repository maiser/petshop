<?php

// Manipulacao de arquivos basica
// Class to manipulate Files and Directories
class File {
	var $length;
	function scanDir($dirPath){
		$dh  = opendir($dirPath);
		while (false !== ($filename = readdir($dh))) {
			$dir[] = $filename;
		}
		sort($dir);
		// We don't want . and .. 'files'
		array_shift($dir);
		array_shift($dir);
		// Get the directory's length
		$this->length = count($dir);
	//	var_dump(print_r($dir));

		return $dir;
	}
	function uploadFile($origemTemp,$arquivo,$caminho){

		// Checa se existe o caminho, sen�o cria a pasta-caminho
		if ( !is_dir($caminho) ){
			mkdir('./'.$caminho);
			chmod ('./'.$caminho, 0777);
		}
        /*if(move_uploaded_file($origemTemp, $caminho.$arquivo)){
        chmod ('./'.$caminho.$arquivo, 0664);
        $msg = array(true, "<br>Arquivo anexado com sucesso!");
        }else{
        //$msg = $this->errorUpload();
        $msg = array(false, $this->errorUpload());
        }

        return ($msg);*/
		if (move_uploaded_file($origemTemp, $caminho.$arquivo)) {
		   chmod ('./'.$caminho.$arquivo, 0664);
		   $msg = array(true, "<br>Arquivo anexado com sucesso!");
		} else {
           //delete($caminho.$arquivo);
		   $msg = array(false, '<font color="red">Houve algum erro ao tentar anexar o arquivo!<br>'.$t.'</font>');
		}
		return ($msg);
		
		
	}
	function printFiles($dir){
		$allFiles = $this->scanDir($dir);
		echo "Arquivos anexos a not�cia:<br>";
		for ($i=0;$i < $this->length ;$i++){
			$link = $dir.$allFiles[$i];
			echo "<a target=\"_blank\" href=\"".$link."\">".$allFiles[$i]."</a><br>";
		}

	}
	function printFilesAdmin($dir){
		if (is_dir($dir)){
			$allFiles = $this->scanDir($dir);
			echo "<hr>";
			for ($i=0;$i < $this->length ;$i++){
				$link = $dir.$allFiles[$i];
				return "<a target=\"_blank\" href=\"".$link."\">".$allFiles[$i]."</a>
				 || <a target=\"_blank\" href=\"inscricoes.php?page=upload&a=remover&file=".$link."\">remover</a><br>";
			}

		}
	}
	function verifExtensao($arquivo){
	    $ext = array('pdf','docx','jpg','pdf','tiff','jpeg','doc','tif','gif','png','bmp');
	    for($i=0; $i < count($ext); $i++){
                  if(eregi('.'.$ext[$i],$arquivo)){
                    $extensao = array(true,'.'.$ext[$i]);
                    break;
                  }else$extensao = array(false);}
	    return $extensao;
	}
    function verifExtensaoImg($arquivo){
	    $ext = array('jpg','jpeg','gif','png');
	    for($i=0; $i < count($ext); $i++){
            if(eregi('.'.$ext[$i],$arquivo)){ return true; }
            else {return false;}
        }
    }

	function mostraArquivo($dir, $arquivo){
		if (is_dir($dir)){
			for ($i=0;$i < $this->length ;$i++){
				$link = $dir.$allFiles[$i];
				return "<a target=\"_blank\" href=\"".$link."\">".$allFiles[$i]."</a>
				 || <a target=\"_blank\" href=\"?a=remover&file=".$link."\">remover</a><br>";
			}

		}
	}
	
 function errorUpload($error_code) {
    switch ($error_code) {
        case UPLOAD_ERR_INI_SIZE:
            return 'O arquivo enviado � maior que o limite permitido!';
        case UPLOAD_ERR_FORM_SIZE:
            return 'O arquivo enviado � maior que o limite permitido no formul&aacute;rio!';
        case UPLOAD_ERR_PARTIAL:
            return 'O upload do arquivo foi feito parcialmente!';
        case UPLOAD_ERR_NO_FILE:
            return 'N&atilde;o foi poss&iacute;vel enviar o arquivo!';
        case UPLOAD_ERR_NO_TMP_DIR:
            return 'A pasta tempor&aacute;ria n&atilde;o existe!';
        case UPLOAD_ERR_CANT_WRITE:
            return 'Erro ao tentar escrever no disco!';
        case UPLOAD_ERR_EXTENSION:
            return 'Upload n&atilde;o permitido para esta extens&atilde;o!';
        default:
            return 'Erro de upload n&atilde;o identificado!';
    }
}

}

// Classe para manipulacao de fotos e imagens
class Photo extends File {
	var $path;
	var $originalPath, $smallPath, $normalPath;
	function Photo($pathname,$id){
		$this->path = $pathname;
        $this->originalPath = $pathname."original/original-".$id.".jpg";
		$this->viewPath = $pathname."view-".$id.".jpg";
        $this->normalPath =  $pathname."normal-".$id.".jpg";
		$this->smallPath =  $pathname."small-".$id.".jpg";
        $this->indexPath =  $pathname."index-".$id.".jpg";
        $this->videoPath =  $pathname.$id.".jpg";
	}
    function smallPhoto($pathname, $id){
        $this->path = $pathname;
		$this->originalPath = $pathname."original-".$id.".jpg";
		$this->smallPath =  $pathname."small-".$id.".jpg";
		$this->normalPath =  $pathname."normal-".$id.".jpg";
        if (is_file($this->normalPath))
        	return "<a href=\"".$this->originalPath."\"><img id=\"noticiaPhoto\" border=\"0\" src=\"".$this->normalPath."\"></a>";
    }
    
	function printNormalPhoto(){
		if (is_file($this->normalPath))
        	echo "<a href=\"".$this->originalPath."\"><img id=\"noticiaPhoto\" border=\"0\" src=\"".$this->normalPath."\"></a>";
	}
	function printSmallPhoto(){
		if (is_file($this->smallPath))
		    echo "<a href=\"".$this->originalPath."\"><img id=\"noticiaPhoto\" border=\"0\" src=\"".$this->smallPath."\"></a>";
	}	
	function uploadPhoto($origemTemp){
		$destino = dirname($this->originalPath)."/";
		$basename = basename($this->originalPath);

		$up = $this->uploadFile($origemTemp,$basename,$destino);
        //verificando se o upload ocorreu normal
        if($up[0] == true){
                $this->convertJPEG();
                $this->createSmall();
                $this->createNormal();
                $this->createView();
                $this->createIndex();
                return('true');
        }else{
            return $up[1];
        }
	}

    function uploadPhotoVideo($origemTemp){
		$destino = dirname($this->originalPath)."/";
		$basename = basename($this->originalPath);

		$up = $this->uploadFile($origemTemp,$basename,$destino);
        //verificando se o upload ocorreu normal
        if($up[0] == true){
                $this->convertJPEG();
                $this->resize($this->videoPath,110,80,100);
                return('true');
        }else{
            return $up[1];
        }
	}
	// Type can be small or medium
	function resize($newPath,$maxWidth,$maxHeight, $quality = 90){
		// Get original dimensions
		list($width_orig, $height_orig) = getimagesize($this->originalPath);

        //imagem original;
        if($maxHeight == 0 || $maxWidth == 0){
            $maxHeight = $height_orig;
            $maxWidth = $width_orig;
            $quality = 100;
        }else{
            //contas para redimensionamento em proporcao
            if (($width_orig > $height_orig)) {
                //$maxWidth = ($maxHeight / $height_orig) * $width_orig;
                $maxHeight = ($maxWidth / $width_orig) * $height_orig;
            } else {
               //$maxHeight = ($maxWidth / $width_orig) * $height_orig;
                $maxWidth = ($maxHeight / $height_orig) * $width_orig;
            }
            
        }
        //echo '<br>'.round($maxWidth).' x '.round($maxHeight);
		//Resample
		$image_p = imagecreatetruecolor($maxWidth, $maxHeight);
		$image = imagecreatefromjpeg($this->originalPath);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $maxWidth, $maxHeight, $width_orig, $height_orig);
		
		// Output
		imagejpeg($image_p,$newPath , $quality);
		return $newPath;
	}
	function createSmall(){
		$this->resize($this->smallPath,100,100,100);
	}
	function createNormal(){
		$this->resize($this->normalPath,300,300,100);
	}
	function createIndex(){//cria foto para o bloco principal (Index)
		$this->resize($this->indexPath,216,175,100);
	}
    function createView(){
		$this->resize($this->viewPath, 700, 525, 100);
	}
	
	function openImage(){
		$src="";
		$size = getimagesize($this->originalPath);
		$file_type = $size['mime'];

		// get file types and create a properly image
	 	if($file_type == "image/jpeg"){$src = imagecreatefromjpeg($this->originalPath);}
		else if($file_type == "image/gif"){$src = cmagecreatefromgif($this->originalPath);}
		else if($file_type == "image/png"){$src = imagecreatefrompng($this->originalPath);}

		return $src;
	}
	function resample(){//funcao apenas pra teste de resample
                $this->convertJPEG();
                $this->createView();
                $this->createNormal();
                $this->createSmall();
                $this->createIndex();
    }
    function delImage(){
      unlink($this->originalPath);
   	  unlink($this->viewPath);
      unlink($this->normalPath);
	  unlink($this->smallPath);
      unlink($this->indexPath);
    }
	function convertJPEG(){
		$image = $this->openImage();
            imagejpeg($image,$this->originalPath , 100);
	}

}

// Classe para conexao com base dados Mysql
class MysqlServers{
        function sql_connect($sqlserver, $sqluser, $sqlpassword, $database){
            $this->connect_id = mysql_connect($sqlserver, $sqluser, $sqlpassword);
            if($this->connect_id){
                if (mysql_select_db($database)){
                    return $this->connect_id;
                }else{
                    return $this->error();
                }
            }else{
                return $this->error();
            }
        }
        function error(){
            if(mysql_error() != ''){
                echo '<b>MySQL Error</b>: '.mysql_error().'<br/><br/><br/><br>';
                if(SHOW_MYSQL_ERROR == false){
               // echo "\n".'<center><br/><br/><b><font color="red">Ops! Ocorreu algum erro!<br> Entre em contato atrav&eacute;s do site ou tente novamente!</fonte></center></b><br><br><input type="hidden" value="MySQL Error No: ('.mysql_errno().') #-#'.mysql_error().'"></input>';
                }
               return(mysql_errno());

            }
        }
        function query($query){
            if ($query != NULL){
                $this->query_result = mysql_query($query, $this->connect_id);
                if(!$this->query_result){
                    return $this->error();
                }else{
                    return $this->query_result;
                }
            }else{
                return '<b>MySQL Error</b>: Empty Query!';
            }
        }
        function get_num_rows($query_id = ""){
            if($query_id == NULL){
                $return = mysql_num_rows($this->query_result);
            }else{
                $return = mysql_num_rows($query_id);
            }
            if(!$return){
                $this->error();
            }else{
                return $return;
            }
        }
        function fetch_row($query_id = ""){
            if($query_id == NULL){
                $return = mysql_fetch_array($this->query_result);
            }else{
                $return = mysql_fetch_array($query_id);
            }
            if(!$return){
                $this->error();
            }else{
                return $return;
            }
        }   
        function get_affected_rows($query_id = ""){
            if($query_id == NULL){
                $return = mysql_affected_rows($this->query_result);
            }else{
                $return = mysql_affected_rows($query_id);
            }
            if(!$return){
                $this->error();
            }else{
                return $return;
            }
        }
        function result($query_id = "", $numlinha = 0, $nomecol=0){
            if($query_id == NULL){
                $return = mysql_result($this->query_result,$numlinha,$nomecol);
            }else{
                $return = mysql_result($query_id,$numlinha,$nomecol);
            }
            if(!$return){
                $this->error();
            }else{
                return $return;
            }
        }
        function sql_close(){
            if($this->connect_id){
                return mysql_close($this->connect_id);
            }
        }
  
	function select_db($database){
	      if (mysql_select_db($database)){
                    return $this->connect_id;
                }else{
                    return $this->error();
                }
	}

    /* funcao dataToData
     * $dataMysql = ANO-MES-DIA
     * $modo 0: dia/mes/ano
     *       1: dia
     *       2: mes
     *       3: ano
     */
    function dateToData($dataMysql, $modo = 0){
        $data = explode('-',$dataMysql);
        $fulldata = $data[2].'/'.$data[1].'/'.$data['0'];

        switch($modo){
            case 0: return($fulldata); break;
            case 1: return($data[2]); break;
            case 2: return($data[1]); break;
            case 3: return($data[0]); break;
            default: return $fulldata;
        }
    }

   /* Paginador de noticias
     * @autor: Maiser J. Alves
     * @var $numrows = numero de registros a ser paginado
     * @var $maxrows maximo de linhas por pagina
     * @var $linkpage = link para a pagina do paginador
     * @var $numpage = numero da pagina selecionada
     * @var $urlget = nome do parametro a ser enviado por GET em $linkpage;
     * @var $divnumpage = divide o numero total de paginas em blocos de tamanho $divnumpage
     * @var $showall = mostrar a opcao de ver todos os registros
     * @return[0] $list_pages o paginador em HTML com os links para as paginas
     * @return[1] $startline o numero onde comeca prmieiro registro
     * @return[2] $maxrows o numero total de registros
     */
    function pageRows($numrows, $maxrows, $linkpage, $numpage, $urlget = 'page',$showall = true, $divnumpage=20 ){

        if(!isset($numpage)){$numpage = 1;}
        $pageindex = 0;
        $startline = 0;
        $list_pages = "";
        $all = '[todas]';
        settype($numpage, "int"); //transformando $numpage em inteiro

        for($i=1; $i < $numrows; $i++){
            if(($i % $maxrows) == 0){
                $pageindex++;
                if($pageindex != $numpage){ //gera hyperlink para as paginas exceto para a de numero $numpage
                 $list_pages .= '&nbsp;<a href="'.$linkpage.'&amp;'.$urlget.'='.$pageindex.'">['.$pageindex.']</a>&nbsp;';
                }else{ //marcador [$numpage] sem hyperlink
                  $list_pages .= '&nbsp;<b>['.$pageindex.']</b>&nbsp;';
                }
                if($pageindex % $divnumpage == 0) $list_pages .= '<br/>';
            }
        }

        if(ceil($numrows / $maxrows) > 0){
            $pageindex++;

            if($pageindex != $numpage){ //gera hyperlink para as paginas exceto para a de numero $numpage
                $list_pages .= '&nbsp;<a href="'.$linkpage.'&amp;'.$urlget.'='.$pageindex.'">['.$pageindex.']</a>&nbsp;';
            }else{ //marcador [$numpage] sem hyperlink
                  $list_pages .= '&nbsp;<b>['.$pageindex.']</b>&nbsp;';
            }
            if($pageindex % $divnumpage == 0) $list_pages .= '<br/>';
        }

        if($numpage != 0){
            if((is_numeric($numpage) == true) && ($pageindex > 0)){
                //se numero da pagina for menor que a quantidade real de paginas
                if($numpage <= $pageindex){
                    $startline = ($numpage * $maxrows) - $maxrows;
                    if($showall == true)
                    $list_pages .= ' <a href="'.$linkpage.'&amp;'.$urlget.'='.'all">'.$all.'</a> ';
                }else{
                    $maxrows = $numrows;
                    $list_pages .= '&nbsp;<b>'.$all.'</b>&nbsp;';
                }
            }
        }else{
            if($showall == true){
                $maxrows = $numrows;
                $list_pages .= '&nbsp;<b>'.$all.'</b>&nbsp;';
            }
        }
        return array($list_pages, $startline, $maxrows);
    }//FIM paginador de noticias

    
}
/*
 * @var $dirFilePHP Caminho absotulo do arquivo php
 * @var $dirTPL Caminho relativo do diretorio onde se encontra o template
 * @var $tplFileName Nome do arquivo de templa (Opcional)
 */
function RelativePathTpl($dirFilePHP, $dirTPL, $tplFileName = ""){

    $arquivo = basename($dirFilePHP);
    $path = substr($dirFilePHP, $arquivo, -strlen($arquivo));

    if($tplFileName == ""){
       return $tplPath = $path.$dirTPL.str_replace('.php', '.tpl', $arquivo);
    }else{
       return $tplPath = $path.$dirTPL.$tplFileName;
    }
    //echo substr(__FILE__, $arquivo, strlen(__FILE__)- basename(__FILE__));
}

function expireHeader(){
        // Data no passado
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        // Sempre modificado
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        // HTTP/1.1
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        // HTTP/1.0
        header("Pragma: no-cache");
}

function trim_text($string, $limite) {
	$string = preg_split("/\s+/",$string,($limite+1));
	unset($string[(sizeof($string)-1)]);
    return implode(' ',$string);
}

function evalCPF($cpf)
{
    if(preg_match("/^(\d{3}\.){2}\d{3}-\d{2}$/",$cpf) || preg_match("/\d{11}$/",$cpf))
    {
        $cpf = preg_replace("/[.-]/","",$cpf);
        if(substr_count($cpf,substr($cpf,0,1)) >= 11)
        {
            return false;
        }
        else
        {
            $cpf_temp = substr($cpf,0,9);
            $soma1 = 0;
            $soma2 = 0;
            for($i = 1; $i<= 9; $i++)
            {
                $soma1 += intval(substr($cpf,$i-1,1)) * $i;
            }
            $dv1 = $soma1 % 11;
            if($dv1 == 10) { $dv1 = 0; }
            $cpf_temp = $cpf_temp.$dv1;
            for($i = 0; $i<=9;$i++)
            {
                $soma2 += intval(substr($cpf_temp,$i,1)) * $i;
            }
            $dv2 = $soma2 % 11;
            if($dv2 == 10) { $dv2 = 0; }
            $cpf_final = $cpf_temp.$dv2;
            if(strcmp($cpf,$cpf_final))
            {
                return false;
            }
            else
            {
                return true;
            }
        }      
    }
    else
    {
        return false;
    }
}
// Verifica se  o CNPJ é verdadeiro
function evalCNPJ($cnpj) {
  if (strlen($cnpj) <> 14) return 0;
   $soma1 = ($cnpj[0] * 5) +
    ($cnpj[1] * 4) +
    ($cnpj[2] * 3) +
    ($cnpj[3] * 2) +
    ($cnpj[4] * 9) +
    ($cnpj[5] * 8) +
    ($cnpj[6] * 7) +
    ($cnpj[7] * 6) +
    ($cnpj[8] * 5) +
    ($cnpj[9] * 4) +
    ($cnpj[10] * 3) +
    ($cnpj[11] * 2);
   $resto = $soma1 % 11;
   $digito1 = $resto < 2 ? 0 : 11 - $resto;
   $soma2 = ($cnpj[0] * 6) +
    ($cnpj[1] * 5) +
    ($cnpj[2] * 4) +
    ($cnpj[3] * 3) +
    ($cnpj[4] * 2) +
    ($cnpj[5] * 9) +
    ($cnpj[6] * 8) +
    ($cnpj[7] * 7) +
    ($cnpj[8] * 6) +
    ($cnpj[9] * 5) +
    ($cnpj[10] * 4) +
    ($cnpj[11] * 3) +
    ($cnpj[12] * 2);
   $resto = $soma2 % 11;
   $digito2 = $resto < 2 ? 0 : 11 - $resto;
  return (($cnpj[12] == $digito1) && ($cnpj[13] == $digito2));
}

if (!verificaCNPJ($cnpj)){
  $erro = 1;
  echo "<script>alert('ERRO. CNPJ não é valido.'); history.back();</script>"; die;
}
	
function getPages($getUrl) {
	
	include_once('getpages.php');
	$numpages = count($pages);
		
		for($i=0; $i < $numpages; $i++){
		
			if($getUrl == $pages[$i]) {
				$bool = true; 
				break;
			}else {
				$bool = false;
			}
		}
			
	return $bool;
}
function getPageCSS($linkpage){
    //if($linkpage == 'album_list') $pathCSS = str_replace('\\','/',DIR_PAGES.'css/'.$linkpage.'.css');
    //if($linkpage == 'album_view') $pathCSS = str_replace('\\','/',DIR_PAGES.'css/'.$linkpage.'.css');
    $pathCSS = str_replace('\\','/',DIR_PAGES.'css/'.$linkpage.'.css');
    if(file_exists($pathCSS)){
    echo '<link rel="stylesheet" href="'.$pathCSS.'" type="text/css" />'."\r\n";
    }
}
function DataLocal(){
    
    switch(date('n')){
        case 1: $mes = 'janeiro'; break;
        case 2: $mes = 'fevereiro'; break;
        case 3: $mes = 'mar�o'; break;
        case 4: $mes = 'abril'; break;
        case 5: $mes = 'maio'; break;
        case 6: $mes = 'junho'; break;
        case 7: $mes = 'julho'; break;
        case 8: $mes = 'agosto'; break;
        case 9: $mes = 'setembro'; break;
        case 10: $mes = 'outubro'; break;
        case 11: $mes = 'novembro'; break;
        case 12: $mes = 'dezembro'; break;
    }
    
    $data = "S�o Carlos, SP, ". date('d')." de $mes de ". date('Y');
    return($data);
}

function digaEduc() {
		$hora = (int)date('H');
		
		if($hora >= 0 || $hora >= 18) {
			$diga = 'Boa noite';
		}
		if($hora >= 6) {
			$diga = 'Bom dia';
		}
		if($hora >= 12) {
			$diga = 'Boa tarde';
		}

		return $diga;
}

function sendMail($email, $from_nome, $from_email, $assunto, $mensagem){
	
					/*$nome = 'IV OMQF';  // 
					$from = 'no-reply@omqf.ifsc.usp.br'; //no-reply@omqf.ifsc.usp.br
					$assunto = "Confirma??o IV OMQF";
					$mensagem = $Mensagem;*/
					
					//$headers = "Content-type: text/html; charset=iso-8859-1\r\n"; 
					$headers = "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=utf-8\r\n"; 
					$headers .= "From: $from_nome<".$from_email.">\r\n";
					$headers .= "Replay-To: $from_email\r\n"; 
					$headers .= "Return-Path: $from_email\r\n"; 
					$headers .= "X-Mailer: PHP v".phpversion()."\r\n"; 
			 	//	$headers .= "Bcc: <>";
			 	
			ini_set(sendmail_from, $from_email);	
	        if (mail($email, $assunto, $mensagem, $headers)) {
	        	
	        	//	$hand = fopen('logmail.txt','ra');
				//	fwrite($hand,'Sucesso: Email enviado para '.$email."\r\n");
				//	fclose($hand);
					
		        	return $bool=true;
		        	
	        	}else{
				//	$hand = fopen('logmail.txt','ra');
				//	fwrite($hand,'Erro: Email nao enviado para '.$email."\r\n");
				//	fclose($hand);
					
					return $bool=false;
				}
			ini_restore(sendmail_from);
}

//function sendMail($to, $body, $subject, $fromaddress, $fromname, $attachments=false)
/*function sendMail($to, $fromname, $fromaddress, $subject, $body)
{
  $eol="\r\n";
  $mime_boundary=md5(time());

  # Common Headers
  $headers .= "From: ".$fromname."<".$fromaddress.">".$eol;
  $headers .= "Reply-To: ".$fromname."<".$fromaddress.">".$eol;
  $headers .= "Return-Path: ".$fromname."<".$fromaddress.">".$eol;    // these two to set reply address
  $headers .= "Message-ID: <".time()."-".$fromaddress.">".$eol;
  $headers .= "X-Mailer: PHP v".phpversion().$eol;          // These two to help avoid spam-filters

  # Boundry for marking the split & Multitype Headers
  $headers .= 'MIME-Version: 1.0'.$eol.$eol;
  $headers .= "Content-Type: multipart/mixed; boundary=\"".$mime_boundary."\"".$eol.$eol;

  # Open the first part of the mail
  $msg = "--".$mime_boundary.$eol;
 
  $htmlalt_mime_boundary = $mime_boundary."_htmlalt"; //we must define a different MIME boundary for this section
  # Setup for text OR html -
  $msg .= "Content-Type: multipart/alternative; boundary=\"".$htmlalt_mime_boundary."\"".$eol.$eol;

  # Text Version
  $msg .= "--".$htmlalt_mime_boundary.$eol;
  $msg .= "Content-Type: text/plain; charset=iso-8859-1".$eol;
  $msg .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
  $msg .= strip_tags(str_replace("<br>", "\n", substr($body, (strpos($body, "<body>")+6)))).$eol.$eol;

  # HTML Version
  $msg .= "--".$htmlalt_mime_boundary.$eol;
  $msg .= "Content-Type: text/html; charset=iso-8859-1".$eol;
  $msg .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
  $msg .= $body.$eol.$eol;

  //close the html/plain text alternate portion
  $msg .= "--".$htmlalt_mime_boundary."--".$eol.$eol;

  if ($attachments !== false)
  {
    for($i=0; $i < count($attachments); $i++)
    {
      if (is_file($attachments[$i]["file"]))
      {  
        # File for Attachment
        $file_name = substr($attachments[$i]["file"], (strrpos($attachments[$i]["file"], "/")+1));
       
        $handle=fopen($attachments[$i]["file"], 'rb');
        $f_contents=fread($handle, filesize($attachments[$i]["file"]));
        $f_contents=chunk_split(base64_encode($f_contents));    //Encode The Data For Transition using base64_encode();
        $f_type=filetype($attachments[$i]["file"]);
        fclose($handle);
       
        # Attachment
        $msg .= "--".$mime_boundary.$eol;
        $msg .= "Content-Type: ".$attachments[$i]["content_type"]."; name=\"".$file_name."\"".$eol;  // sometimes i have to send MS Word, use 'msword' instead of 'pdf'
        $msg .= "Content-Transfer-Encoding: base64".$eol;
        $msg .= "Content-Description: ".$file_name.$eol;
        $msg .= "Content-Disposition: attachment; filename=\"".$file_name."\"".$eol.$eol; // !! This line needs TWO end of lines !! IMPORTANT !!
        $msg .= $f_contents.$eol.$eol;
      }
    }
  }


  # Finished
  $msg .= "--".$mime_boundary."--".$eol.$eol;  // finish with two eol's for better security. see Injection.
 
  # SEND THE EMAIL
  ini_set(sendmail_from,$fromaddress);  // the INI lines are to force the From Address to be used !
  $mail_sent = mail($to, $subject, $msg, $headers);
 
  ini_restore(sendmail_from);
 
  return $mail_sent;
}*/
	
?>
