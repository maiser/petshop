<?php

if (!isset($GLOBALS['I18N']))
    exit(0);
else
    global $I18N;
global $_SS;

$userule = $_SS->get_var('user_perm');
$mod = new Template('templates/modules.html');
$rule = new Rules($userule, $mod);
//$auth = new Auth();
$mod->PAGE_TITLE = $I18N->getr('M&oacute;dulos');

//var_dump($_SESSION);
/*
 * Sistema de permissões para classe Rules
 * 0000 - Obrigatório para este sistema de permissão implementado
 * 		  Sintaxe: 0000 => (permissão_bloco, nome_do_bloco)
 * 
 * abcd: numeros de zero a nove
 * 
 * a - tipo de usuario 
 * 		9 - locked
 * 		8 - usuario root
 * 	 	7 - usuario adminsitrador
 * 		6 - usuario filial
 * 		5 - usuario com prioridades
 * 		4 - usuario comum
 * 		3 - somente leitura
 * 		2 - NA
 * 		0 - obrigatorio
 * 
 * b - permissões para o usuario
 * 		7 - leitura, escrita e gravação
 * 		...
 * 		0 - nenhuma permissão
 * 
 * 7777 - Administrador do sistema
 * 6777 - Filial
 * 5555 - Locador
 * 4444 - Contabilidade
 * 
 * /

  /************************************************************
 * MODULO ADMIN
 * ******************************************************** */
$rule->getLinksBlock(
        array(
            0000 => array(7000, 'BLK_ADM'),
            7000 => array(
                0 => '<li><a href="?link=sys_cad_user">Cadastrar usu&aacute;rio</a></li>',
                1 => '<li><a href="?link=list_users">Listar usu&aacute;rios do sistema</a></li>',
            )
        )
);
$mod->TITLE0 = $I18N->getr("ROOT");


/* * ********************************************************
 * MODULO CADASTROS
 * ******************************************************** */
$rule->getLinksBlock(
        array(
            0000 => array(6000, 'BLK_CAD'),
            7000 => array(),
            6000 => array(
                0 => '<li><a href="?link=cad_cli">Cliente e seu PET</a></li>', 
            )
        )
);
$mod->TITLE1 = $I18N->getr("CADASTRAR");


/* * ********************************************************
 * MODULO LISTAR CADASTROS
 * ******************************************************** */
$rule->getLinksBlock(
        array(
            0000 => array(6500, 'BLK_LIST_CAD'),
            6000 => array(
                0 => '<li><a href="?link=list_pets">Cliente / PET</a></li>',

            )
        )
);
$mod->TITLE2 = $I18N->getr("LISTAR");


/* * ********************************************************
 * MODULO AGENDAMENTO
 * ******************************************************** */
$rule->getLinksBlock(
        array(
            0000 => array(6500, 'BLK_AGEND'), //6500
            7000 => array(),
            6000 => array(
                0 => '<li><a href="?link=agendar">Agendar servi&ccedil;o</a></li>',
                5 => '<li><a href="?link=check_agenda">Consultar agenda</a></li>'
            )
        )
);
$mod->TITLE3 = $I18N->getr("AGENDAMENTO");


/* * ********************************************************
 * MODULO RELATORIOS
 * ******************************************************** */
$rule->getLinksBlock(
        array(
            0000 => array(6500, 'BLK_RELAT'), //6500
            7000 => array(),
            6000 => array(
                //15 => '<li><a href="?link=relat_fat">Faturamento</a></li>',
                //5 => '<li><a href="?link=list_services">Pendentes</a></li>',
                10 => '<li><a href="?link=pend_pg">Pagamento pendente</a></li>',
                15 => '<li><a href="?link=faturas">Faturas</a></li>'
                
            ),
        )
);
$mod->TITLE4 = $I18N->getr("SERVI&Ccedil;OS");


$mod->show();
?>